<?php
session_start();
header('Content-Type: image/png');
// Max caractères
$max = 5;

// Largeur de l'image
$largeur = 200;

// Nb max de formes géo
$nb_geo = 10;

// Génération captcha
$captcha = str_shuffle(md5(uniqid()));
if($max > strlen($captcha)) {
    $max = strlen($captcha);
}
$captcha = substr($captcha, 0, $max);
$_SESSION['sign_captcha'] = $captcha;

// Pour fond
$red = rand(0, 255);
$blue = rand(0, 255);
$green = rand(0, 255);

$img = imagecreate($largeur, 80);
$back = imagecolorallocate($img, $red, $green, $blue);
$noir = imagecolorallocate($img, 0, 0, 0);
$blanc = imagecolorallocate($img, 255, 255, 255);
$bleu = imagecolorallocate($img, 0, 0, 255);
$vert = imagecolorallocate($img, 0, 255, 0);
$rouge = imagecolorallocate($img, 255, 0, 0);

// Couleurs random formes géo
$color1 = imagecolorallocate($img, 150, 200, 100);
$color2 = imagecolorallocate($img, 24, 10, 70);
$color3 = imagecolorallocate($img, 150, 101, 142);
$color4 = imagecolorallocate($img, 80, 145, 180);

// Tableaux de base
$colors = [$blanc, $bleu, $vert, $rouge];
$colors_geo = [$color1, $color2, $color3, $color4];
$fonts = scandir('./public/fonts/captcha_fonts/');

// Enlève . et ..
unset($fonts[0]);
unset($fonts[1]);

$arrayChar = str_split($captcha);
$space = 10;
$color = $noir;

// Random de forme géométrique
for($i = 1; $i <= $nb_geo; $i++) {
    $geo = rand(1,15);
    // Récupère couleur des geo
    $color_geo = $colors_geo[rand(0,3)];
    switch ($geo) {
        case 1:
            $forme = imagefilledellipse ($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 2:
            $forme = ImageEllipse($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 3:
            $forme = imagefilledellipse ($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 4:
            $forme = ImageEllipse($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 5:
            $forme = ImagefilledRectangle ($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 6:
            $forme = ImageRectangle($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 7:
            $forme = ImagefilledRectangle ($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        case 8:
            $forme = ImageRectangle($img, rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
        default:
            $forme = ImageLine ($img,rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $color_geo);
            break;
    }
}
for($i = 1; $i <= 20; $i++) {
    // Affiche 20 lignes random dérrière
    $forme = ImageLine ($img,rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $colors_geo[rand(0,3)]);
}

// Affichage caractères
foreach($arrayChar as $char) {
    // Random majuscule
    $toup =  rand(0, 1);
    // Prend une couleur du tableau des couleurs de text
    $test = $colors[rand(0,3)];
    // Prend une font
    $font = $fonts[rand(2,(count($fonts)+1))];
    // Vérifie que la couleur n'est pas couleur du fond, de la précédente utilisée et du dernier objet géo
    while($color == $test || $color_geo == $test || $back == $test) {
        $test = $colors[rand(0,3)];
    }
    imagettftext($img, rand(13, 25), rand(-50, 50), $space= $space + rand(10,25)+10, 30, $color = $test, './public/fonts/captcha_fonts/'.$font, ($toup == 0)?strtolower($char):strtoupper($char));
}
for($i = 1; $i <= 5; $i++) {
    // Affiche 5 lignes random dessus
    $forme = ImageLine ($img,rand(0, 200), rand(0, 100), rand(10, 50), rand(10, 50), $colors_geo[rand(0,3)]);
}

imagepng($img);
imagedestroy($img);

