


var randomCaracter = function () {
    var listString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var string = '';
    for (var i = 0; i < 15; i++) {
        string = string + listString[Math.floor(Math.random() * listString.length)];
    }
    return string;
}
$("#password_adduser").attr("type", "text");

$('#random_password').on('click', function (e) {
    e.preventDefault();

    $('#password_adduser').val(randomCaracter());

});

$('#lock_unlock').on('click', function (e) {
    e.preventDefault();

    if ($("#password_adduser").attr("type") === "text") {
        $("#password_adduser").attr("type", "password");

        $('#lock_unlock').attr('class', 'fa-2x fa fa-eye-slash');
    }
    else {
        $("#password_adduser").attr("type", "text");
        $('#lock_unlock').attr('class', 'fa-2x fa fa-eye');

    }

});