$("html").addClass("hidden");

$(document).ready(function() {

    $("html").removeClass("hidden");

    var twitchChannels = ["ESL_SC2", "OgamingSC2", "cretetion", "freecodecamp", "storbeck", "habathcx", "riotgames", "syndicate", "RobotCaleb", "noobs2ninjas", "brunofin", "comstar404"];

    var twitchData = [];

    var template = $('#twitch_data_template').html();

    var compiled = Handlebars.compile(template);

    //Loop through all the twitch channels in the array and pass them to the function which gets the corresponding data
    for (var i = 0; i < twitchChannels.length; i++) {
        getTwitchData(twitchChannels[i]);
    }

    //When all the ajax calls start
    $(document).ajaxStart(function() {
        $(".check").show();
    });

    //When all the ajax calls are done and dusted compile and render our template
    $(document).ajaxStop(function() {
        $(".container").removeClass("containerFlex");
        var rendered = compiled({
            data: twitchData
        });
        $('.container').html(rendered);
    });

    //When the all button link is clicked display all the channels
    $(".all").click(function() {
        $(".green").parents(".twitch_data_container").show();
        $(".grey").parents(".twitch_data_container").show();
        $(".red").parents(".twitch_data_container").show();
    });

    //When the online button link is clicked display only the channels which are online
    $(".online").click(function() {
        $(".twitch_data_container").hide();
        $(".green").parents(".twitch_data_container").show();

    });

    //When the offline button link is clicked display only the channels which are offline
    $(".offline").click(function() {
        $(".twitch_data_container").hide();
        $(".grey").parents(".twitch_data_container").show();
    });

    //Binding the events mouseenter and mouseleave on twitch data containers as they were not present in the DOM when the page loads
    $(".container").on({
        mouseenter: function() {

            $(this).addClass("hover");
            $(".hover").click(function() {

                var content = $(this).find(".twitch_channel_name").text();

                if ($(this).find("span").text() == "Online") {

                    $("#twitchModal iframe").attr({
                        'src': 'https://player.twitch.tv/?channel=' + content,
                        'height': 940,
                        'width': 360,
                        'allowfullscreen': ''
                    });

                    $("#twitchModal").modal("show");
                    $(".container").addClass("blur");
                } else {
                    window.open("https://www.twitch.tv/" + content);

                }

            });
        },
        mouseleave: function() {
            $(this).removeClass("hover");
        }
    }, ".twitch_data_container");

    //When the modal closes clear the iframe attributes and remove the blur effect which appears when it is on
    $('#twitchModal').on('hidden.bs.modal', function() {
        $(this).find('iframe').html("");
        $(this).find('iframe').attr("src", "");
        $(".container").removeClass("blur");
    });

    //The function which does the magic of getting the twitch data for the all the channels in the array from the twitch API
    function getTwitchData(channel) {
        $.ajax({
            url: "https://api.twitch.tv/kraken/streams/" + channel,
            headers: {
                'Client-ID': 'ttw9r23onu152f4ugrzbxrf8jvuqf2q'
            },
            success: function(result) {

                if (result.stream !== null) {

                    twitchData.push({
                        channelName: channel,
                        twitchImage: result.stream.channel.logo,
                        channelMessage: result.stream.channel.status,
                        status: "<span class='green' style='color:green'>Online</span>"
                    });

                } else {
                    $.ajax({
                        url: "https://api.twitch.tv/kraken/channels/" + channel,
                        headers: {
                            'Client-ID': 'ttw9r23onu152f4ugrzbxrf8jvuqf2q'
                        },
                        success: function(result) {
                            twitchData.push({
                                channelName: channel,
                                twitchImage: result.logo,
                                channelMessage: "",
                                status: "<span class='grey' style='color:grey'>Offline</span>"
                            });
                        }
                    });

                }

            },

            error: function(error) {

                twitchData.push({
                    channelName: channel,
                    twitchImage: "https://static-cdn.jtvnw.net/jtv-static/404_preview-300x300.png",
                    channelMessage: "",
                    status: "<span class='red' style='color:red'>Account Closed</span>"
                });

            }

        });
    }

    //Function which does the magic of searching through the twitch channels on the page and displaying the results when the search matches with the channel name
    function TwitchChannelSearch() {
        var criteria = $("#twitchSearch").val();

        $(".twitch_channel_name").each(function() {

            if ($(this).text() == criteria) {

                $(this).parents(".twitch_data_container").addClass("show_search");

                $(".twitch_data_container").each(function() {
                    if (!$(this).hasClass("show_search")) {
                        $(this).hide();
                    }
                })

            }

            if (!$(this).text() == criteria) {

                $(".twitch_data_container").each(function() {
                    if ($(this).hasClass("show_search")) {
                        $(this).removeClass("show_search");
                    }
                    $(this).show();
                })

            }
        })

    }

    //Whent the user starts typing in the search input box call the function which does this magic
    $("#twitchSearch").keyup(function() {

        TwitchChannelSearch();

    });

    //When the user clicks on the search icon call the function which does this magic
    $(".glyphicon-search").click(function() {
        TwitchChannelSearch();
    });

});