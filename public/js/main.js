/**
 * Includes a JS file on an other
 * @param fileName (Must be the path of the file, keep in mind you're on the js/ folder here
 */
function include(fileName) {
    document.write("<script src='" + fileName + "'></script>");
}

// Helpers functions
include(root_folder+"public/js/functions/helpers.js");

// Observer events
include(root_folder+"public/js/observers/observers.js");


$(".menuBurger").click(function(){
    $(".left-column").toggleClass("active");
});


include(root_folder+"public/js/admin/user/adduser.js");
