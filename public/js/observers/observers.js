/**
 * Password reset with email input
 * If check reset is 0, the reset form will show, otherwise, the login form will
 */
var check_reset = 0;
$('.reset_password').on('click', function() {
    if(!check_reset) {
        $(this).text('Je connais mon mdp, revenir sur la connexion');
        $(".login_form > label > input").each(function() {
            $(this).attr('disabled', 'disabled');
        });
        $(".reset_form > label > input").each(function() {
            $(this).removeAttr('disabled');
        });
        $('#reset_input').removeAttr('disabled');
        $('#login_form').attr('action', 'user/resetAjax');
        $("#login_form").attr('name', 'reset_form');
        $('#login_submit').val('Reinitialiser');
        $('.login_form').css('display', 'none');
        $('.reset_form').css('display', 'block');
        check_reset = 1;
    } else {
        $(this).text('Reinitialiser mon mdp');
        $(".login_form > label > input").each(function() {
            $(this).removeAttr('disabled');
        });
        $(".reset_form > label > input").each(function() {
            $(this).attr('disabled', 'disabled');
        });
        $('#login_submit').text('Se connecter');
        $('#reset_input').attr('disabled', 'disabled');
        $('#login_form').attr('action', 'user/login');
        $("#login_form").attr('name', 'login_form');
        $('.login_form').css('display', 'block');
        $('.reset_form').css('display', 'none');
        check_reset = 0;
    }
});


//////////////////////////////////////////////////////////// $HEADER HIDE ON SCROLL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}


//////////////////////////////////////////////////////////// $SLIDER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$(document).ready(function() {

   images = news_slide;

    var nbImages = images.length;
    var pattern = "";

    for(var i in images){
        // Concat of pattern for each image from json
        pattern +=
            "<div id='image_"+i+"' url='"+ images[i][jsonValues['link']] +"' class='" + itemsName + "' style='background-image : url(" + images[i][jsonValues['url']] + ");' > \
            </div>";
        numberOfImages++;
    }

    initialiseAll(images);
    $("#" + railName).html(pattern);

    timer = setInterval(nextImage, timeBetweenAnimations);

    /* Action Observers */
    $("#" + railName).mouseenter(function(){
        slideshowHovered = true;
        pauseSliding();
    }).mouseleave(function(){
        slideshowHovered = false;
        playSliding();
    });

    $("#" + playButton).on("click", function() {
        slideshowPaused = false;
        playClicked = !playClicked;
        if(playClicked)
            playSliding();
        else
            pauseSliding();
        pauseClicked = false;
        actualisePlayPauseColors();
    });

    $("#" + pauseButton).on("click", function() {
        slideshowPaused = true;
        pauseClicked = !pauseClicked;
        playClicked = false;
        actualisePlayPauseColors();
    });

    $("#" + railName).click(function() {
        changeSlidingStatus();
    });


    $("."+controlersName).click(function() {
        if(thumbnailStyle == 'classic') {
            $("."+controlersName).each(function() {
                if($(this).hasClass('fa-circle')) {
                    $(this).removeClass('fa-circle');
                    $(this).addClass('fa-circle-o');
                }
            });
            $(this).removeClass('fa-circle-o');
            $(this).addClass('fa-circle');
            position = $(this).attr('id').substr(11,1);
            clearInterval(timer);
            if(slideshowPaused) {
                slideshowPaused = false;
            }
            nextImage();
            if(!slideshowPaused && pauseClicked) {
                slideshowPaused = true;
            }
            timer = setInterval(nextImage, timeBetweenAnimations);
        } else if(thumbnailStyle == "modern") {
            $("."+controlersName).each(function() {
                if($(this).hasClass('show_control')) {
                    $(this).removeClass('show_control');
                }
            });
            $(this).addClass('show_control');
            position = $(this).attr('id').substr(11,1);
            clearInterval(timer);
            if(slideshowPaused) {
                slideshowPaused = false;
            }
            nextImage();
            if(!slideshowPaused && pauseClicked) {
                slideshowPaused = true;
            }
            timer = setInterval(nextImage, timeBetweenAnimations);
        }
    });

    $('#' + previousButton).on("click", function() {
        if(!animating) {
            animating = true;
            forcePreviousImage();
            setTimeout(function(){animating = false},slidingSpeed);
        }
    });

    $('#' + nextButton).on("click", function() {
        if(!animating) {
            animating = true;
            forceNextImage();
            setTimeout(function(){animating = false},slidingSpeed);
        }
    });
});

// Initialise HTML and CSS content
function initialiseAll(images)
{
    var count = 0;
    var link = images[count][jsonValues['link']];
    var htmlContent = '';
    if(showTitle)
        htmlContent = ' <div class="title_block_'+thumbnailStyle+'"><a class="link_hf" href="'+link+'"><h3 id="' + titleName + '"></h3></a></div>';
    htmlContent += '<div id="' + viewedPartName + '"> ';
    if(enableLeftRightButtons)
        htmlContent += '<div id="' + previousButton + '" class="left_right_'+thumbnailStyle+' fa ' + previousDesign + '"> </div>';
    htmlContent += ' <div id="' + railName + '"></div>';
    if(enablePlayPauseButtons)
        htmlContent += '<div id="' + pauseButton + '" class="fa ' + pauseDesign + '"> </div>\
                                            <div id="' + playButton + '" class="fa ' + playDesign + '"> </div>';
    if(enableLeftRightButtons)
        htmlContent += '<div id="' + nextButton + '" class="left_right_'+thumbnailStyle+' fa ' + nextDesign + '"> </div> \
                            </div>';
    if(showControls) {
        htmlContent+= '<div class="' + controlsName + '"> ';
        for(var i in images) {
            if(thumbnailStyle == 'classic') {
                if(!count)
                    htmlContent += '<i id="controller_'+i+'" class=" '+controlersName+' fa fa-circle" aria-hidden="true"></i>';
                else
                    htmlContent += '<i id="controller_'+i+'" class=" '+controlersName+' fa fa-circle-o" aria-hidden="true"></i>';
            }
            if(thumbnailStyle == 'modern') {
                if(!count)
                    htmlContent += '<div id="controller_'+i+'" class="show_control '+controlersName+' '+thumbnailStyle+'"><span class="nb_controllers">'+(count+1)+'</span></div>';
                else
                    htmlContent += '<div id="controller_'+i+'" class=" '+controlersName+' '+thumbnailStyle+'"><span class="nb_controllers">'+(count+1)+'</span></div>';
            }
            count++;
        }
        htmlContent += '</div>';
    }
    htmlContent += '</div>\
                    <div class="block_desc"></div>';
    if(showDescription)
        htmlContent+= '<div class="description_'+thumbnailStyle+'" id="' + descriptionName + '"><a class="link_hf" href="'+link+'"><p id="'+descName+'"></p></a></div>';

    $("#" + slideshowName).html(htmlContent);


    /**
     * Here is the main CSS content for the JS Slideshow, feel free to change it in order to match your needs according to the classes and ids you chose
     */
    var cssText = "";

    cssText += "<style >";

    cssText += "body { margin: 0; padding: 0;}";

    cssText += ".fa-circle {color: "+selectedCircleColor+"; }";

    cssText += ".fa-circle-o {color: "+unselectedCircleColor+"; }";

    cssText += "#" + slideshowName + "{ width: " + slideshowWidth + "%; text-align:center; } "; // Center everything in the slideshow

    cssText += "#" + slideshowName + ", #" + slideshowName + " * { margin:0; padding:0; } "; // Avoid every custom padding and margin in the slideshow

    var margin = '';
    if(thumbnailStyle == 'modern') {
        margin = '';/*margin-left:20% !important;*/
    }

    cssText += "#" + viewedPartName + "{ width: " + viewedPartWidth + "%; overflow: hidden; position:relative; "+margin+" "; // All images are side by side. This allow to hide all other images and only keep one.
    if(centered)
        cssText += "margin-left: auto;margin-right: auto"; // If centered is true : the slideshow is centered.
    cssText += " } ";


    cssText += "#" + railName + " img { max-width: 100%; } "; // Allow images to take all width possible without overflow. max-width parameter is for small images.

    cssText += "#" + railName + " { width: " + numberOfImages + "00%; height: " + imageMaxHeight +"vh ; } "; // number of images * 100%.

    cssText += "." + itemsName + "{ width : " + (100/numberOfImages) + "% ; display : inline-block ; height: " + imageMaxHeight +"vh ; background-position: center center;  background-size: cover;} "; // all items (div of a image) take 100% of the space possible, so we need to divide the 100% by the number of images.

    cssText += "." + controlersName+ " { margin-left: 15px  !important; margin-right: 15px  !important; }";

    cssText += "." + controlersName+ ": first-child { margin-left: 0px !important; margin-right: 0px  !important; }";

    cssText += "." + controlersName+ ": lastt-child {margin-left: 0px  !important; margin-left: 0px  !important; }";

    cssText += "#" + pauseButton + "{ position:absolute ; left:10px ; bottom:10px; font-size:1.5em ; color : " + playPauseColor + " ; }" // Fix play button at bottom left and give him colors

    cssText += "#" + playButton + "{ position:absolute ; left:50px ; bottom:10px; font-size:1.5em ; color : " + playPauseColor + " ; }" // Fix pause button at bottom left and give him colors

    cssText += ".modern:hover {background-color: "+buttonHoverBackground+"; cursor:pointer;}";

    cssText += ".modern { display:inline-block; height:40px; margin-left:5px !important; margin-right:5px  !important}";

    cssText += ".show_control {background-color: "+ buttonBackground+ "; color:"+buttonTextColor+"}";

    cssText += "." + controlsName + "{margin-top:5px !important;}";

    if(imageMaxHeight == 100) {
        cssText += "#"+titleName+" {position: absolute;z-index: 1;color: black;top: 65px;left: 113px;font-size: 40px; font-family:"+fontFamily+";}";
        cssText += "#"+descriptionName+" {position: absolute;z-index: 1;color: black;top: 100px;left: 150px;font-size: 30px; font-family:"+fontFamily+";}";
    }
    cssText += ".left_right_modern {background: "+buttonBackground+";padding: 13px !important;color:"+onImageButtons+"}";

    cssText += ".left_right_classic {font-size:2em!important;color:black;}";

    cssText += ".description_modern {font-family:"+fontFamily+";float:left; color:"+onImageButtons+";position: relative; background: "+onImageBackground+";transform:skewX(20deg); -ms-transform:skewX(20deg); -webkit-transform:skewX(20deg); bottom: 63vh;display: inline-block;padding: 10px!important;padding-right: 40px !important;padding-left: 30px !important; z-index:1;left:22%}";

    cssText += ".left_right_modern:hover {cursor: pointer;}";

    cssText += ".title_block_modern {font-family:"+fontFamily+";float:left; color:"+buttonTextColor+";position: relative; background: "+buttonBackground+";transform:skewX(15deg); -ms-transform:skewX(15deg); -webkit-transform:skewX(15deg); top: 25vh;display: inline-block;padding: 10px!important;padding-right: 40px !important;padding-left: 30px !important; z-index:1;left:19%}";

    cssText += "#"+titleName+" {transform:skewX(-15deg); -ms-transform:skewX(-15deg); -webkit-transform:skewX(-15deg);text-transform:uppercase;}";
    cssText += "#"+descName+" {transform:skewX(-20deg)!important; -ms-transform:skewX(-20deg)!important; -webkit-transform:skewX(-20deg)!important; color: black;}";

    cssText += ".nb_controllers {padding:16px !important;top: 11px;position:relative;font-family:"+fontFamily+", cursive; }";

    cssText += "@-webkit-keyframes progress { 0% { width: 0%; } 100% { width: 100%; } }";

    cssText += "#" + previousButton + "{ position:absolute ; left:0px; font-size:1em; top:" + ((imageMaxHeight / 2) - 5) + "vh}";  // Fix previous button at center left

    cssText += "#" + nextButton + "{ position:absolute ; right:0px; font-size:1em; top:" + ((imageMaxHeight / 2 ) - 5) + "vh}";  // Fix previous button at center right

    cssText += "</style>"; // END OF CSS

    $("head").append(cssText);

    $("#" + titleName).html(images[(position-1)][jsonValues["title"]]);

    $("#" + descName).html(images[(position-1)][jsonValues["description"]]);
}

// Move the rail with animation
function nextImage()
{
    if(!slideshowPaused) {
        position++;

        if(position > numberOfImages) {
            $("#" + railName).stop().animate({marginLeft: '0%'}, slidingSpeed);
            position = 1;
        }
        else {
            $("#" + railName).stop().animate({marginLeft: '-'+ (position-1) +'00%'}, slidingSpeed);
        }

        $(".link_hf").attr('href', images[position-1][jsonValues["link"]]);

        animateText(titleName, animatingTitleType);
        animateText(descName, animatingDescriptionType);

        nextThumbnail(position-1);
    }
}

// Animate to the previous image
function previousImage()
{
    position--;
    if(position < 1) {
        $("#" + railName).animate({marginLeft: '-' + (numberOfImages - 1) + '00%'}, slidingSpeed);
        position = numberOfImages;
    }
    else
        $("#" + railName).animate({marginLeft: '-' + (position-1) +'00%'}, slidingSpeed);

    $(".link_hf").attr('href', images[position-1][jsonValues["link"]]);

    animateText(titleName, animatingTitleType);
    animateText(descName, animatingDescriptionType);

    nextThumbnail(position - 1);
}

/* Animate text
 * content : Title or Description
 * type : Type of animation
 */
function animateText(content, type)
{
    var typeOfContent;

    if(content == titleName)
        typeOfContent = jsonValues["title"];
    else
        typeOfContent = jsonValues["description"];

    switch(type) {

        case ("slideUp") : // Slide up / Slide down animation
            $("#" + content).slideUp((slidingSpeed/2), function(){
                $("#" + content).html(images[(position-1)][typeOfContent]);
                $("#" + content).slideDown((slidingSpeed/2));
            });
            break;

        case ("fade") : // Fade in / Fade out animation
            $("#" + content).fadeOut((slidingSpeed/2), function(){
                $("#" + content).html(images[(position-1)][typeOfContent]);
                $("#" + content).fadeIn((slidingSpeed/2));
            });
            break;

        default : // Default is Fade in / Fade Out
            $("#" + content).html(images[(position-1)][typeOfContent]);
            break;

    }
}

/* Change thumbnail when changing slide. Find the current active thumbnail to reinitialize it and active the new one
 * position : new position of slider
 */
function nextThumbnail(position)
{
    if(thumbnailStyle == 'classic') {
        $('.controllers').each(function() {
            if($(this).hasClass('fa-circle')) {
                $(this).removeClass('fa-circle');
                $(this).addClass('fa-circle-o');
            }
        });
        if($("#controller_"+(position)+"").hasClass('fa-circle-o')) {
            $("#controller_"+(position)+"").removeClass('fa-circle-o');
            $("#controller_"+(position)+"").addClass('fa-circle');
        }
    } else if(thumbnailStyle == 'modern') {
        $('.controllers').each(function() {
            if($(this).hasClass('show_control')) {
                $(this).removeClass('show_control');
            }
        });
        $("#controller_"+(position)+"").addClass('show_control');
    }
}

// Invert status of sliding
function changeSlidingStatus()
{
    if(!slideshowPaused)
        pauseSliding();
    else
        playSliding();
}

// Pause sliding
function pauseSliding()
{
    if(!pauseClicked && !playClicked)
        slideshowPaused = true;
}

// Play sliding
function playSliding()
{
    if(!pauseClicked && !playClicked)
        slideshowPaused = false;
}

// Checked status of play and pause buttons to change their colors
function actualisePlayPauseColors()
{
    if(playClicked)
        $("#" + playButton).css("color", playPauseColorActivated);
    else
        $("#" + playButton).css("color", playPauseColor);
    if(pauseClicked)
        $("#" + pauseButton).css("color", playPauseColorActivated);
    else
        $("#" + pauseButton).css("color", playPauseColor);
}

// Force the next image if user click on next button
function forceNextImage()
{
    slideshowPaused = false;
    nextImage();
    slideshowPaused = true;
    reloadInterval();
}

// Force the previous image if user click on previous button
function forcePreviousImage()
{
    previousImage();
    reloadInterval();
}

// Reload the interval
function reloadInterval()
{
    clearInterval(timer)
    timer = setInterval(nextImage, timeBetweenAnimations);
}




//////////////////////////////////////////////////////////// MODAL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

(function(){
    var $content = $('.modal_info').detach();

    $('.open_button').on('click', function(e){
        modal.open({
            content: $content,
            width: 540,
            height: 270,
        });
        $content.addClass('modal_content');
        $('.modal, .modal_overlay').addClass('display');
        $('.open_button').addClass('load');
    });
}());

var modal = (function(){

    var $close = $('<button role="button" class="modal_close" title="Close"><span></span></button>');
    var $content = $('<div class="modal_content"/>');
    var $modal = $('<div class="modal"/>');
    var $window = $(window);

    $modal.append($content, $close);

    $close.on('click', function(e){
        $('.modal, .modal_overlay').toggleClass('conceal');
        $('.modal, .modal_overlay').toggleClass('display');
        $('.modal, .modal_overlay').removeClass('conceal');
        $('.open_button').removeClass('load');
        e.preventDefault();
        modal.close();
    });

    return {
        center: function(){
            var top = Math.max($window.height(-220) - $modal.outerHeight(),0) / 2;
            var left = Math.max($window.width() - $modal.outerWidth(), 0) / 2;
            $modal.css({
                top: top + $window.scrollTop(),
                left: left + $window.scrollLeft(),
            });
        },
        open: function(settings){
            $content.empty().append(settings.content);

            $modal.css({
                width: settings.width || 'auto',
                height: settings.height || 'auto'
            }).appendTo('body');

            modal.center();
            $(window).on('resize', modal.center);
        },
        close: function(){
            $content.empty();
            $modal.detach();
            $(window).off('resize', modal.center);
        }
    };
}());


//////////////////////////////////////////////////////////// MODAL 2 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


(function(){
    var d = $('#somedialog');
    $('.open').click(function(e){
        d.removeClass('dialog-close');
        d.addClass('dialog-open');
    });
    $('.close, .dialog-overlay').click(function(e){
        d.removeClass('dialog-open');
        d.addClass('dialog-close');
    });
}());


//////////////////////////////////////////////////////////// MENU RESPONSIVE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
(function($){

    $('#header__icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
    })

})(jQuery);

burger = document.getElementById("burger-button");

burger.addEventListener("click", (e) => {
    e.preventDefault();
document.body.classList.toggle("open");
burger.classList.toggle("open");
});

//////////////////////////////////////////////////////////// SLIDER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\





//////////////////////////////////////////////////////////// FORM ANIMATIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$(function() {
    $('input').on('change', function() {
        var input = $(this);
        if (input.val().length) {
            input.addClass('populated');
        } else {
            input.removeClass('populated');
        }
    });

    setTimeout(function() {
        $('#fname').trigger('focus');
    }, 500);
});


//////////////////////////////////////////////////////////// PROFIL AVATAR \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


$(function() {
    $('.view_details').click(function() {
        if ($(this).is(':checked')) {
            $(this)
                .next('label')
                .html('&times;')
                .attr('title', 'tap here to close full profil');
            $(this)
                .parent()
                .next('main')
                .slideDown('normal');
        } else {
            $(this)
                .next('label')
                .html('=')
                .attr('title', 'tap here to view full profil');
            $(this)
                .parent()
                .next('main')
                .slideUp('fast');
        }
    });
});

//////////////////////////////////////////////////////////// $MENU LINE UP \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


$(function(){

    $('.display-games').click(function(){
        $('.menu-games').toggleClass('menu-games-on');
    });

});


$(function(){

    $('.display-games').click(function(){
        $('.fa-chevron-right').toggleClass('fa-rotate');
    });

});


//////////////////////////////////////////////////////////// $TRI LIST NEWS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$(document).ready(function() {
    var triItem = $(".tri ul li"),  // change to your own sort item selector
        portfolioItem = $(".display-news .news-section"); // change to your own portfolio item selector

    triItem.click(function() {
        var toSortId = $(this).attr("id");

        $(".active").removeClass("active");

        $("#"+toSortId).addClass("active");


        if(toSortId == "all") {
            portfolioItem.each(function() {
                $(this).removeClass("sortedDown");
                $(this).addClass("sortedUp");
            });
        }

        else {
            portfolioItem.each(function() {
                if($(this).hasClass(toSortId)) {
                    $(this).removeClass("sortedDown");
                    $(this).addClass("sortedUp");
                }

                else {
                    $(this).removeClass("sortedUp");
                    $(this).addClass("sortedDown");
                }
            });
        }

    });
});



////////////////////////////////////////////////////////////  $STREAM    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// Twitch users
if(!users) {
    var users = [];
}
// Running code on ready state
$(document).ready(function() {

    // Iterating through "users" array
    users.forEach(function getUsers(channel){
        function makeURL(type, name) {
            return 'https://wind-bow.gomix.me/twitch-api/' + type + '/' + name + '?callback=?';
        };

        // Checking for user's stream status
        $.getJSON(makeURL("streams", channel), function(data) {

            var streamInfo,
                status,
                defaultAvatar = 'https://raw.githubusercontent.com/boniverski/twitch-tv/master/image/twitch-icon.png'; //If there's no user's avatar

            if (data.stream === null) {
                streamInfo = "";
                status = "offline"
            } else {
                streamInfo = data.stream.game;
                status = "online"
            };

            // Fetching JSON data and setting up variables
            $.getJSON(makeURL("channels", channel), function(data) {


                var avatar = data.logo != null ? data.logo : defaultAvatar,
                    user = data.display_name != null ? data.display_name : channel,
                    game = streamInfo ? " " + data.status : "",
                    url = data.url,
                    searchQuery = user.toLowerCase();

                //Check if user exist
                if (data.error) {
                    game = "This user does not exit or the account is closed.";
                    status = "offline";
                }

                // Setting online/fffline indicator in user's card
                var statusIndicator, addClass;
                if (status === "online") {
                    statusIndicator = '<span class="user__availability user__availability--on" title="Online"></span>';
                    addClass = 'online';
                } else {
                    statusIndicator = '<span class="user__availability user__availability--off" title="Offline"></span>';
                    addClass = 'offline';
                }

                // Displays user's card in results
                var html = `<div class="user ${addClass}" data-filter-item data-filter-name="${searchQuery}"><a href="${url}" target="_blank"><img class="user__avatar" src="${avatar}"></a><div class="user__card"><a href="#"><h4 id="${user}" class="user--name">${user}</h4></a><p class="user--stream-info">${game}</p></div>${statusIndicator}</div>`;
                $(html).hide().appendTo(".main").fadeIn(300);
                $(".load-bar").hide(300); // Hiding loading bar when data is ready
                $(".availability__btn--all").addClass("active-all-btn"); //Active "All" button

                // Setting up search bar
                $("[data-search]").on("keyup", function() {
                    var searchVal = $(this).val().toLowerCase();
                    var filterItems = $("[data-filter-item]");
                    if (searchVal != "") {
                        filterItems.addClass("hidden");
                        $('[data-filter-item][data-filter-name*="${searchVal}"]').removeClass("hidden");
                    } else {
                        filterItems.removeClass("hidden");
                    }
                });

                $('.user--name').on('click', function() {
                    var user = $(this).attr('id');
                    $('.exemple-iframe').html(
                     '<iframe  class="float-left col-mobile" src="https://player.twitch.tv/?channel='+user+'" frameborder="0" allowfullscreen="true" scrolling="no" height="500" width="350"></iframe><a href="https://www.twitch.tv/'+user+'?tt_medium=live_embed&tt_content=text_link" style="padding:2px 0px 4px; display:block; width:345px; font-weight:normal; font-size:10px; text-decoration:underline;"></a>' +
                     '<iframe class="float-right col-mobile" src="https://www.twitch.tv/pgl/chat?popout=" frameborder="0" scrolling="no" height="500" width="350"></iframe>'
                    );
                });
            });
        });
    });

    //Filtering users based on stream status
    $(".availability__btn").click(function() {
        var status = $(this).attr('id');
        if (status === "all") {
            $(".online, .offline").removeClass("hidden");
            $(".availability__btn--all").addClass("active-all-btn");
            $(".availability__btn--on").removeClass("active-on-btn");
            $(".availability__btn--off").removeClass("active-off-btn");
        } else if (status === "on") {
            $(".online").removeClass("hidden");
            $(".offline").addClass("hidden");
            $(".availability__btn--on").addClass("active-on-btn");
            $(".availability__btn--off").removeClass("active-off-btn");
            $(".availability__btn--all").removeClass("active-all-btn");
        } else {
            $(".offline").removeClass("hidden");
            $(".online").addClass("hidden");
            $(".availability__btn--off").addClass("active-off-btn");
            $(".availability__btn--on").removeClass("active-on-btn");
            $(".availability__btn--all").removeClass("active-all-btn");
        }
    })
});

////////////////////////////////////////////////////////////  PROFILE    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


var clicked = 0;
var value = '';
$('.profile_change').on('click', function() {
    var value = '';
    var field = $(this).attr('id');
    var user = $(this).attr('user');
    var element = $(this).parent().parent().parent().find(".profile__information-detail-text");


    if(!clicked) {
        value = $(this).parent().parent().parent().find(".profile__information-detail-text").text();

        var type = "text";
        if (field === 'password') {
            type = 'password';
        }
        $(element).html('<input type="' + type + '" name="' + field + '" value="' + value + '">');
        clicked = 1;
    } else {
        value = $("[name='"+field+"']").val();
        var name = $("[name='"+field+"']").attr('name');

        $.ajax({
            type: "POST",
            url: root_folder+"user/edit",
            content: "application/json; charset=utf-8",
            dataType: "html",
            data: "name="+name+"&value="+value+"&user="+user,
            success: function(msg) {
                if(msg == 'redirect') {
                    window.location.href = root_folder+'/user/logout';
                } else {
                    $(element).html(msg);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                // TODO: Show error
            }
        });
        clicked = 0;
    }
});


$('div.animalcontent').hide();
$('#catcontent').hide();
$('p.animal').bind('mouseover', function() {
    $('div.animalcontent').fadeOut();
    $('#'+$(this).attr('id')+'content').fadeIn();
});


////////////////////////////////////////////////////////////  $MESSAGERIE    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

if(!data) {
    data = {};
}

var app = {
    data: data,
    html: {
        boxes: document.querySelector('.boxes'),
        mail: document.querySelector('.mail'),
        letters: document.querySelector('.mail .letters tbody'),
        display: document.querySelector('.mail .display')
    },
    cur_box: undefined,
    start: function(){
        app.loadInboxes();
    },
    loadInboxes: function(){
        app.data.boxes.forEach(function(box, index){
            var newBox = `
            <div class="box" id="box_${index}"> 
               <div class="count">${box.mail.length}</div> 
               <div class="logo"><i class="fa fa-envelope-o" aria-hidden="true"></i></div> 
               <div class="logo">${box.label}</div> 
            </div> 
          `;
            app.html.boxes.innerHTML += newBox;
        });

        app.data.boxes.forEach(function(box, index){
            document.querySelector('#box_'+index).onclick = function(){
                var boxID = this.id.split('_')[1];
                app.showInboxMail(boxID);
            };
        });
    },
    showInboxMail: function(boxID){
        app.clearDisplay();
        var box = app.data.boxes[boxID];
        app.cur_box = boxID;
        app.html.letters.innerHTML = '';
        box.mail.forEach(function(letter, index){
            var newLetter = `
            <tr id="letter_${index}"> 
               <td>${letter.Date}</td> 
               <td>${letter.Objet}</td> 
               <td>${letter.Emetteur}</td> 
               <td>${letter.Destinataire}</td> 
            </tr> 
         `;
            app.html.letters.innerHTML += newLetter;
        });
        box.mail.forEach(function(letter, index){
            var letter = document.querySelector('#letter_'+index);
            letter.onclick = function(){
                var letterID = this.id.split('_')[1];
                app.showLetter(letterID);
            }
        });
    },
    showLetter: function(letterID){
        var letter = app.data.boxes[app.cur_box].mail[letterID];
        var letterHTML = `
         <div>EMETTEUR: ${letter.Emetteur}</div> 
         <div>DESTINATAIRE: ${letter.Destinataire}</div> 
         <div>DATE: ${letter.Date}</div> 
         <h2>${letter.Objet}</h2> 
         <div>${letter.Message}</div> 
      `;
        app.html.display.innerHTML = letterHTML;
    },
    clearDisplay: function(){
        app.html.display.innerHTML = '';
    }
};

app.start();


////////////////////////////////////////////////////////////  $NOTIFICATIONS   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$('.button').on('click', function(event){
    var type = $(this).data('type');
    var status = $(this).data('status');

    $('.button').removeClass('is-active');
    $(this).addClass('is-active');

    $('.notify')
        .removeClass()
        .attr('data-notification-status', status)
        .addClass(type + ' notify')
        .addClass('do-show');

    event.preventDefault();
});



////////////////////////////////////////////////////////////  $ARTICLES   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

/*var nystories = document.querySelector("p").offsetTop;
window.onscroll = function() {
    if (window.pageYOffset > 0) {
        var opac = (window.pageYOffset / nystories);
        document.body.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + "))";
    }
};*/




////////////////////////////////////////////////////////////  $TEAM PLAYERS LIST   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
function DropDown(el) {
    this.dd = el;
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            event.stopPropagation();
        });
    }
}
$(function() {

    var dd = new DropDown( $('#dd') );

    $(document).click(function() {
        // all dropdowns
        $('.wrapper-dropdown-2').removeClass('active');
    });

});


////////////////////////////////////////////////////////////  $ARTICLES DETAILS   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


$(function(){
    $.stellar({
        horizontalScrolling: false,
        verticalOffset: 0,
        responsive: true
    });
});



