<?php

class GamingController
{

    public function indexAction($params)
    {
        $view = new View("gaming");
        $team = new Team();
        $limit = 3;

        $page = ($params['page']-1) * $limit;

        $team->customSQL("
            select
                ".PREFIX."game_list.name as game,
                ".PREFIX."team.*
            from
                ".PREFIX."game_list,
                ".PREFIX."team
            where
                ".PREFIX."game_list.id = ".PREFIX."team.id_game_list
            limit
                ".$page.", ".$limit."
            ", "");
        $teams = $team->executeSQL();
        $view->assign("page", $params['page']);
        $view->assign("teams", $teams);
    }

    public function addGameAction($params)
    {

    }

    public function updateGameAction($params)
    {

    }

    public function deleteGameAction($params)
    {

    }

}