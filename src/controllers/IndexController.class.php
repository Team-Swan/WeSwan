<?php

class IndexController{

	public function indexAction($params){
	    $user = new User();
		$view = new View();
		$view->assign("sign_form", $user->getSignForm());
        $view->assign("login_form", $user->getLoginForm());

        $articles = new Content();
        $articles->selectSQL(PREFIX."content.slug, ".PREFIX."content.title, ".PREFIX."content.content , ".PREFIX."content.image");
        $articles->fromSQL("user");
        $articles->whereSQL("type", "news");
        $articles->limitSQL(3);
        $slide_news = $articles->executeSQL();
        $view->assign("slide_news", $slide_news);

        $articles = new Content();
        $articles->selectSQL(PREFIX."content.*, ".PREFIX."user.username");
        $articles->fromSQL("user");
        $articles->whereSQL("type", "news");
        $articles->andJoinSQL("id_user", "user", "id");
        $articles->limitSQL(6);
        $block_articles = $articles->executeSQL();
        $view->assign("block_articles", $block_articles);

        $articles = new Content();
        $articles->selectSQL(PREFIX."content.*, ".PREFIX."user.username");
        $articles->fromSQL("user");
        $articles->whereSQL("type", "events");
        $articles->andJoinSQL("id_user", "user", "id");
        $articles->limitSQL(6);
        $block_events = $articles->executeSQL();
        $view->assign("block_events", $block_events);

        $flux_rss = new Content();
        $category = "news";
	    $flux_rss->customSQL("
	        select 
	            ".PREFIX."content.*
	        from 
	            ".PREFIX."content
	        where
	            ".PREFIX."content.type = :".$category."
	        limit 10
	    ", [":".$category => $category] );
	    $news_rss = $flux_rss->executeSQL();

	    if(count($news_rss)) {
            // Création des entêtes du flux RSS
            $oRssFeed = new RSSFeed('utf-8');
            $oRssFeed->setProtectString(true);
            $oRssFeed->setTitle('Flux RSS des actualités');
            $oRssFeed->setDescription('Les dernières actualités directement par flux RSS');
            $oRssFeed->setLink(URL_SITE . '/' . basename(__FILE__));
            $oRssFeed->setWebMaster(WEBMASTER_EMAIL, WEBMASTER_NOM);
            $oRssFeed->setManagingEditor(WEBMASTER_EMAIL, WEBMASTER_NOM);
            $oRssFeed->setCopyright('(C) Copyright 2007 - ' . URL_SITE . ' - Tous droits réservés - reproduction interdite');
            $oRssFeed->setGenerator('');
            $oRssFeed->setLanguage('fr');

            foreach($news_rss as $oNews)
            {

               /* $oRssItem = new RSSFeedItem();
                $oRssItem->setTitle($oNews['title']);
                $oRssItem->setDescription($oNews['content']);
                $oRssItem->setLink(URL_SITE .'/news/'.$oNews['slug']);
                $oRssItem->setGuid(URL_SITE .'/news/'.$oNews['slug'], true);

                $oRssItem->setAuthor(WEBMASTER_EMAIL, $oNews->auteur);

                $oRssItem->setPubDate(CoreHelper::getShortdate($oNews['created_at'], 4));
                $oRssFeed->appendItem($oRssItem);
                $oRssItem = null;*/
            }

            // Sauvegarde du flux RSS
            //$oRssFeed->save(ROOT_FOLDER.'/rss-news.xml');
            $view->assign('rss', $oRssFeed);
        }
	}
}