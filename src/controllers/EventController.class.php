<?php

class EventController {


    public function indexAction($params)
    {
        $view = new view("events");
        $articles = new Content();

        $limit = 3;
        $page = ($params['page']-1) * $limit;

        $articles->selectSQL(PREFIX."content.*, ".PREFIX."user.username");
        $articles->fromSQL("user");
        $articles->whereSQL("type", "events");
        $articles->andJoinSQL("id_user", "user", "id");
        $articles->limitSQL($page, $limit);
        $block_events = $articles->executeSQL();
        $view->assign("block_events", $block_events);
        $view->assign("page", $params['page']);
    }


    public function showAction($params)
    {
        $link = "event/show/".urldecode($params[0])."/".urldecode($params[1]);


        $article = new Content();
        $user = new User();

        $article = $article->populate(['slug' => $link]);
        $user = $user->populate(['id' => $article->getIdUser()]);

        $comment = new Comment();
        $comment->selectSQL(PREFIX."comment.*, ".PREFIX."user.username, ".PREFIX."user.avatar");
        $comment->fromSQL("user");
        $comment->whereSQL("id_content", $article->getId());
        $comment->andJoinSQL("id_user", "user", "id");
        $comment->orderSQL("created_at");
        $comments = $comment->executeSQL();

        if($article && $article->getId()) {
            $view = new View("article");
            $view->assign("article", $article);
            $view->assign("user", $user);
            $view->assign("comments", $comments);
            $view->assign("comment_form", $comment->getCommentForm());
        } else {
            $view = new View("page404", "singlepage");
        }
    }

    public function suggestEventAction($params)
    {

    }

    public function filterEventsAction($params)
    {

    }
}