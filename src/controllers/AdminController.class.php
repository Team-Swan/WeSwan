<?php

class AdminController
{

    public function indexAction($params)
    {
        $v = new view("admin/index", "backend");
    }

    public function addUserAction($params)
    {
        $v = new view("admin/user", "backend");
        $v->assign("ActionPage", "add");
        $form = new AddUserAdmin();
        $v->assign("addAdminUser_form", $form->addAdmin());
        if (isset($_POST['addUser'])) {
            unset($params[0]);
            unset($params['addUser']);
            if (UserHelper::validSignForm($form->addAdmin(), $params) == TRUE && CoreHelper::uploadImage($_FILES, $form->addAdmin()) == TRUE) {
                unset($_SESSION['hasError']);
                unset($_SESSION['oldData']);
                unset($_SESSION['listOfErrors']);
                $user = new User();
                foreach ($params as $name => $value) {
                    $value = htmlentities(trim($value));
                    if ($name != 'captcha' && $name != 'password_confirmation' && $name != 'cgu' && $name != 'csrf' && $name != "role") {
                        if ($name == 'password') {
                            $pass = $value;
                            $value = password_hash($value, PASSWORD_BCRYPT);

                        }
                        $user->{"set" . ucfirst($name)}($value);
                        $user->setIdRole($params['role']);
                        if ($name == "certify") {
                            if ($value == 0) {
                                $mail = CoreHelper::mailSetup();
                                $mail->addAddress($user->getEmail(), $user->getUsername());
                                $mail->Subject = "Confirmation de ton compte sur WeSwan CMS";
                                $mail->Body = "Salut " . $user->getUsername() . "!<br><br>
                                        Bienvenue sur <a href='https://weswan.fr'>weswan.fr</a>!<br>
                                        Plus qu'une étape pour finaliser ton inscription, tu y es presques.<br><br>
                                        Clique sur ce lien pour valider ton compte:<br>
                                        <a href=\"https://WeSwan.fr/user/confirmPassword?token=" . $user->getToken() . "&email=" . $user->getEmail() . "\">Confirmer mon adresse e-mail</a><br><br>
                                        <br>
                                        Ton mot de passe a été envoyé par l'administrateur, le voici : <b>" . $pass . "</b><br>
                                        A bientôt,<br>
                                        Equipe WeSwan";

                                $mail->AltBody = "Salut " . $user->getUsername() . "!<br><br>
                                        Bienvenue sur https://weswan.fr .<br>
                                        Plus qu'une étape pour finaliser ton inscription, tu y es presques.<br><br>
                                        Copie ce lien dans ton navigateur pour confirmer ton adresse e-mail:<br>
                                        https://WeSwan.fr/user/confirmPassword?token=" . $user->getToken() . "&email=" . $user->getEmail() . "<br><br>
                                        A bientôt,<br>
                                        Equipe WeSwan";
                            } else {
                                $user->setCertify(1);
                            }
                        }
                        if (empty($_FILES["avatar"]["name"])) {
                            $user->setAvatar("NULL");
                        } else {
                            $user->setAvatar($_SESSION['data_array_image']);
                        }
                    }
                }
                $user->save();
                if (!$mail->send()) {
                    $_SESSION['listOfErrors'][] = "Oups il y a eu un problème lors de l'envoie du mail!";
                } else {
                    $_SESSION['listOfSuccess'][] = "Utilisateur correctement ajouté";
                }
                unset($_SESSION['data_array_image']);
            } else {
                // Sends errors back to the home page
                $_SESSION['hasError'] = 1;
                // Sends data sent on form back to the home page
                foreach ($params as $name => $value) {
                    if ($name != 'captcha' && $name != 'password' && $name != 'password_confirmation' && $name != 'cgu') {
                        $_SESSION['oldData'][$name] = $value;
                    }
                }
                header("Location: " . ROOT_FOLDER . "admin/adduser");
            }
        }
        UserHelper::$listErrors = [];
    }

    public function editUserAction($params)
    {
        $v = new view("admin/user", "backend");
        $v->assign("ActionPage", "edit");
        $user = new User();
        $form = new EditUserAdmin();
        $role = new Role();

        $get_user_info = $user->getElements(NULL, "id", $params[0]);
        if ($get_user_info != NULL) {
            $get_role = $role->getElements();
            $array_data_role = [];
            for ($i = 0; $i < count($get_role); $i++) {
                $array_data_role[$get_role[$i]['id']] = $get_role[$i]['name'];
            }
            $v->assign("editAdminUser_form", $form->editAdmin($get_user_info[0], $array_data_role));
            $user = $user->populate(['id' => $params[0]]);
            if (isset($_POST['editUser'])) {
                unset($params[0]);
                unset($params['editUser']);

                if (UserHelper::validSignForm($form->editAdmin($get_user_info[0], $array_data_role), $params) == TRUE && CoreHelper::uploadImage($_FILES, NULL) == TRUE) {
                    unset($_SESSION['hasError']);
                    unset($_SESSION['oldData']);
                    unset($_SESSION['listOfErrors']);

                    foreach ($params as $name => $value) {
                        $value = htmlentities(trim($value));
                        if ($name == 'password') {
                            if (!empty($value)) {
                                $value = password_hash($value, PASSWORD_BCRYPT);
                            } else {
                                $value = $user->getPassword();
                            }
                        }
                        if ($name != 'captcha' && $name != 'password_confirmation' && $name != 'cgu' && $name != 'csrf' && $name != "role" && $name != "avatar") {
                            if ($user->getId()) {
                                $user->{"set" . ucfirst($name)}($value);
                                $user->setIdRole($params['role']);
                                if (empty($_FILES["avatar"]["name"])) {
                                    $user->setAvatar($user->getAvatar());
                                } else {
                                    $user->setAvatar($_SESSION['data_array_image']);
                                }
                                $user->save();
                            }
                        }
                    }
                    unset($_SESSION['data_array_image']);
                    $_SESSION['listOfSuccess'][] = "Utilisateur modifié";
                    header('Location: ../edituser/' . intval($user->getId()));
                    exit();
                } else {
                    // Sends errors back to the home page
                    $_SESSION['hasError'] = 1;
                    // Sends data sent on form back to the home page
                    foreach ($params as $name => $value) {
                        if ($name != 'captcha' && $name != 'password_confirmation' && $name != 'cgu' && $name != 'csrf' && $name != "role" && $name != "avatar") {
                            $_SESSION['oldData'][$name] = $value;
                        }
                    }
                }
            }
        }


    }

    public function delImgAction($params)
    {
        $v = new view("admin/user", "backend");
        $v->assign("ActionPage", "delimg");
        $user = new User();
        $user = $user->populate(['id' => intval($params[0])]);
        if ($user->getId() == NULL) {
            header('Location: ../userlist');
            exit();
        }
        if ($user->getAvatar() != NULL) {
            $user->setAvatar("NULL");
            $user->save();
            $_SESSION['listOfSuccess'][] = "Image supprimé";
            header('Location: ../edituser/' . intval($params[0]));
            exit();
        }
        $_SESSION['listOfSuccess'][] = "Image supprimé";
        header('Location: ../edituser/' . intval($params[0]));
        exit();

    }

    public function listUsersAction($params)
    {
        $v = new view("admin/listUsers", "backend");
        $user = new User();

        $get_user = $user->getElements();

        $user->selectSQL(PREFIX . "role.name");
        $user->fromSQL("role");
        $user->joinSQL("id_role", "role", "id");
        $get_user_role = $user->executeSQL();
        $v->assign("get_user", $get_user);
        $v->assign("get_role_user", $get_user_role);
    }

    public function listPagesAction($params)
    {
        $v = new view("admin/listPages", "backend");

        $content = new Content();

        $get_content = $content->getElements(NULL, "type", "'page'");

        $content->selectSQL(PREFIX . "user.username");
        $content->fromSQL("user");
        $content->joinSQL("id_user", "user", "id");
        $get_user_name = $content->executeSQL();
        $v->assign("get_content", $get_content);
        $v->assign("get_user_name", $get_user_name);
    }

    public function addPageAction($params)
    {
        $v = new view("admin/page", "backend");
        $v->assign("ActionPage", "add");


        $form = new AddPageAdmin();
        $v->assign("addAdminPage_form", $form->addPage());

        if (isset($_POST['addPage'])) {
            if (empty($params['title']) && empty($params['content'])) {
                $_SESSION['listOfErrors'][] = "Champs vide";
                header("Location: " . ROOT_FOLDER . "admin/addpage");
                exit();
            } else if (strlen($params['title']) < 3 || strlen($params['title']) > 50) {
                $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
                header("Location: " . ROOT_FOLDER . "admin/listpages");
                exit();
            } else {
                unset($_SESSION['hasError']);
                unset($_SESSION['oldData']);
                unset($_SESSION['listOfErrors']);
                $content = new Content();
                $content->setIdUser(1);
                $content->setTitle(htmlentities($params['title']));
                $content->setContent(htmlentities($params['content']));
                $content->setType("page");
                $content->setImage("null");
                $content->setStatus(1);
                $string = strtolower($content->getTitle());
                $string = preg_replace("/[^a-z0-9_\s-]/", "", $content->getTitle());
                //Clean up multiple dashes or whitespaces
                $string = preg_replace("/[\s-]+/", " ", $content->getTitle());
                //Convert whitespaces and underscore to dash
                $string = preg_replace("/[\s_]/", "-", $content->getTitle());
                $content->setSlug($string);
                $content->save();
                $_SESSION['listOfSuccess'][] = "page correctement ajouté";
                header("Location: " . ROOT_FOLDER . "admin/listpages");
                exit();
            }
        }


    }

    public function editPageAction($params)
    {
        $v = new view("admin/page", "backend");
        $v->assign("ActionPage", "edit");

        $form = new EditPageAdmin();
        $content = new Content();
        $content_get = $content->getElements(NULL, "id", $params[0]);

        $v->assign("editAdminPage_form", $form->editPage($content_get[0]));
        if ($content_get != NULL) {
            if (isset($_POST['editPage'])) {
                if (empty($params['title']) && empty($params['content'])) {
                    $_SESSION['listOfErrors'][] = "Champs vide";
                    header("Location: " . ROOT_FOLDER . "admin/addpage");
                    exit();
                } else if (strlen($params['title']) < 3 || strlen($params['title']) > 50) {
                    $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
                    header("Location: " . ROOT_FOLDER . "admin/listpages");
                    exit();
                } else {
                    unset($_SESSION['hasError']);
                    unset($_SESSION['oldData']);
                    unset($_SESSION['listOfErrors']);
                    $content = $content->populate(["id" => $params[0]]);
                    if ($content->getId()) {
                        $content->setIdUser(1);
                        $content->setTitle(htmlentities($params['title']));
                        $content->setContent(htmlentities($params['content']));
                        $content->setImage("null");
                        $content->setStatus(1);
                        $string = strtolower($content->getTitle());
                        $string = preg_replace("/[^a-z0-9_\s-]/", "", $content->getTitle());
                        //Clean up multiple dashes or whitespaces
                        $string = preg_replace("/[\s-]+/", " ", $content->getTitle());
                        //Convert whitespaces and underscore to dash
                        $string = preg_replace("/[\s_]/", "-", $content->getTitle());
                        $content->setSlug($string);
                        $content->save();
                        $_SESSION['listOfSuccess'][] = "page correctement ajouté";
                        header("Location: " . ROOT_FOLDER . "admin/listpages");
                        exit();
                    }

                }
            }
        } else {
            $_SESSION['listOfErrors'][] = "Error ID";
            header("Location: " . ROOT_FOLDER . "admin/listpages");
            exit();
        }


    }

    public function deletePageAction($params)
    {
        $v = new view("admin/deletePage", "backend");

        $content = new Content();
        $content = $content->populate(['id' => $params[0]]);
        if ($content->getId() != NULL) {
            $content->setStatus(0);
            $content->save();
            $_SESSION['listOfSuccess'][] = "page correctement supprimé";
            header("Location: " . ROOT_FOLDER . "admin/listpages");
            exit();
        }


    }

    public function listNewsAction($params)
    {
        $v = new view("admin/listNews", "backend");

        $content = new Content();

        $get_news = $content->getElements(NULL, "type", "'news'");

        $content->selectSQL(PREFIX . "user.username");
        $content->fromSQL("user");
        $content->joinSQL("id_user", "user", "id");
        $get_user_name = $content->executeSQL();
        $v->assign("get_news", $get_news);
        $v->assign("get_user_name", $get_user_name);
    }

    public function addNewsAction($params)
    {
        $v = new view("admin/news", "backend");
        $v->assign("ActionPage", "add");

        $form = new AddNewsAdmin();
        $v->assign("addAdminNews_form", $form->addNews());

        if (isset($_POST['addPage'])) {

            if (empty($params['title']) && empty($params['content'])) {
                $_SESSION['listOfErrors'][] = "Champs vide";
                header("Location: " . ROOT_FOLDER . "admin/addnews");
                exit();
            } else if (strlen($params['title']) < 3 || strlen($params['title']) > 50) {
                $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
                header("Location: " . ROOT_FOLDER . "admin/listpages");
                exit();
            } else {

                if (CoreHelper::uploadImage($_FILES, NULL) == TRUE) {
                    unset($_SESSION['hasError']);
                    unset($_SESSION['oldData']);
                    unset($_SESSION['listOfErrors']);
                    $content = new Content();
                    $content->setIdUser(1);
                    $content->setTitle(htmlentities($params['title']));
                    $content->setContent(htmlentities($params['content']));
                    $content->setType("news");
                    if (empty($_FILES["avatar"]["name"])) {
                        $content->setImage($content->getImage());
                    } else {
                        $content->setImage($_SESSION['data_array_image']);
                    }
                    $content->setStatus(1);
                    $content->setCategory(htmlentities($params['category']));
                    $string = strtolower($content->getTitle());
                    $string = preg_replace("/[^a-z0-9_\s-]/", "", $content->getTitle());
                    //Clean up multiple dashes or whitespaces
                    $string = preg_replace("/[\s-]+/", " ", $content->getTitle());
                    //Convert whitespaces and underscore to dash
                    $string = preg_replace("/[\s_]/", "-", $content->getTitle());
                    $content->setSlug("news/show/" . $content->getCategory() . "/" . $string);

                    $content->save();
                    unset($_SESSION['data_array_image']);
                    $_SESSION['listOfSuccess'][] = "news correctement ajouté";
                    header("Location: " . ROOT_FOLDER . "admin/listnews");
                    exit();
                } else {
                    // Sends errors back to the home page
                    $_SESSION['hasError'] = 1;
                    // Sends data sent on form back to the home page
                    foreach ($params as $name => $value) {
                        $_SESSION['oldData'][$name] = $value;

                    }
                }

            }
        }

    }

    public function editNewsAction($params)
    {
        $v = new view("admin/news", "backend");
        $v->assign("ActionPage", "edit");

        $form = new EditNewsAdmin();
        $content = new Content();
        $content_get = $content->getElements(NULL, "id", $params[0]);

        $v->assign("editAdminNews_form", $form->editNews($content_get[0]));
        if ($content_get != NULL) {
            if (isset($_POST['editNews'])) {
                if (empty($params['title']) && empty($params['content'])) {
                    $_SESSION['listOfErrors'][] = "Champs vide";
                    header("Location: " . ROOT_FOLDER . "admin/editnews/" . $params[0]);
                    exit();
                } else if (strlen($params['title']) < 3 || strlen($params['title']) > 50) {
                    $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
                    header("Location: " . ROOT_FOLDER . "admin/editnews/" . $params[0]);
                    exit();
                } else {
                    if (CoreHelper::uploadImage($_FILES, NULL) == TRUE) {
                        unset($_SESSION['hasError']);
                        unset($_SESSION['oldData']);
                        unset($_SESSION['listOfErrors']);
                        $content = $content->populate(["id" => $params[0]]);
                        if ($content->getId()) {
                            $content->setIdUser(1);
                            $content->setTitle(htmlentities($params['title']));
                            $content->setContent(htmlentities($params['content']));
                            if (empty($_FILES["avatar"]["name"])) {
                                $content->setImage($content->getImage());
                            } else {
                                $content->setImage($_SESSION['data_array_image']);
                            }
                            $content->setStatus(1);
                            $content->setCategory(htmlentities($params['category']));
                            $string = strtolower($content->getTitle());
                            $string = preg_replace("/[^a-z0-9_\s-]/", "", $content->getTitle());
                            //Clean up multiple dashes or whitespaces
                            $string = preg_replace("/[\s-]+/", " ", $content->getTitle());
                            //Convert whitespaces and underscore to dash
                            $string = preg_replace("/[\s_]/", "-", $content->getTitle());
                            $content->setSlug("news/show/" . $content->getCategory() . "/" . $string);
                            $content->save();
                            $_SESSION['listOfSuccess'][] = "news correctement modifié";
                            header("Location: " . ROOT_FOLDER . "admin/listnews");
                            exit();
                        }
                    } else {
                        // Sends errors back to the home page
                        $_SESSION['hasError'] = 1;
                        // Sends data sent on form back to the home page
                        foreach ($params as $name => $value) {
                            $_SESSION['oldData'][$name] = $value;

                        }
                    }
                }
            }
        } else {
            $_SESSION['listOfErrors'][] = "Error ID";
            header("Location: " . ROOT_FOLDER . "admin/listnews");
            exit();
        }

    }


    public function deleteNewsAction($params)
    {
        $v = new view("admin/deleteNews", "backend");

        $content = new Content();
        $content = $content->populate(['id' => $params[0]]);
        if ($content->getId() != NULL) {
            $content->setStatus(0);
            $content->save();
            $_SESSION['listOfSuccess'][] = "news correctement supprimé";
            header("Location: " . ROOT_FOLDER . "admin/listnews");
            exit();
        }

    }

    public function listEventsAction($params)
    {
        $v = new view("admin/listEvents", "backend");

        $content = new Content();

        $get_events = $content->getElements(NULL, "type", "'events'");

        $content->selectSQL(PREFIX . "user.username");
        $content->fromSQL("user");
        $content->joinSQL("id_user", "user", "id");
        $get_user_name = $content->executeSQL();
        $v->assign("get_events", $get_events);
        $v->assign("get_user_name", $get_user_name);
    }

    public function addEventAction($params)
    {
        $v = new view("admin/event", "backend");
        $v->assign("ActionPage", "add");

        $form = new AddEventsAdmin();
        $v->assign("addAdminEvents_form", $form->addEvents());

        if (isset($_POST['addEvents'])) {

            if (empty($params['title']) && empty($params['content'])) {
                $_SESSION['listOfErrors'][] = "Champs vide";
                header("Location: " . ROOT_FOLDER . "admin/addnews");
                exit();
            } else if (strlen($params['title']) < 3 || strlen($params['title']) > 50) {
                $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
                header("Location: " . ROOT_FOLDER . "admin/listevents");
                exit();
            } else {

                if (CoreHelper::uploadImage($_FILES, NULL) == TRUE) {
                    unset($_SESSION['hasError']);
                    unset($_SESSION['oldData']);
                    unset($_SESSION['listOfErrors']);
                    $content = new Content();
                    $content->setIdUser(1);
                    $content->setTitle(htmlentities($params['title']));
                    $content->setContent(htmlentities($params['content']));
                    $content->setType("events");
                    if (empty($_FILES["avatar"]["name"])) {
                        $content->setImage($content->getImage());
                    } else {
                        $content->setImage($_SESSION['data_array_image']);
                    }
                    $content->setStatus(1);
                    $content->setCategory(htmlentities($params['category']));
                    $string = strtolower($content->getTitle());
                    $string = preg_replace("/[^a-z0-9_\s-]/", "", $content->getTitle());
                    //Clean up multiple dashes or whitespaces
                    $string = preg_replace("/[\s-]+/", " ", $content->getTitle());
                    //Convert whitespaces and underscore to dash
                    $string = preg_replace("/[\s_]/", "-", $content->getTitle());
                    $content->setSlug("event/show/" . $content->getCategory() . "/" . $string);
                    $content->save();
                    unset($_SESSION['data_array_image']);
                    $_SESSION['listOfSuccess'][] = "evenement correctement ajouté";
                    header("Location: " . ROOT_FOLDER . "admin/listevents");
                    exit();
                } else {
                    // Sends errors back to the home page
                    $_SESSION['hasError'] = 1;
                    // Sends data sent on form back to the home page
                    foreach ($params as $name => $value) {
                        $_SESSION['oldData'][$name] = $value;

                    }
                }

            }
        }
    }

    public function editEventAction($params)
    {
        $v = new view("admin/event", "backend");
        $v->assign("ActionPage", "edit");

        $form = new EditEventsAdmin();
        $content = new Content();
        $content_get = $content->getElements(NULL, "id", $params[0]);


        $v->assign("editAdminEvents_form", $form->editEvents($content_get[0]));
        if ($content_get != NULL) {
            if (isset($_POST['editNews'])) {
                if (empty($params['title']) && empty($params['content'])) {
                    $_SESSION['listOfErrors'][] = "Champs vide";
                    header("Location: " . ROOT_FOLDER . "admin/editevent/" . $params[0]);
                    exit();
                } else if (strlen($params['title']) < 3 || strlen($params['title']) > 50) {
                    $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
                    header("Location: " . ROOT_FOLDER . "admin/editevent/" . $params[0]);
                    exit();
                } else {
                    if (CoreHelper::uploadImage($_FILES, NULL) == TRUE) {
                        unset($_SESSION['hasError']);
                        unset($_SESSION['oldData']);
                        unset($_SESSION['listOfErrors']);
                        $content = $content->populate(["id" => $params[0]]);
                        if ($content->getId()) {
                            $content->setIdUser(1);
                            $content->setTitle(htmlentities($params['title']));
                            $content->setContent(htmlentities($params['content']));
                            if (empty($_FILES["avatar"]["name"])) {
                                $content->setImage($content->getImage());
                            } else {
                                $content->setImage($_SESSION['data_array_image']);
                            }
                            $content->setStatus(1);
                            $content->setCategory(htmlentities($params['category']));
                            $string = strtolower($content->getTitle());
                            $string = preg_replace("/[^a-z0-9_\s-]/", "", $content->getTitle());
                            //Clean up multiple dashes or whitespaces
                            $string = preg_replace("/[\s-]+/", " ", $content->getTitle());
                            //Convert whitespaces and underscore to dash
                            $string = preg_replace("/[\s_]/", "-", $content->getTitle());
                            $content->setSlug("news/show/" . $content->getCategory() . "/" . $string);
                            $content->save();
                            $_SESSION['listOfSuccess'][] = "news correctement modifié";
                            header("Location: " . ROOT_FOLDER . "admin/listevents");
                            exit();
                        }
                    } else {
                        // Sends errors back to the home page
                        $_SESSION['hasError'] = 1;
                        // Sends data sent on form back to the home page
                        foreach ($params as $name => $value) {
                            $_SESSION['oldData'][$name] = $value;

                        }
                    }
                }
            }
        } else {
            $_SESSION['listOfErrors'][] = "Error ID";
            header("Location: " . ROOT_FOLDER . "admin/listevents");
            exit();
        }

    }

    public function deleteEventAction($params)
    {
        $v = new view("admin/deleteEvents", "backend");

        $content = new Content();
        $content = $content->populate(['id' => $params[0]]);
        if ($content->getId() != NULL) {
            $content->setStatus(0);
            $content->save();
            $_SESSION['listOfSuccess'][] = "evenement correctement supprimé";
            header("Location: " . ROOT_FOLDER . "admin/listevents");
            exit();
        }

    }

    public function listBannedAction($params)
    {
        $v = new view("admin/listBanned", "backend");

        $user = new User();

        $get_user = $user->getElements(NULL, "is_banned", 1);
        $user->selectSQL(PREFIX . "role.name");
        $user->fromSQL("role");
        $user->joinSQL("id_role", "role", "id");
        $get_user_role = $user->executeSQL();
        $v->assign("get_user", $get_user);
        $v->assign("get_role_user", $get_user_role);
    }

    public function banUserAction($params)
    {
        $v = new view("admin/banUser", "backend");
        $user = new User();
        $user = $user->populate(['id' => $params[0]]);
        if ($user->getId() != NULL) {
            $user->setIsBanned(1);
            $user->save();
            $_SESSION['listOfSuccess'][] = "utilisateur correctement banni";
            header("Location: " . ROOT_FOLDER . "admin/listbanned");
            exit();
        }
    }

    public function unBanUserAction($params)
    {
        $v = new view("admin/unBanUser", "backend");
        $user = new User();
        $user = $user->populate(['id' => $params[0]]);
        if ($user->getId() != NULL) {
            $user->setIsBanned(0);
            $user->save();
            $_SESSION['listOfSuccess'][] = "utilisateur correctement débanni";
            header("Location: " . ROOT_FOLDER . "admin/listbanned");
            exit();
        }
    }

    public function listTeamsAction($params)
    {
        $v = new view("admin/listTeams", "backend");
    }

    public function editTeamAction($params)
    {
        $v = new view("admin/team", "backend");

        $v->assign("ActionPage", "edit", "backend");
    }

    public function addTeamAction($params)
    {
        $v = new view("admin/team", "backend");

        $v->assign("ActionPage", "add");
    }

    public function listGamesAction($params)
    {

        $v = new view("admin/listGames", "backend");

        $game_list = new Game_list();
        $form = new AddGameAdmin();

        $get_game_liste = $game_list->getElements();
        $v->assign("addAdminGames_form", $form->addGame());
        $v->assign("get_game_liste", $get_game_liste);

    }


    public function editGameAction($params)
    {
        $v = new view("admin/game", "backend");
        $v->assign("ActionPage", "edit");
    }

    public function addGameAction($params)
    {
        $v = new view("admin/game", "backend");
        $v->assign("ActionPage", "add");

        $gameList = new Game_list();
        if (empty($params['title'])) {
            $_SESSION['listOfErrors'][] = "Champs vide";
            header("Location: " . ROOT_FOLDER . "admin/listGames");
            exit();
        } else if (strlen($params['title']) < 3 || strlen($params['title']) > 15) {
            $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
            header("Location: " . ROOT_FOLDER . "admin/listGames");
            exit();
        } else {
            if (CoreHelper::uploadImage($_FILES, NULL) == TRUE) {
                unset($_SESSION['hasError']);
                unset($_SESSION['oldData']);
                unset($_SESSION['listOfErrors']);
                $gameList->setIdUser(1);
                $gameList->setName($params['title']);
                if (empty($_FILES["avatar"]["name"])) {
                    $gameList->setImage($gameList->getImage());
                } else {
                    $gameList->setImage($_SESSION['data_array_image']);
                }
                $gameList->setStatus(1);
                $gameList->save();
                $_SESSION['listOfSuccess'][] = "Jeu correctement ajouté";
                header("Location: " . ROOT_FOLDER . "admin/listGames");
                exit();
            } else {
                // Sends errors back to the home page
                $_SESSION['hasError'] = 1;
                // Sends data sent on form back to the home page
                foreach ($params as $name => $value) {
                    $_SESSION['oldData'][$name] = $value;

                }
            }
        }

    }


    public function challengeTeamAction($params)
    {
        $v = new view("admin/challengeTeam", "backend");
    }


    public function maintenanceAction($params)
    {
        $v = new view("admin/maintenance", "backend");
    }

    public function generalInfoAction($params)
    {
        $v = new view("admin/configuration", "backend");
    }

    public function appareanceAction($params)
    {
        $v = new view("admin/appareance", "backend");
    }

    public function addonsAction($params)
    {
        $v = new view("admin/addons", "backend");
    }

    public function monitoringAction($params)
    {
        $v = new view("admin/monitoring", "backend");
    }

    public function aboutAction($params)
    {
        $v = new view("admin/about", "backend");
    }

    public function supportAction($params)
    {
        $v = new view("admin/support", "backend");
    }

    public function listRolesAction($params)
    {
        $v = new view("admin/listRoles", "backend");

        $role = new Role();
        $form = new AddRoleAdmin();

        $get_role = $role->getElements();
        $v->assign("addAdminRole_form", $form->addRole());
        $v->assign("get_role", $get_role);

    }

    public function addRolesAction($params)
    {

        $role = new Role();
        if (empty($params['role'])) {
            $_SESSION['listOfErrors'][] = "Champs vide";
            header("Location: " . ROOT_FOLDER . "admin/listroles");
            exit();
        } else if (strlen($params['role']) < 3 || strlen($params['role']) > 15) {
            $_SESSION['listOfErrors'][] = "Le nom du champs rôle doit être supérieur à 15 et inférieur à 3";
            header("Location: " . ROOT_FOLDER . "admin/listroles");
            exit();
        } else {
            unset($_SESSION['hasError']);
            unset($_SESSION['oldData']);
            unset($_SESSION['listOfErrors']);
            $role->setName($params['role']);
            $role->setStatus(1);
            $role->save();
            $_SESSION['listOfSuccess'][] = "Role correctement ajouté";
            header("Location: " . ROOT_FOLDER . "admin/listroles");
            exit();
        }
    }

    public function editRolesAction($params)
    {
        $v = new view("admin/editRoles", "backend");

        $form = new EditRoleAdmin();
        $role = new Role();

        $role_get = $role->getElements(NULL, "id", $params[0]);
        $v->assign("editAdminRole_form", $form->editRole($role_get[0]));
        if ($role_get != NULL) {
            $role = $role->populate(["id" => $params[0]]);
            if (isset($_POST['editRole'])) {
                if ($role->getId()) {
                    $role->setName($params['role']);
                    $role->save();
                    $_SESSION['listOfSuccess'][] = "Role correctement modifié";
                    header("Location: " . ROOT_FOLDER . "admin/listroles");
                    exit();
                }
            }
        } else {
            $_SESSION['listOfErrors'][] = "l'id n'existe pas";
            header("Location: " . ROOT_FOLDER . "admin/listroles");
            exit();
        }

    }


    public function displayRoleAction($params)
    {
        $v = new view("admin/displayRole", "backend");

        $form = new EditRoleAdmin();
        $role = new Role();

        $role_get = $role->getElements(NULL, "id", $params[0]);

        if ($role_get != NULL) {
            $role = $role->populate(["id" => $params[0]]);
            if ($role->getId()) {
                $role->setStatus(1);
                $role->save();
                $_SESSION['listOfSuccess'][] = "role affiché";
                header("Location: " . ROOT_FOLDER . "admin/listroles");
                exit();
            }
        } else {
            $_SESSION['listOfErrors'][] = "l'id n'existe pas";
            header("Location: " . ROOT_FOLDER . "admin/listroles");
            exit();
        }


    }

    public function unDisplayRoleAction($params)
    {
        $v = new view("admin/unDisplayrole", "backend");
        $form = new EditRoleAdmin();
        $role = new Role();

        $role_get = $role->getElements(NULL, "id", $params[0]);

        if ($role_get != NULL) {
            $role = $role->populate(["id" => $params[0]]);
            if ($role->getId()) {
                $role->setStatus(0);
                $role->save();
                $_SESSION['listOfSuccess'][] = "role caché";
                header("Location: " . ROOT_FOLDER . "admin/listroles");
                exit();
            }
        } else {
            $_SESSION['listOfErrors'][] = "l'id n'existe pas";
            header("Location: " . ROOT_FOLDER . "admin/listroles");
            exit();
        }


    }

    public function displayGameAction($params)
    {
        $v = new view("admin/displayGame", "backend");


        $role = new Game_list();

        $role_get = $role->getElements(NULL, "id", $params[0]);


        if ($role_get != NULL) {
            $role = $role->populate(["id" => $params[0]]);
            if ($role->getId()) {
                $role->setStatus(1);
                $role->save();
                $_SESSION['listOfSuccess'][] = "game affiché";
                header("Location: " . ROOT_FOLDER . "admin/listGames");
                exit();
            }
        } else {
            $_SESSION['listOfErrors'][] = "l'id n'existe pas";
            header("Location: " . ROOT_FOLDER . "admin/listGames");
            exit();
        }


    }

    public function unDisplayGameAction($params)
    {
        $v = new view("admin/unDisplayGame", "backend");

        $role = new Game_list();

        $role_get = $role->getElements(NULL, "id", $params[0]);

        if ($role_get != NULL) {
            $role = $role->populate(["id" => $params[0]]);
            if ($role->getId()) {
                $role->setStatus(0);
                $role->save();
                $_SESSION['listOfSuccess'][] = "game caché";
                header("Location: " . ROOT_FOLDER . "admin/listGames");
                exit();
            }
        } else {
            $_SESSION['listOfErrors'][] = "l'id n'existe pas";
            header("Location: " . ROOT_FOLDER . "admin/listGames");
            exit();
        }


    }

    public function deleteAction($view)
    {
        $v = new view("admin/delete", "backend");
        $v->assign("namePage", $view[0]);
        $v->assign("id", $view[1]);

        $var = ucfirst($view[0]);
        ${$view[0]} = new $var();

        ${$view[0]} = ${$view[0]}->populate(['id' => $view[1]]);
        if (${$view[0]}->getId() != NULL) {
            ${$view[0]}->setIsDeleted(1);
            ${$view[0]}->save();
            $_SESSION['listOfSuccess'][] = $view[0] . " correctement supprimé";
            header("Location: " . ROOT_FOLDER . "admin/list" . $view[0] . "s");
            exit();
        }


    }

    public function SettingsAction($params)
    {
        $v = new view("admin/settings", "backend");
        $v->assign("ActionPage", "add");
        $form = new AddSettingsAdmin();
        $v->assign("adminSettings_form", $form->adminSettings());

        $config = new Config();
        unset($params['addUser']);
        foreach ($params as $key => $value) {
            $config->setWording($key);
            $config->setValue($value);
            $config->save();
        }

    }


}