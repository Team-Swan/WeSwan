<?php

class UserController{

    /**
     * @param $params
     * SignUp action -> Inserts an user
     */
    public function signUpAction($params)
    {
        $user = new User();

        if(UserHelper::validSignForm($user->getSignForm(), $params)) {
            unset($_SESSION['hasError']);
            unset($_SESSION['oldData']);
            unset($_SESSION['listOfErrors']);
            foreach($params as $name => $value) {
                $value = htmlentities(trim($value));
                if($name != 'captcha' && $name != 'password_confirmation' && $name != 'cgu' && $name != 'csrf') {
                    if($name == 'password') {
                        $value = password_hash($value, PASSWORD_BCRYPT);
                    }
                    $user->{"set" . ucfirst($name)}($value);
                    $user->setIdRole(1);
                }
            }
            $user->save();

            $mail = CoreHelper::mailSetup();
            $mail->addAddress($user->getEmail(), $user->getUsername());
            $mail->Subject = "Confirmation de ton compte sur WeSwan CMS";
            $mail->Body = "Salut ".$user->getUsername()."!<br><br>
            Bienvenue sur <a href='https://weswan.fr'>weswan.fr</a>!<br>
            Plus qu'une étape pour finaliser ton inscription, tu y es presques.<br><br>
            Clique sur ce lien pour valider ton compte:<br>
            <a href=\"https://weswan.fr/user/confirmPassword?token=".$user->getToken()."&email=".$user->getEmail()."\">Confirmer mon adresse e-mail</a><br><br>
            A bientôt,<br>
            Equipe WeSwan";

            $mail->AltBody = "Salut ".$user->getUsername()."!<br><br>
            Bienvenue sur https://weswan.fr .<br>
            Plus qu'une étape pour finaliser ton inscription, tu y es presques.<br><br>
            Copie ce lien dans ton navigateur pour confirmer ton adresse e-mail:<br>
            https://WeSwan.fr/user/confirmPassword?token=".$user->getToken()."&email=".$user->getEmail()."<br><br>
            A bientôt,<br>
            Equipe WeSwan";

            if(!$mail->send()) {
                $_SESSION['listOfErrors'][] = "Oups il y a eu un problème lors de l'envoie du mail!";
                header("Location: ".ROOT_FOLDER."");
                exit();
            }
            else {
                $_SESSION['listOfSuccess'][] = "Un mail d'inscription a été envoyé à ton adresse mail!";
                header("Location: ".ROOT_FOLDER."");
                exit();
            }
        } else {
            // Sends errors back to the home page
            $_SESSION['hasError'] = 1;
            // Sends data sent on form back to the home page
            foreach($params as $name => $value) {
                if ($name != 'captcha' && $name != 'password' && $name != 'password_confirmation' && $name != 'cgu') {
                    $_SESSION['oldData'][$name] = $value;
                }
            }
            header("Location: ".ROOT_FOLDER."");
        }

        exit();
    }

    public function confirmPasswordAction()
    {
        if(isset($_GET['token']) && isset($_GET['email'])) {
            $token = htmlentities($_GET['token']);
            $email = htmlentities($_GET['email']);
            if(!empty($token) && !empty($email)) {
                $user = new User();
                $userRequest = $user->populate(["token" => $token, "email" => $email]);
                if($userRequest && $token == $userRequest->getToken() && $email == $userRequest->getEmail()) {
                    $userRequest->setCertify(1);
                    $userRequest->save();
                    $_SESSION['listOfSuccess'][] = "Ton compte a bien été confirmé, tu peux maintenant te connecter!";
                    header('Location : '.ROOT_FOLDER);
                    exit();
                } else {
                    $view = new View("page404", "singlepage");
                }
            }  else {
                $view = new View("page404", "singlepage");
            }
        }
        else {
            $view = new View("page404", "singlepage");
        }
    }

    public function loginAction($params)
    {
        $user = new User();
        foreach ($params as $name => $value) {
            $value = htmlentities(trim($value));
            if ($name != 'captcha' && $name != 'csrf') {
                $user->{"set" . ucfirst($name)}($value);
            }
        }

        $logedUser = $user->populate(["username" => $user->getUsername()]);
        if ($logedUser && $logedUser->getId()) {
            if(password_verify($user->getPassword(), $logedUser->getPassword())) {
                if (!$logedUser->getCertify()) {
                    $_SESSION['listOfErrors'][] = "Tu ne pourras te connecter qu'après avoir confirmer ton adresse e-mail!";
                    header("Location: ".ROOT_FOLDER."");
                    exit();
                }
                $logedUser->setLastConnexionAt(date("Y-m-d h:i:s"));
                $logedUser->save();
                unset($_SESSION['listOfErrors']);
                $_SESSION['id'] = $logedUser->getId();
                $_SESSION['id_role'] = $logedUser->getIdRole();
                $_SESSION['username'] = $logedUser->getUsername();
                $_SESSION['email'] = $logedUser->getEmail();
                $_SESSION['avatar'] = $logedUser->getAvatar();
            } else {
                $_SESSION['listOfErrors'][] = "Pseudo ou mot-de-passe incorrect";
            }
        } else {
            $_SESSION['listOfErrors'][] = "Cet utilisateur n'existe pas";
        }

        header("Location: ".ROOT_FOLDER."");
        exit();
    }

    public function logoutAction($params)
    {
        session_destroy();
        header("Location: ".ROOT_FOLDER);
        exit();
    }


    public function resetAjaxAction($params)
    {
        foreach($params as $id => $val) {
            $params[$id] = htmlentities(trim($val));
        }
        $user = new User();
        $logedUser = $user->populate(["email" => $params['email']]);

        if($logedUser && $logedUser->getId() != -1) {
            $logedUser->setToken(sha1(uniqid(rand(), true)) . date('YmdHis'));
            $logedUser->save();

            $mail = CoreHelper::mailSetup();
            $mail->addAddress($logedUser->getEmail(), $logedUser->getUsername());
            $mail->Subject = "Renouveller ton mot-de-passe sur WeSwan CMS";
            $mail->Body = "Salut ".$logedUser->getUsername()."!<br><br>
            Une demande de renouvellement de ton mot de passe a été demandée sur <a href='https://weswan.fr'>weswan.fr</a>.<br>
            Si ce n'est pas toi, ne prends pas en compte ce message.<br>
            Sinon, clique sur le lien ci-dessous pour le changer.<br><br>
            <a href=\"https://weswan.fr/user/resetPassword?token=".$logedUser->getToken()."&email=".$logedUser->getEmail()."\">Renouveller mon mot-de-passe</a><br><br>
            A bientôt,<br>
            Equipe WeSwan";

            $mail->AltBody = "Salut ".$logedUser->getUsername()."!<br><br>
            Une demande de renouvellement de ton mot de passe a été demandée sur https://weswan.fr .<br>
            Si ce n'est pas toi, ne prends pas en compte ce message.<br>
            Sinon, copie ce lien dans ton navigateur pour le changer.<br><br>
            https://WeSwan.fr/user/resetPassword?token=".$logedUser->getToken()."&email=".$logedUser->getEmail()."<br><br>
            A bientôt,<br>
            Equipe WeSwan";

            if(!$mail->send()) {
                $_SESSION['listOfErrors'][] = "Oups il y a eu un problème lors de l'envoie du mail!";
                header("Location: ".ROOT_FOLDER."");
                exit();
            }
            else {
                $_SESSION['listOfSuccess'][] = "Un mail de redefinition a été envoyé à ton adresse mail!";
                header("Location: ".ROOT_FOLDER."");
                exit();
            }
        } else {
            $_SESSION['listOfErrors'][] = "Oups il y a eu un problème, vérifie le mail saisi";
            header("Location: ".ROOT_FOLDER."");
            exit();
        }
    }

    public function resetPasswordAction($params)
    {
        if(isset($_GET['token']) && isset($_GET['email'])) {
            $token = htmlentities($_GET['token']);
            $email = htmlentities($_GET['email']);
            if (!empty($token) && !empty($email)) {
                $user = new User();
                $userRequest = $user->populate(["token" => $token, "email" => $email]);
                if ($userRequest) {
                    if ($token == $userRequest->getToken() && $email == $userRequest->getEmail()) {
                        $view = new View("reset_password", "singlepage");
                        $view->assign("reset_form", $user->getResetForm());
                    } else {
                        $view = new View("page404", "singlepage");
                    }
                } else {
                    $view = new View("page404", "singlepage");
                }
            } else {
                $view = new View("page404", "singlepage");
            }
        }
    }

    public function resetedPasswordAction($params)
    {
        $user = new User();

        if (UserHelper::validSignForm($user->getResetForm(), $params)) {
            unset($_SESSION['hasError']);
            unset($_SESSION['oldData']);
            unset($_SESSION['listOfErrors']);
            foreach ($params as $name => $value) {
                $value = htmlentities(trim($value));
                ${$name} = $value;
            }
            if($email && $token && $password) {
                $user = $user->populate(['email' => $email]);
                if ($token == $user->getToken()) {
                    if ($user->getId()) {
                        $value = password_hash($password, PASSWORD_BCRYPT);
                        $user->setPassword($value);
                        $user->save();
                        $_SESSION['listOfSuccess'][] = "Mot de passe changé avec succès!";
                        header("Location: ".ROOT_FOLDER."");
                        exit();
                    } else {
                        $view = new View("page404", "singlepage");
                    }
                } else {
                    $view = new View("page404", "singlepage");
                }
            } else {
                $view = new View("page404", "singlepage");
            }
        } else {
            $view = new View("page404", "singlepage");
        }

    }

    public function profileAction($params)
    {

        if(isset($params[0])) {
            $search['username'] = $params[0];
        }
        if(empty($params)) {
            $search['username'] = $_SESSION['username'];
        }

        $user = new User();
        $user->selectSQL(PREFIX."user.*, ".PREFIX."role.name as role");
        $user->fromSQL("role");
        $user->whereSQL(PREFIX."user.username", $search['username']);
        $user->andJoinSQL("id_role", "role", "id");
        $user_p = $user->executeSQL()[0];

        $pm = new Private_message();
        $receiver = $_SESSION['id'];
        $pm->customSQL("
            select
               COUNT(id) as nb
            from
                ".PREFIX."private_message
            where
                id_receiver = :".$receiver."
        ", [':'.$receiver => $receiver]);
        $nb_msg_received = $pm->executeSQL();

        $pm->customSQL("
            select
               COUNT(id) as nb
            from
                ".PREFIX."private_message
            where
                id_sender = :".$receiver."
        ", [':'.$receiver => $receiver]);
        $nb_msg_sent = $pm->executeSQL();

        if($user_p['id']) {
            $view = new View("profile");
            $view->assign("user", $user_p);
            $view->assign("nb_msg_sent", $nb_msg_sent);
            $view->assign("nb_msg_received", $nb_msg_received);
        } else {
            $view = new View("page404", "singlepage");
        }


    }


    public function editAction($params)
    {

        $_SESSION['listOfErrors'] = [];
        if(isset($params['name']) && isset($params['user']) && isset($params['value'])) {
            $params['name'] = htmlentities(trim($params['name']));
            $params['user'] = htmlentities(trim($params['user']));
            if ($params['name'] == 'lastname' || $params['name'] == 'firstname' || $params['name'] == 'birthday' || $params['name'] == 'password') {
                $params['value'] = htmlentities(trim($params['value']));

                $user = new User();

                if ($params['user'] == $_SESSION['id']) {
                    $user = $user->populate(["id" => $params['user']]);
                    if($user->getId()) {
                        if(UserHelper::validSignForm($user->getSignForm(), $params)) {
                            if($params['name'] == 'password') {
                                $params['value'] = $value = password_hash($params['value'], PASSWORD_BCRYPT);
                            }
                            $user->{"set" . ucfirst($params['name'])}($params['value']);
                            $user->save();
                            if($params['name'] == 'birthday') {
                                echo CoreHelper::getShortdate($user->{"get" . ucfirst($params['name'])}());
                            } else {
                                if($params['name'] == 'password') {
                                    echo "redirect";
                                } else {
                                    echo $user->{"get" . ucfirst($params['name'])}();
                                }
                            }
                            return 0;
                        }
                    }
                }
            }

        }
        $_SESSION['listOfErrors'][] = "Oups il y a eu un problème";
        header("Location: ".ROOT_FOLDER."/user/profile/");
        exit();
    }

    public function conpageAction($params)
    {
        $view = new View("connexion");
        $user = new User();
        $view->assign("login_form", $user->getLoginForm());
    }

    public function inspageAction($params)
    {
        $view = new View("inscription");
        $user = new User();
        $view->assign("sign_form", $user->getSignForm());
    }

    public function likeStreamerAction($params)
    {

    }

    public function followAction($params)
    {
        // Not a priority
    }


    /**********************
    ADMIN
     **********************/

// Might need his own Controller?
    public function getSessionsAction($params)
    {

    }

// Might need his own Controller?
    public function getPermissionsAction($params)
    {

    }

}