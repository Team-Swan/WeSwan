<?php

class CommentController{

    public function addAction($params){
        if(isset($_SESSION['id'])) {
            $id = $params[0];
            $comment = htmlentities($params['comment']);

            if ($id && $comment) {
                if (strlen($comment) > 3 && strlen($comment) < 500) {
                    $com = new Comment();
                    $com->setIdUser($_SESSION['id']);
                    $com->setComment($comment);
                    $com->setIdContent($id);
                    $com->save();

                    $article = new Content();
                    $article = $article->populate(["id" => $id]);

                    header("Location: " . ROOT_FOLDER . "" . $article->getSlug());
                    exit();
                }
            }
        } else {
            $view = new View("page404", "singlepage");
        }
    }
}