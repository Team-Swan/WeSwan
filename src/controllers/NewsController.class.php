<?php

class NewsController {

    public function indexAction($params)
    {
        $view = new View("news");

        $news = new Content();
        $limit = 2;

        $page = ($params['page']-1) * $limit;

        $news->selectSQL(PREFIX."content.*, ".PREFIX."user.username");
        $news->fromSQL("user");
        $news->whereSQL("type", "news");
        $news->andJoinSQL("id_user", "user", "id");
        $news->limitSQL($page, $limit);
        $tab_news = $news->executeSQL();
        $view->assign("news", $tab_news);

        $news->selectSQL('DISTINCT category');
        $news->fromSQL();
        $categories = $news->executeSQL();
        $view->assign("categories", $categories);

        $view->assign("page", $params['page']);

    }

    public function showAction($params)
    {
        $link = "news/show/".urldecode($params[0])."/".urldecode($params[1]);


        $article = new Content();
        $user = new User();

        $article = $article->populate(['slug' => $link]);
        $user = $user->populate(['id' => $article->getIdUser()]);

        $comment = new Comment();
        $comment->selectSQL(PREFIX."comment.*, ".PREFIX."user.username, ".PREFIX."user.avatar");
        $comment->fromSQL("user");
        $comment->whereSQL("id_content", $article->getId());
        $comment->andJoinSQL("id_user", "user", "id");
        $comment->orderSQL("created_at");
        $comments = $comment->executeSQL();

        if($article && $article->getId()) {
            $view = new View("article");
            $view->assign("article", $article);
            $view->assign("user", $user);
            $view->assign("comments", $comments);
            $view->assign("comment_form", $comment->getCommentForm());
        } else {
            $view = new View("page404", "singlepage");
        }
    }

    public function searchAction($params)
    {

    }

    public function indexCategoryAction($params)
    {

    }

    public function showCategoryAction($params)
    {

    }

    public function searchCategoryAction($params)
    {

    }

    public function addCommentAction($params)
    {

    }

    public function deleteCommentAction($params)
    {

    }


}