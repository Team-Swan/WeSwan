<?php

class MessagerieController
{
    public function indexAction($params)
    {
        if(isset($_SESSION['id'])) {
            $view = new View("messagerie");

            $pm = new Private_message();
            $pm->selectSQL(PREFIX . "private_message.*, we_from_user.username as sender, we_to_user.username as received");
            $pm->fromSQL("user we_from_user, " . PREFIX . "user we_to_user");
            $pm->whereSQL(PREFIX . "private_message.id_receiver", $_SESSION['id']);
            $pm->andJoinSQL("id_sender", "from_user", "id");
            $pm->andJoinSQL("id_receiver", "to_user", "id");
            $pm->limitSQL(10);
            $messages = $pm->executeSQL();
            $view->assign("messages_r", $messages);
            $view->assign("write_pm", $pm->getMessageForm());

            $pm->selectSQL(PREFIX . "private_message.*, we_from_user.username as sender, we_to_user.username as received");
            $pm->fromSQL("user we_from_user, " . PREFIX . "user we_to_user");
            $pm->whereSQL(PREFIX . "private_message.id_sender", $_SESSION['id']);
            $pm->andJoinSQL("id_sender", "from_user", "id");
            $pm->andJoinSQL("id_receiver", "to_user", "id");
            $pm->limitSQL(10);
            $messages = $pm->executeSQL();
            $view->assign("messages_s", $messages);
        } else {
            $view = new View("page404", "singlepage");
        }

    }


    public function addAction($params)
    {
        if (isset($_SESSION['id'])) {
            $pm = new Private_message();
            $user = new User();

            $title = htmlentities($params['objet']);
            $content = htmlentities($params['content']);
            $sender = htmlentities($params['destinataire']);

            $user = $user->populate(['username' => $sender]);
            if ($user && $user->getId()) {
                if (strlen($content) < 5 || strlen($title) < 3) {
                    $_SESSION['listOfErrors'][] = "Le message ou l'objet est trop court pour être envoyé!";
                    header('Location: ' . ROOT_FOLDER . "messagerie");
                    exit();
                } else {
                    $pm->setTitle($title);
                    $pm->setContent($content);
                    $pm->setIdReceiver($user->getId());
                    $pm->setIdSender($_SESSION['id']);
                    $pm->save();
                    $_SESSION['listOfSuccess'][] = "Le message a bien été envoyé!";
                    header('Location: ' . ROOT_FOLDER . "messagerie");
                    exit();
                }
            } else {
                $_SESSION['listOfErrors'][] = "Le pseudo du destinataire n'existe pas";
                header('Location: ' . ROOT_FOLDER . "messagerie");
                exit();
            }
        } else {
            $view = new View("page404", "singlepage");
        }
    }

        public function suggestEventAction($params)
    {

    }

        public function filterEventsAction($params)
    {

    }
    }