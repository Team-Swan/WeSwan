<?php

class TeamController {

    public function indexAction($params)
    {
        $params[0] = htmlentities(urldecode($params[0]));
        $view = new View("team");
        $team = new Team();


        $team = $team->populate(['name' => $params[0]]);

        $setdata = [
                ":".$team->getId() => $team->getId(),
            ];

        $team->customSQL("
            select
                ".PREFIX."user.username,
                ".PREFIX."user.avatar,
                ".PREFIX."team_user.role
            from
                ".PREFIX."user,
                ".PREFIX."team_user
            where
                ".PREFIX."user.id = ".PREFIX."team_user.id_user and
                ".PREFIX."team_user.id_team = :".$team->getId()."
            order by
                role
        ", $setdata);

        $players = $team->executeSQL();




        $setdata = [
            ":".$team->getId() => $team->getId(),
        ];

        $team->customSQL("
            select
                ".PREFIX."user.username,
                ".PREFIX."user.avatar
            from
                ".PREFIX."team_user_demand,
                ".PREFIX."user
            where
                ".PREFIX."user.id = ".PREFIX."team_user_demand.id_user and
                ".PREFIX."team_user_demand.id_team = :".$team->getId()."
        ", $setdata);

        $demands = $team->executeSQL();

        if($team->getId()) {
            $view->assign("team", $team);
            $view->assign("join_team", $team->getApllyForm());
            $view->assign("players", $players);
            $view->assign("demands", $demands);
        }
    }
    public function acceptAction($params)
    {
        $team_t = new Team();
        $user_t = new User();

        $user = htmlentities($params[0]);
        $team = htmlentities($params[1]);

        $team_t = $team_t->populate(['id' => $team]);
        $user_t = $user_t->populate(['username' => $user]);

        if($team_t->getLeader() == $_SESSION['id']) {
            $team_t->customSQL("
                    SELECT 
                        COUNT(" . PREFIX . "team_user_demand.id) as nb 
                    FROM
                        " . PREFIX . "team_user_demand
                    WHERE
                        id_user = :".$user_t->getId(). " and 
                        id_team = :".$team. "
                ", [
                ":" . $user_t->getId() => $user_t->getId(),
                ":" . $team => $team
            ]);
            $result = $team_t->executeSQL();

            if ($result[0]['nb']) {
                $team_t->customSQL("
                    DELETE FROM " . PREFIX . "team_user_demand
                    WHERE
                        id_user = :" . $user_t->getId() . " and 
                        id_team = :" . $team . "
                ", [
                    ":" . $user_t->getId() => $user_t->getId(),
                    ":" . $team => $team
                ]);
                $team_t->executeSQL();

                $role = "Membre";
                $status = "1";

                $team_t->customSQL("
                    INSERT INTO " . PREFIX . "team_user (id_user, id_team, role, status)
                    VALUES (:".$user_t->getId()." , :".$team.", :".$role.", :".$status.")
                ", [
                    ":".$user_t->getId() => $user_t->getId(),
                    ":".$team => $team,
                    ":".$role => $role,
                    ":".$status => $status
                ]);
                $team_t->executeSQL();

                $_SESSION['listOfSuccess'][] = "L'utilisateur a bien été ajouté à ton équipe!";
                header("Location: ".ROOT_FOLDER."/team/index/".$team_t->getName());
                exit();
            } else {
                $_SESSION['listOfErrors'][] = "Cet utilisateur n'est pas dans la liste d'attente!";
                header("Location: ".ROOT_FOLDER."/team/index/".$team_t->getName());
                exit();
            }
        } else {
            $view = new View("page404", "singlepage");
        }
    }

    public function updateTeamAction($params)
    {

    }

    public function deleteTeamAction($params)
    {

    }

    public function seeTeamAppliesAction($params)
    {

    }

    public function suggestTeamAction($params)
    {

    }

    public function filterTeamsAction($params)
    {

    }

    public function applyAction($params)
    {
        $team = new Team();
        $user_e = new User();
        $id = htmlentities($params[0]);
        $user = $_SESSION['id'];
        $status = 1;
        if($id && $user) {
            $team_check = $team->populate(["id" => $id]);
            $user_check = $user_e->populate(['id' => $user]);
            if($team_check &&$user_check ) {
                $team->customSQL("
                    SELECT 
                        COUNT(".PREFIX."team_user.id) as nb 
                    FROM
                        ".PREFIX."team_user
                    WHERE
                        id_user = :".$user." and 
                        id_team = :".$id."
                ", [
                    ":".$user => $user,
                    ":".$id => $id
                ]);
                $result = $team->executeSQL();

                if(!$result[0]['nb']) {

                    $team->customSQL("
                    SELECT 
                        COUNT(" . PREFIX . "team_user_demand.id) as nb 
                    FROM
                        " . PREFIX . "team_user_demand
                    WHERE
                        id_user = :" . $user . " and 
                        id_team = :" . $id . "
                ", [
                        ":" . $user => $user,
                        ":" . $id => $id
                    ]);
                    $result = $team->executeSQL();

                    if (!$result[0]['nb']) {
                        $team->customSQL("
                    INSERT INTO " . PREFIX . "team_user_demand (
                        id_user,
                        id_team,
                        status
                    ) VALUES (
                        :" . $user . ",
                        :" . $id . ",
                        :" . $status . "
                    )
                ", [
                            ":" . $user => $user,
                            ":" . $id => $id,
                            ":" . $status => $status
                        ]);
                        $team->executeSQL();
                        $_SESSION['listOfSuccess'][] = "Un message a été envoyé au leader de l'équipe!";
                        header("Location: " . ROOT_FOLDER . "team/index/" . $team_check->getName());
                        exit();
                    } else {
                        $_SESSION['listOfErrors'][] = "Tu as déjà fais une demande pour rejoindre cette équipe!";
                        header("Location: ".ROOT_FOLDER."/team/index/" . $team_check->getName());
                        exit();
                    }
                } else {
                    $_SESSION['listOfErrors'][] = "Tu fais déjà partie de cette équipe!!";
                    header("Location: ".ROOT_FOLDER."/team/index/" . $team_check->getName());
                    exit();
                }
            } else {
                $_SESSION['listOfErrors'][] = "Oups il y a eu un problème, tu ne peux pas rejoindre cette équipe";
                header("Location: ".ROOT_FOLDER."/gaming/1");
                exit();
            }
        }
    }

}