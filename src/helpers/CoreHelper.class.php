<?php

class CoreHelper
{

    /**
     * @param $error
     * @param $field
     * @return mixed
     * Dynamic error messages handler
     */
    public static function getErrorMessage($error, $field)
    {
        $msgErrors = [
            "required" => "Le champs :0::val: est obligatoire",
            "username_limits" => "Le champs :0::val: doit avoir au moins :1::val: caractères et ne peut pas en dépasser :2::val:",
            "email_exists" => "Le mail choisi existe déjà",
            "username_exists" => "Le nom d'utilisateur choisi existe déjà",
            "wrong_email" => "L'email saisi n'est pas correct",
            "form_nb" => "Le formulaire contient des données non attendues",
            "different_passwords" => "Le mot de passe et la confirmation ne sont correspondent pas",
            "password_limits" => "Le champs :0::val: doit avoir au moins :1::val: caractères",
            "cgu" => "Vous devez accepter les conditions d'utilisation pour vous inscrire",
            "captcha" => "Le captcha saisi ne correspond pas à l'image",
            "wrong_extension" => "Mauvaise extension pour l'avatar",
            "fail_upload" => "Erreur upload server",
            "image_size" => "L'avatar est trop lourd",
            "upload_dir" => "Dossier d'upload contenant une erreur",
            "no_age" => "Vous devez avoir entre :0::val: et :1::val: ans",
            "date_no_valid" => "La date d'anniversaire n'est pas valide",
            "role_exists" => "fail"

        ];

        $string = $msgErrors[$error];
        for ($i = 0; $i < substr_count($msgErrors[$error], ":val:"); $i++) {
            $string = str_replace(":" . $i . ":", $field[$i], $string);
        }

        return str_replace(":val:", "", $string);
    }

    /**
     * @return PHPMailer
     * Essential mail setup
     */
    public static function mailSetup()
    {
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->CharSet = "UTF-8";
        $mail->SMTPDebug = 0;

        $mail->SMTPAuth = TRUE;
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "ssl";
        //Set TCP port to connect to
        $mail->Port = 465;
        //Set SMTP host name
        $mail->Host = "ssl0.ovh.net";

        $mail->isHTML(TRUE);
        //Provide username and password
        $mail->Username = "contact@weswan.fr";
        $mail->Password = "openweswan17";

        $mail->From = "contact@weswan.fr";
        $mail->FromName = "WeSwan CMS";
        return $mail;
    }

    /**
     * @return string
     * Generates a new token
     */
    public static function generateToken()
    {
        return sha1(uniqid(rand(), TRUE)) . date('YmdHis');
    }

    /**
     * @param $date
     * @param int $mode
     * @return string
     * Formats DateTime to a string date
     */
    public static function getShortdate($date, $mode = 0)
    {
        if ($date) {
            $datetime = new DateTime($date);
            switch ($mode) {
                case 1:
                    // 01/Janvier/70
                    return $datetime->format('d/M/y');
                    break;
                case 2:
                    // 01/Janvier/1970
                    return $datetime->format('d/M/Y');
                    break;
                case 3:
                    // 01/01/1970 00h00m00s
                    return $datetime->format('d/m/Y h:i');
                    break;
                case 4:
                    // 01/01/1970 00h00m00s
                    return $datetime->format('Y-m-d');
                    break;
                default:
                    // 01/01/1970
                    return $datetime->format('d/m/Y');
                    break;
            }
        }
        return '';
    }


    public static function uploadImage($forms, $params)
    {
        $avatarExtensionAuthorized = ["png", "jpg", "jpeg", "gif"];
        $avatarMaxSize = 10000000;

        UserHelper::$listErrors = [];
        $error = FALSE;
        // $form = $user->GetSignForm()
        // Gets into $_POST || $_GET array
        if (!count(UserHelper::$listErrors)) {
            if (!empty($forms['avatar']['name'])) {
                if ($forms["avatar"]["error"] == 0) {
                    $infoAvatar = pathinfo($forms["avatar"]["name"]);

                    if (!in_array(strtolower($infoAvatar["extension"]), $avatarExtensionAuthorized)) {
                        UserHelper::$listErrors[] = CoreHelper::getErrorMessage("wrong_extension", []);
                        $error = TRUE;
                    }
                    if ($forms["avatar"]["size"] > $avatarMaxSize) {
                        UserHelper::$listErrors[] = CoreHelper::getErrorMessage("image_size", []);
                        $error = TRUE;
                    }
                } else {
                    $error = TRUE;
                    switch ($forms["avatar"]["error"]) {
                        case UPLOAD_ERR_INI_SIZE:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        case UPLOAD_ERR_FORM_SIZE:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        case UPLOAD_ERR_PARTIAL:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        case UPLOAD_ERR_NO_TMP_DIR:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        case UPLOAD_ERR_CANT_WRITE:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        case UPLOAD_ERR_EXTENSION:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("fail_upload", []);
                            break;
                        default:
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("image_size", []);
                            break;
                    }

                }
            }
            if (!$error && !empty($forms['avatar']['name'])) {
                $uploadPath = "uploads";
                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath);
                }
                $nameAvatar = uniqid() . "." . strtolower($infoAvatar["extension"]);
                if (!move_uploaded_file($forms["avatar"]["tmp_name"], $uploadPath . DS . $nameAvatar)) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("upload_dir", []);
                    $error = TRUE;
                } else {
                    $_SESSION['data_array_image'] = $uploadPath . "/" . $nameAvatar;
                }
            } else {
                $_SESSION['data_array_image'] = "NULL";
            }
        }
        // If errors, invalid form
        if (count(UserHelper::$listErrors)) {
            $_SESSION['listOfErrors'] = UserHelper::$listErrors;
            return FALSE;
        }
        if (isset($_SESSION['hasError'])) {
            unset($_SESSION['hasError']);
        }
        if (isset($_SESSION['oldData'])) {
            unset($_SESSION['oldData']);
        }
        if (isset($_SESSION['listOfErrors'])) {
            unset($_SESSION['listOfErrors']);
        }
        return TRUE;
    }


}