<?php

class UserHelper
{
    public static $listErrors = [];

    /**
     * @param $form
     * @param $params
     * @return bool
     * Checks if the signUp Form is valid
     */
    public static function validSignForm($form, $params)
    {

        UserHelper::$listErrors = [];
        // $form = $user->GetSignForm()
        // Gets into $_POST || $_GET array

        if (!count(UserHelper::$listErrors)) {
            foreach ($params as $field => $value) {
                $params[$field] = trim($params[$field]);

                if (isset($form['data'][$field]['required']) && empty($value) || $field == "captcha" && empty($value)) {
                    if ($field == "captcha") {
                        $fld = "Captcha";
                    } else {
                        $fld = $form['data'][$field]['label'];
                    }
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("required", [0 => $fld]);
                }


                if ($field == "username" && !empty($value) && (strlen($value) < 5 || strlen($value) > 15)) {
                    echo $field;
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("username_limits", [0 => $form['data'][$field]['label'], 1 => 5, 2 => 15]);
                }


                if ($field == "email" && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("wrong_email", []);
                }

                if ($field == "password") {
                    if (isset($params['password_confirmation']) && $value != $params['password_confirmation'] && $field == "password_confirmation"){
                        UserHelper::$listErrors[] = CoreHelper::getErrorMessage("different_passwords", [0 => $form['data'][$field]['label']]);
                    } else {
                        if ($form['error']['control'] && strlen($value) < 6 || strlen($value) > 30) {
                            UserHelper::$listErrors[] = CoreHelper::getErrorMessage("password_limits", [0 => $form['data'][$field]['label'], 1 => 6]);
                        }
                    }
                }


                if ($field == "cgu" && empty($value)) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("cgu", []);
                }

                if ($field == "captcha" && ($value != $_SESSION['sign_captcha'])) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("captcha", []);
                }


                $checkUser = new User();

                if ($form['error']['control'] && $field == "email" && ($checkUser->populate(['email' => $value]))) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("email_exists", []);
                }

                if ($form['error']['control'] && $field == "username" && ($checkUser->populate(['username' => $value]))) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("username_exists", []);
                }


                $checkRole = new Role();
                if ($form['error']['control'] && $field == "role" && (!$checkRole->populate(['id' => $value]))) {
                    UserHelper::$listErrors[] = CoreHelper::getErrorMessage("role_exists", []);
                }
            }
        }

        // If errors, invalid form
        if (count(UserHelper::$listErrors)) {
            foreach ($params as $name => $value) {
                if ($name != "captcha" && $name != "password" && $name != "password_confirmation" && $name != "cgu") {
                    $_SESSION['oldData'][$name] = $value;
                }
            }
            $_SESSION['listOfErrors'] = UserHelper::$listErrors;
            return FALSE;
        }
        if (isset($_SESSION['hasError'])) {
            unset($_SESSION['hasError']);
        }
        if (isset($_SESSION['oldData'])) {
            unset($_SESSION['oldData']);
        }
        if (isset($_SESSION['listOfErrors'])) {
            unset($_SESSION['listOfErrors']);
        }
        return TRUE;
    }


}