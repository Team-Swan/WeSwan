<?php

class Routing{

	private $uriExploded;

	private $controller;
	private $controllerName;
	private $action;
	private $actionName;

	private $params;
    private $getParams;

	public function __construct(){
		//    /projet%20php/user/add
		$uri = $_SERVER["REQUEST_URI"];
		//    user/add
		$uri = preg_replace("#".BASE_PATH_PATTERN."#i", "", $uri, 1);
		//Array ( [0] => user [1] => add )
		$this->uriExploded = explode("/",  trim(explode("?",  $uri   )[0], "/"));

		$params = explode("?",  trim($uri, "/")   );
		if(isset($params[1]) && $params[1] != '') {
            $this->getParams = $params[1];
        }
		$this->setController();
		$this->setAction();
		$this->setParams();
		$this->runRoute();

	}

	public function setController(){
		$this->controller = (empty($this->uriExploded[0]))?"Index":ucfirst($this->uriExploded[0]);
		$this->controllerName = $this->controller."Controller";
		unset($this->uriExploded[0]);
	}

	public function setAction(){
        if(empty($this->uriExploded[1])) {
            $this->action = "index";
        } elseif( !method_exists($this->controllerName, $this->uriExploded[1] ) && is_numeric($this->uriExploded[1]) && $this->uriExploded[1] > 0) {

            $this->action = "index";
            $_POST['page'] = intval($this->uriExploded[1]);
        } else {
            $this->action = $this->uriExploded[1];
        }
        $this->actionName = $this->action."Action";
		unset($this->uriExploded[1]);
	}

	public function setParams(){
		$this->params = array_merge(array_values($this->uriExploded), $_POST);
		if($this->getParams) {
            $this->params[] = $this->getParams;
		}
    }


	public function checkRoute(){
		//Est ce qu'il existe un fichier du nom de xxxxContoller $this->controllerName
		$pathController = "src/controllers/".$this->controllerName.".class.php";
		if( !file_exists($pathController) ){

			return false;
		}
		include $pathController;

		if ( !class_exists($this->controllerName)  ){

			return false;
		}
		if(  !method_exists($this->controllerName, $this->actionName) ){

			return false;
		}
		return true;
	}


	public function runRoute(){
		if($this->checkRoute()){
		    if($this->controllerName == "AdminController" && isset($_SESSION['id_role']) != 4) {
                $this->page404();
                exit();
            }
            //$this->controllerName = IndexController
                $controller = new $this->controllerName();
                //$this->actionName = indexAction


                $controller->{$this->actionName}($this->params);
		}else{
			$this->page404();
		}
	}

	public function page404(){
        $view = new View("page404", "singlepage");
	}


}






