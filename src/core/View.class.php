<?php

class View {
    protected $view;
    protected $template;
    protected $data = [];

    public function __construct($view = "index", $template = "frontend") {
        $this->setView($view);
        $this->setTemplate($template, []);
    }

    public function setView($view) {
        if(file_exists("src/views/".$view.".view.php")){ // Toujours dans l'index à ce niveau là
            $this->view = $view.".view.php";
        } else {
            die("La vue n'existe pas");
        }
    }

    public function setTemplate($template, $config) {

        if(file_exists("src/views/".$template.".temp.php")){
            $this->template = $template.".temp.php";
        } else {
            die("Le template n'existe pas");
        }
    }

    public function assign($key, $value) {
        $this->data[$key] = $value;
    }

    public function includeModal($modal, $config) {
        if(file_exists("src/views/modals/".$modal.".mod.php")){
            include "src/views/modals/".$modal.".mod.php";
        } else {
            die("Le modal n'existe pas ");
        }
    }

    public function __destruct() {
        extract($this->data);
        include "src/views/".$this->template;
    }
}