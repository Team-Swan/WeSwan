<?php

class BaseSql
{
    private $db;
    private $table;
    private $columns = [];

    public function __construct()
    {
        try {
            $this->db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";port=" . DB_PORT, DB_USER, DB_PASSWORD);
            $this->db->exec("SET NAMES 'UTF8'");
        } catch (Exception $e) {
            die ('Erreur SQL : ' . $e->getMessage());
        }
        $this->table = strtolower(get_called_class());
        $objectsVar = get_class_vars($this->table);
        $sqlVars = get_class_vars(get_class());
        // Colums are only keys that are not on both objects (BaseSql & User for example)
        $this->columns = array_diff_key($objectsVar, $sqlVars);
    }

    public function save()
    {
        $data = [];
        if ($this->id == -1) {
            unset($this->columns['id']);
            unset($this->columns['created_at']);
            unset($this->columns['date_created']);
            unset($this->columns['loged_at']);
            $sqlCol = NULL;
            $sqlKey = NULL;
            foreach ($this->columns as $column => $value) {
                $data[$column] = $this->$column;
                $sqlCol .= "," . $column;
                $sqlKey .= ", :" . $column;
            }
            $sqlCol = ltrim($sqlCol, ",");
            $sqlKey = ltrim($sqlKey, ",");
            $query = $this->db->prepare("INSERT INTO " . PREFIX . $this->table . " 
     (" . $sqlCol . ") 
    VALUES 
     (" . $sqlKey . ")");

            $query->execute($data);

        } else {
            // Dynamic update
            $sqlSet = NULL;

            foreach ($this->columns as $column => $value) {
                $data[$column] = $this->$column;
                $sqlSet[] = $column . "= :" . $column;
            }
            $query = $this->db->prepare("UPDATE " . PREFIX . $this->table . " 
    SET
     date_updated = sysdate(),
     " . implode(",", $sqlSet) . " 
    WHERE 
     id=:id;");
            $query->execute($data);
        }


    }

    /**
     * @param $search
     * @return mixed
     * Return an instance of an object after a SQL query with WHERE params
     */
    public function populate($search)
    {
        // Gets the row that we need
        $query = $this->getOneBy($search, TRUE);

        // Sets the fetch mode to make an instance of $this->table called
        $query->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->table);
        $object = $query->fetch();
        return $object;
    }

    /**
     * @param array $search
     * @param bool $returnQuery
     * @return mixed|PDOStatement
     * If returnQuery is true, this function returns the query from where it's called to set a fetch mode.
     * Otherwise, it returns an array with the query result
     */
    public function getOneBy($search = [], $returnQuery = FALSE)
    {
        foreach ($search as $key => $value) {
            $where[] = $key . '=:' . $key;
        }

        $query = $this->db->prepare("SELECT * FROM " . PREFIX . $this->table . " WHERE " . implode(" AND ", $where));
        $query->execute($search);
        if ($returnQuery) {
            return $query;
        }

        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param null $order
     * @param null $where
     * @param null $value
     * @param null $limit
     * @param null $offset
     * @return array|null
     */
    public function getElements($order = NULL, $where = NULL, $value = NULL, $limit = NULL, $offset = NULL)
    {
        $query = "SELECT * FROM " . PREFIX . $this->table;

        if ($where !== NULL) {
            $query .= " WHERE " . $where . " = " . $value;
        }
        if ($order !== NULL) {
            $query .= " ORDER BY " . $order;
        }

        if ($limit !== NULL) {
            $and = "";
            if ($offset !== NULL) {
                $and = ', ' . $offset;
            }
            $query .= " LIMIT " . $limit . $and;
        }

        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $object = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (empty($object)) {
                return NULL;
            }
            return $object;
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

    }

    public function selectSQL($attr)
    {
        $this->query = "SELECT " . $attr;
    }

    public function fromSQL($table = NULL)
    {
        $this->query .= " FROM " . PREFIX . $this->table;

        if ($table != NULL) {
            $this->query .= ", " . PREFIX . $table;
        }
    }

    public function joinSQL($attr2, $table, $attr1)
    {
        $this->query .= " WHERE " . PREFIX . $table . "." . $attr1 . " = " . PREFIX . $this->table . "." . $attr2;
    }

    public function andJoinSQL($attr2, $table, $attr1)
    {
        $this->query .= " AND " . PREFIX . $table . "." . $attr1 . " = " . PREFIX . $this->table . "." . $attr2;
    }

    public function whereSQL($condKey, $condValue)
    {
        $this->query .= " WHERE " . $condKey . " = :" . $condValue . "";
        $this->data[":" . $condValue] = $condValue;
    }

    public function whereNotSQL($condKey, $condValue)
    {
        $this->query .= " WHERE " . $condKey . " != :" . $condValue . "";
        $this->data[":" . $condValue] = $condValue;
    }

    public function andWhereSQL($condKey, $condValue)
    {
        $this->query .= " AND " . $condKey . " = :" . $condValue;
        $this->data[":" . $condValue] = $condValue;
    }

    public function groupbySQL($attr)
    {
        $this->query .= " GROUP BY " . $attr;
    }

    public function orderSQL($attr)
    {
        $this->query .= " ORDER BY " . $attr;
    }

    public function limitSQL($limit = NULL, $offset = NULL)
    {
        $this->query .= " LIMIT " . $limit;
        if ($offset !== NULL) {
            $this->query .= ", " . $offset;
        }
    }

    public function customSQL($query = NULL, $vals = [])
    {
        $this->query = $query;
        if (is_array($vals)) {
            foreach ($vals as $id => $val) {
                $this->data[$id] = $val;
            }
        }
    }

    public function executeSQL()
    {

        try {
            $stmt = $this->db->prepare($this->query);
            if (isset($this->data) && count($this->data)) {
                foreach ($this->data as $field => &$val) {
                    $stmt->bindParam($field, $val);
                }
            }


            $stmt->execute();
            $object = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (empty($object)) {
                return NULL;
            }
            $this->data = [];
            return $object;
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
    }
}