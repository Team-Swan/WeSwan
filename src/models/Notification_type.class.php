<?php

namespace models\Notification_type;
use core\BaseSql\BaseSql;


class Notification_type extends BaseSql {

    protected $id;
    protected $name;
    protected $status;
     protected $id_user;

    /**
     * Notification_type constructor.
     * @param $id
     * @param $status
     * @param $name
     * @param $id_user
     */
    public function __construct($id, $status, $name, $id_user)
    {
        $this->id = $id;
        $this->status = $status;
        $this->name = $name;
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }



}