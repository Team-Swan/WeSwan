<?php

namespace models\Notifications;
use core\BaseSql\BaseSql;

class Notifications extends BaseSql {


    protected $id;
    protected $created_at;
    protected $is_view;
    protected $id_notification_type;
    protected $id_user;
    protected $wording;

    /**
     * Notifications constructor.
     * @param $id
     * @param $created_at
     * @param $is_view
     * @param $id_notification_type
     * @param $id_user
     * @param $wording
     */
    public function __construct($id, $created_at, $is_view, $id_notification_type, $id_user, $wording)
    {
        $this->id = $id;
        $this->created_at = $created_at;
        $this->is_view = $is_view;
        $this->id_notification_type = $id_notification_type;
        $this->id_user = $id_user;
        $this->wording = $wording;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getIsView()
    {
        return $this->is_view;
    }

    /**
     * @param mixed $is_view
     */
    public function setIsView($is_view)
    {
        $this->is_view = $is_view;
    }

    /**
     * @return mixed
     */
    public function getIdNotificationType()
    {
        return $this->id_notification_type;
    }

    /**
     * @param mixed $id_notification_type
     */
    public function setIdNotificationType($id_notification_type)
    {
        $this->id_notification_type = $id_notification_type;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * @param mixed $wording
     */
    public function setWording($wording)
    {
        $this->wording = $wording;
    }




}