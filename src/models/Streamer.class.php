<?php

class Streamer extends BaseSql
{
    protected $id;
    protected $username;

    public function __construct($id = -1, $username = NULL) {
        parent::__construct();
        $this->id = $id;
        $this->username = $username;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $this;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsernam($username) {
        $this->username = $username;
    }
}

