<?php

class User extends BaseSql {
    protected $id;
    protected $id_role;
    protected $username;
    protected $firstname;
    protected $lastname;
    protected $birthday;
    protected $email;
    protected $password;
    protected $avatar;
    protected $is_deleted;
    protected $is_banned;
    protected $certify;
    protected $token;
    protected $created_at;
    protected $last_connexion_at;

    /**
     * User constructor.
     * @param int $id
     * @param int $id_role
     * @param string $username
     * @param string $firstname
     * @param string $lastname
     * @param string $birthday
     * @param string $email
     * @param string $password
     * @param string $avatar
     * @param null $is_deleted
     * @param null $is_banned
     * @param null $certify
     * @param null $token
     * @param null $created_at
     * @param null $last_connexion_at
     */
    public function __construct($id = -1, $id_role = 0, $username = "", $firstname = NULL, $lastname = NULL, $birthday = NULL, $email = "", $password = "", $avatar = NULL, $is_deleted = NULL, $is_banned = NULL, $certify = NULL, $token = NULL, $created_at = NULL, $last_connexion_at = NULL)
    {
        parent::__construct();
        $this->id = $id;
        $this->id_role = $id_role;
        $this->username = $username;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->password = $password;
        $this->avatar = $avatar;
        $this->is_deleted = $is_deleted;
        $this->is_banned = $is_banned;
        $this->certify = $certify;
        $this->token = CoreHelper::generateToken();
        $this->created_at = $created_at;
        $this->last_connexion_at = $last_connexion_at;
    }

    /**
     * Returns the User Sign Up form
     * @return array
     */
    public function getSignForm()
    {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/user/signUp",
                "parent" => "user",
                "class" => "form",
                "submit" => "S'inscrire",
                "name" => "sign"
            ],
            "data" => [
                "username" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Pseudo",
                    "class" => "form_input",
                    "id" => "username_sign",
                    "required" => 1
                ],
                "email" => [
                    "element" => "input",
                    "type" => "email",
                    "label" => "E-Mail",
                    "class" => "form_input",
                    "id" => "email_sign",
                    "required" => 1
                ],
                "password" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Mot de Passe",
                    "class" => "form_input",
                    "id" => "password_sign",
                    "required" => 1
                ],
                "password_confirmation" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Confirmation Mot de Passe",
                    "class" => "form_input",
                    "id" => "password_confirmation_sign",
                    "required" => 1
                ],
                "cgu" => [
                    "element" => "input",
                    "type" => "checkbox",
                    "label" => "Vous acceptez les conditions d'utilisation du site",
                    "class" => "option-input checkbox",
                    "id" => "cgu_sign",
                    "required" => 1,
                    "checked" => 1
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
                "captcha" => 1
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }

    /**
     * Returns the User Login form
     * @return array
     */
    public function getLoginForm()
    {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/user/login",
                "parent" => "user",
                "class" => "form",
                "id" => "login_form",
                "submit" => "Se connecter",
                "hasReset" => 1,
                "name" => "login"
            ],
            "data" => [
                "username" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Pseudo",
                    "class" => "form_input",
                    "id" => "username_sign",
                    "required" => 1
                ],
                "password" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Mot de Passe",
                    "class" => "form_input",
                    "id" => "password_sign",
                    "required" => 1
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "id" => "token_sign",
                    "hidden" => 1,
                    "required" => 1
                ]
            ],
            "links" => [
                0 => [
                    "href" => "#",
                    "content" => "Reinitialiser mon mdp",
                    "class" => "reset_password",
                    "form_name" => "reset_form"
                ]
            ],
            "error" => [
                "id" => "error_login_div"
            ]
        ];
    }

    /**
     * Returns the User Reset Password form
     * @return array
     */
    public function getResetForm()
    {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/user/resetedPassword",
                "parent" => "user",
                "class" => "form",
                "id" => "reset_form",
                "submit" => "Envoyer",
                "name" => "reset"
            ],
            "data" => [
                "password" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Mot de Passe",
                    "class" => "form_input",
                    "id" => "password_reset",
                    "required" => 1
                ],
                "password_confirmation" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Confirmation Mot de Passe",
                    "class" => "form_input",
                    "id" => "password_confirmation_reset",
                    "required" => 1
                ],
                "email" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => $_GET['email'],
                    "hidden" => 1,
                    "required" => 1
                ],
                "token" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => $_GET['token'],
                    "hidden" => 1,
                    "required" => 1
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ]
            ],
            "error" => [
                "id" => "error_reset_div"
            ]
        ];

    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdRole()
    {
        return $this->id_role;
    }

    /**
     * @param mixed $id_role
     */
    public function setIdRole($id_role)
    {
        $this->id_role = $id_role;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $username = str_replace(" ", "", $username);
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $firstname = str_replace(" ", "", $firstname);
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $lastname = str_replace(" ", "", $lastname);
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $pattern = (strpos($birthday, "-"))?"Y-m-d":"d/m/Y";
        $dateBirthday = DateTime::createFromFormat($pattern, $birthday);
        $dateErrors = DateTime::getLastErrors();
        if($dateErrors["warning_count"]+$dateErrors["error_count"]==0){
            $this->birthday = $dateBirthday->format("Y-m-d");
        } else {
            $this->birthday = "1970-01-01";
        }
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param mixed $is_deleted
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
    }

    /**
     * @return mixed
     */
    public function getIsBanned()
    {
        return $this->is_banned;
    }

    /**
     * @param mixed $is_banned
     */
    public function setIsBanned($is_banned)
    {
        $this->is_banned = $is_banned;
    }

    /**
     * @return mixed
     */
    public function getCertify()
    {
        return $this->certify;
    }

    /**
     * @param mixed $certify
     */
    public function setCertify($certify)
    {
        $this->certify = $certify;
    }

    /**
     * @return mixed
     */

    public function getToken() {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token) {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getLastConnexionAt()
    {
        return $this->last_connexion_at;
    }

    /**
     * @param mixed $last_connexion_at
     */
    public function setLastConnexionAt($last_connexion_at)
    {
        $this->last_connexion_at = $last_connexion_at;
    }
}