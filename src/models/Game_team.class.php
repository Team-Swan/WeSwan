<?php

namespace models\Game_team;
use core\BaseSql\BaseSql;

class Game_team extends BaseSql
{
    protected $id;
    protected $id_game;
    protected $id_team;
    protected $score;
    protected $has_played;

    /**
     * Game_team constructor.
     * @param $id
     * @param $id_game
     * @param $id_team
     * @param $score
     * @param $has_played
     */
    public function __construct($id, $id_game, $id_team, $score, $has_played)
    {
        $this->id = $id;
        $this->id_game = $id_game;
        $this->id_team = $id_team;
        $this->score = $score;
        $this->has_played = $has_played;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdGame()
    {
        return $this->id_game;
    }

    /**
     * @param mixed $id_game
     */
    public function setIdGame($id_game)
    {
        $this->id_game = $id_game;
    }

    /**
     * @return mixed
     */
    public function getIdTeam()
    {
        return $this->id_team;
    }

    /**
     * @param mixed $id_team
     */
    public function setIdTeam($id_team)
    {
        $this->id_team = $id_team;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return mixed
     */
    public function getHasPlayed()
    {
        return $this->has_played;
    }

    /**
     * @param mixed $has_played
     */
    public function setHasPlayed($has_played)
    {
        $this->has_played = $has_played;
    }


}