<?php


class Game_list extends BaseSql {

    protected $id;
    protected $id_user;
    protected $name;
    protected $image;
    protected $status;

    /**
     * Game_list constructor.
     * @param $id
     * @param $id_user
     * @param $name
     * @param $image
     * @param $status
     */
    public function __construct($id = -1, $id_user = 0, $name = null, $image = null, $status = null)
    {
        parent::__construct();
        $this->id = $id;
        $this->id_user = $id_user;
        $this->name = $name;
        $this->image = $image;
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}