<?php

namespace models\Rights;
use core\BaseSql\BaseSql;

class Rights extends BaseSql
{

    protected $id;
    protected $right_name;
    protected $right_code;
    protected $created_at;

    /**
     * Rights constructor.
     * @param $id
     * @param $right_name
     * @param $right_code
     * @param $created_at
     */
    public function __construct($id, $right_name, $right_code, $created_at)
    {
        $this->id = $id;
        $this->right_name = $right_name;
        $this->right_code = $right_code;
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRightName()
    {
        return $this->right_name;
    }

    /**
     * @param mixed $right_name
     */
    public function setRightName($right_name)
    {
        $this->right_name = $right_name;
    }

    /**
     * @return mixed
     */
    public function getRightCode()
    {
        return $this->right_code;
    }

    /**
     * @param mixed $right_code
     */
    public function setRightCode($right_code)
    {
        $this->right_code = $right_code;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
}
