<?php

class Team extends BaseSql {

   protected $id;
   protected $id_game_list;
   protected $leader;
   protected $image;
   protected $content;
   protected $status;
   protected $on_demand;
   protected $score;
   protected $has_played;
   protected $name;
   protected $id_user;
   protected $created_at;

    /**
     * Team constructor.
     * @param $id
     * @param $id_game_list
     * @param $leader
     * @param $image
     * @param $content
     * @param $status
     * @param $on_demand
     * @param $score
     * @param $has_played
     * @param $name
     * @param $id_user
     * @param $created_at
     */
    public function __construct($id = -1, $id_game_list = NULL, $leader = NULL, $image = NULL, $content = NULL, $status = 0, $on_demand = 0, $score = NULL, $has_played = 0, $name  = NULL, $id_user = NULL, $created_at = NULL)
    {
        parent::__construct();
        $this->id = $id;
        $this->id_game_list = $id_game_list;
        $this->leader = $leader;
        $this->image = $image;
        $this->content = $content;
        $this->status = $status;
        $this->on_demand = $on_demand;
        $this->score = $score;
        $this->has_played = $has_played;
        $this->name = $name;
        $this->id_user = $id_user;
        $this->created_at = $created_at;
    }

    public function getApllyForm()
    {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/team/apply",
                "parent" => "comment",
                "class" => "team",
                "id" => "team_form",
                "submit" => "Demander à Rejoindre",
                "name" => "team"
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdGameList()
    {
        return $this->id_game_list;
    }

    /**
     * @param mixed $id_game_list
     */
    public function setIdGameList($id_game_list)
    {
        $this->id_game_list = $id_game_list;
    }

    /**
     * @return mixed
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * @param mixed $leader
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getOnDemand()
    {
        return $this->on_demand;
    }

    /**
     * @param mixed $on_demand
     */
    public function setOnDemand($on_demand)
    {
        $this->on_demand = $on_demand;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return mixed
     */
    public function getHasPlayed()
    {
        return $this->has_played;
    }

    /**
     * @param mixed $has_played
     */
    public function setHasPlayed($has_played)
    {
        $this->has_played = $has_played;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }





}