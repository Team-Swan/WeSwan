<?php

namespace models\Game;
use core\BaseSql\BaseSql;

class Game extends BaseSql {

    protected $id;
    protected $id_user;
    protected $id_game_list;
    protected $player_max_per_team;
    protected $title;
    protected $nb_team;
    protected $stream_link;
    protected $date_start;
    protected $date_end;
    protected $status;
    protected $type;
    protected $plateform;

    /**
     * Game constructor.
     * @param $id
     * @param $id_user
     * @param $id_game_list
     * @param $player_max_per_team
     * @param $title
     * @param $nb_team
     * @param $stream_link
     * @param $date_start
     * @param $date_end
     * @param $status
     * @param $type
     * @param $plateform
     */
    public function __construct($id, $id_user, $id_game_list, $player_max_per_team, $title, $nb_team, $stream_link, $date_start, $date_end, $status, $type, $plateform)
    {
        $this->id = $id;
        $this->id_user = $id_user;
        $this->id_game_list = $id_game_list;
        $this->player_max_per_team = $player_max_per_team;
        $this->title = $title;
        $this->nb_team = $nb_team;
        $this->stream_link = $stream_link;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->status = $status;
        $this->type = $type;
        $this->plateform = $plateform;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getIdGameList()
    {
        return $this->id_game_list;
    }

    /**
     * @param mixed $id_game_list
     */
    public function setIdGameList($id_game_list)
    {
        $this->id_game_list = $id_game_list;
    }

    /**
     * @return mixed
     */
    public function getPlayerMaxPerTeam()
    {
        return $this->player_max_per_team;
    }

    /**
     * @param mixed $player_max_per_team
     */
    public function setPlayerMaxPerTeam($player_max_per_team)
    {
        $this->player_max_per_team = $player_max_per_team;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getNbTeam()
    {
        return $this->nb_team;
    }

    /**
     * @param mixed $nb_team
     */
    public function setNbTeam($nb_team)
    {
        $this->nb_team = $nb_team;
    }

    /**
     * @return mixed
     */
    public function getStreamLink()
    {
        return $this->stream_link;
    }

    /**
     * @param mixed $stream_link
     */
    public function setStreamLink($stream_link)
    {
        $this->stream_link = $stream_link;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @param mixed $date_start
     */
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @param mixed $date_end
     */
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPlateform()
    {
        return $this->plateform;
    }

    /**
     * @param mixed $plateform
     */
    public function setPlateform($plateform)
    {
        $this->plateform = $plateform;
    }


}