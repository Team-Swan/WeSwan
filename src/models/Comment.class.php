<?php


class Comment extends BaseSql {

    protected $id;
    protected $id_user;
    protected $id_content;
    protected $comment;
    protected $created_at;


    /**
     * Comment constructor.
     * @param int $id
     * @param null $id_user
     * @param null $id_content
     * @param null $comment
     * @param null $created_at
     */
    public function __construct($id = -1, $id_user = NULL, $id_content = NULL, $comment = NULL, $created_at = NULL)
    {
        parent::__construct();
        $this->id = $id;
        $this->id_user = $id_user;
        $this->id_content = $id_content;
        $this->comment = $comment;
        $this->created_at = $created_at;
    }

    /**
     * @return array
     */
    public function getCommentForm()
    {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/comment/add",
                "parent" => "comment",
                "class" => "form",
                "id" => "comment_form",
                "submit" => "Envoyer",
                "name" => "comment"
            ],
            "data" => [
                "comment" => [
                    "element" => "textarea",
                    "class" => "form_input",
                    "id" => "comment_input",
                    "rows" => 5,
                    "cols" => 70,
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "id" => "token_sign",
                    "hidden" => 1,
                    "required" => 1
                ]
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getIdContent()
    {
        return $this->id_content;
    }

    /**
     * @param mixed $id_content
     */
    public function setIdContent($id_content)
    {
        $this->id_content = $id_content;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $date_created
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

}