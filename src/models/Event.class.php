<?php

namespace models\Event;
use core\BaseSql\BaseSql;

class Event extends BaseSql {
    protected $id;
    protected $name;
    protected $type;
    protected $status;
    protected $description;
    protected $date_start;
    protected $date_end;
    protected $created_at;
    protected $date_event;
    protected $id_user;
    protected $is_deleted;

    /**
     * Event constructor.
     * @param $id
     * @param $name
     * @param $type
     * @param $status
     * @param $description
     * @param $date_start
     * @param $date_end
     * @param $created_at
     * @param $date_event
     * @param $id_user
     * @param $is_deleted
     */
    public
    function __construct($id, $name, $type, $status, $description, $date_start, $date_end, $created_at, $date_event, $id_user, $is_deleted)
    {
        parent::__construct();
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->status = $status;
        $this->description = $description;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->created_at = $created_at;
        $this->date_event = $date_event;
        $this->id_user = $id_user;
        $this->is_deleted = $is_deleted;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @param mixed $date_start
     */
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @param mixed $date_end
     */
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getDateEvent()
    {
        return $this->date_event;
    }

    /**
     * @param mixed $date_event
     */
    public function setDateEvent($date_event)
    {
        $this->date_event = $date_event;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param mixed $is_deleted
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
    }


}