<?php

class Private_message extends BaseSql {

    protected $id;
    protected $created_at;
    protected $id_sender;
    protected $id_receiver;
    protected $title;
    protected $content;
    protected $is_deleted;

    /**
     * Message constructor.
     * @param $id
     * @param $created_at
     * @param $id_sender
     * @param $id_receiver
     * @param $title
     * @param $content
     * @param $is_deleted
     */
    public function __construct($id = -1, $created_at = NULL, $id_sender = NULL, $id_receiver = NULL, $title = NULL, $content = NULL, $is_deleted = 0)
    {
        parent::__construct();
        $this->id = $id;
        $this->created_at = $created_at;
        $this->id_sender = $id_sender;
        $this->id_receiver = $id_receiver;
        $this->title = $title;
        $this->content = $content;
        $this->is_deleted = $is_deleted;
    }

    public function getMessageForm() {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/messagerie/add",
                "parent" => "messagerie",
                "class" => "form",
                "id" => "messagerie_form",
                "submit" => "Envoyer",
                "name" => "messagerie"
            ],
            "data" => [
                "destinataire" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "destinataire",
                    "class" => "form_input",
                    "id" => "destinataire"
                ],
                "objet" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "objet",
                    "class" => "form_input",
                    "id" => "objet"
                ],
                "content" => [
                    "element" => "textarea",
                    "class" => "form_input",
                    "id" => "comment_input",
                    "rows" => 5,
                    "cols" => 70,
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "id" => "token_sign",
                    "hidden" => 1,
                    "required" => 1
                ]
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getIdSender()
    {
        return $this->id_sender;
    }

    /**
     * @param mixed $id_sender
     */
    public function setIdSender($id_sender)
    {
        $this->id_sender = $id_sender;
    }

    /**
     * @return mixed
     */
    public function getIdReceiver()
    {
        return $this->id_receiver;
    }

    /**
     * @param mixed $id_receiver
     */
    public function setIdReceiver($id_receiver)
    {
        $this->id_receiver = $id_receiver;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param mixed $is_delete
     */
    public function setIsDeleted($is_delete)
    {
        $this->is_delete = $is_delete;
    }



}