<?php

namespace models\Forum;
use core\BaseSql\BaseSql;

class Forum extends BaseSql {
    protected $id;
    protected $title;
    protected $active;
    protected $created_at;
    protected $date_update;
    protected $id_category;

    /**
     * Forum constructor.
     * @param $id
     * @param $title
     * @param $active
     * @param $created_at
     * @param $date_update
     * @param $id_category
     */
    public function __construct($id, $title, $active, $created_at, $date_update, $id_category)
    {
        parent::__construct();
        $this->id = $id;
        $this->title = $title;
        $this->active = $active;
        $this->created_at = $created_at;
        $this->date_update = $date_update;
        $this->id_category = $id_category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param mixed $date_update
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;
    }

    /**
     * @return mixed
     */
    public function getIdCategory()
    {
        return $this->id_category;
    }

    /**
     * @param mixed $id_category
     */
    public function setIdCategory($id_category)
    {
        $this->id_category = $id_category;
    }

}