<?php

namespace models\Game_fixture;
use core\BaseSql\BaseSql;

class Game_fixture extends BaseSql {

    protected $id;
    protected $id_game;
    protected $id_team1;
    protected $id_team2;
    protected $created_at;

    /**
     * Game_fixture constructor.
     * @param $id
     * @param $id_game
     * @param $id_team1
     * @param $id_team2
     * @param $created_at
     */
    public function __construct($id, $id_game, $id_team1, $id_team2, $created_at)
    {
        $this->id = $id;
        $this->id_game = $id_game;
        $this->id_team1 = $id_team1;
        $this->id_team2 = $id_team2;
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdGame()
    {
        return $this->id_game;
    }

    /**
     * @param mixed $id_game
     */
    public function setIdGame($id_game)
    {
        $this->id_game = $id_game;
    }

    /**
     * @return mixed
     */
    public function getIdTeam1()
    {
        return $this->id_team1;
    }

    /**
     * @param mixed $id_team1
     */
    public function setIdTeam1($id_team1)
    {
        $this->id_team1 = $id_team1;
    }

    /**
     * @return mixed
     */
    public function getIdTeam2()
    {
        return $this->id_team2;
    }

    /**
     * @param mixed $id_team2
     */
    public function setIdTeam2($id_team2)
    {
        $this->id_team2 = $id_team2;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }



}