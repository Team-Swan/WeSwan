<?php


class Config extends BaseSql
{

    protected $id;
    protected $wording;
    protected $value;

    /**
     * Conf constructor.
     * @param $id
     * @param $wording
     * @param $value
     */
    public function __construct($id = -1, $wording = NULL, $value = NULL)
    {
        parent::__construct();
        $this->id = $id;
        $this->wording = $wording;
        $this->value = $value;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * @param mixed $wording
     */
    public function setWording($wording)
    {
        $this->wording = $wording;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


}