<?php

class Role extends BaseSql
{

    protected $id;
    protected $name;
    protected $status;
    protected $created_at;

    /**
     * Role constructor.
     * @param $id
     * @param $name
     * @param $status
     * @param $created_at
     */
    public function __construct($id = -1, $name = NULL, $status = NULL, $created_at = NULL)
    {
        parent::__construct();
        $this->id = $id;
        $this->name = $name;
        $this->status = $status;
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }


}