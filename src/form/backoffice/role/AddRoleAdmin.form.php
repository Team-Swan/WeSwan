<?php


class AddRoleAdmin {

    function __construct()
    {


    }

    public function addRole()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/addroles",
                "enctype" => "multipart/form-data",
                "parent" => "role",
                "class" => "form",
                "submit" => "Ajouter",
                "name" => "addrole"
            ],
            "data" => [
                "role" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Role",
                    "id" => "role"
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}