<?php


class EditRoleAdmin {

    function __construct()
    {


    }

    public function editRole($data)
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/editroles/" . $data['id'],
                "enctype" => "multipart/form-data",
                "parent" => "role",
                "class" => "form",
                "submit" => "Editer",
                "name" => "editRole"
            ],
            "data" => [
                "role" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Role",
                    "id" => "role",
                    "value" => $data['name']
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}