<?php


class EditSettingsAdmin {

    function __construct()
    {


    }

    public function editAdminSettings($get_settings)

    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/settings",
                "parent" => "conf",
                "class" => "form",
                "submit" => "Modifier",
                "name" => "settings"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Title",
                    "id" => "title",
                    "value" => $get_settings["title"]
                ],
                "description" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Description",
                    "id" => "description",
                    "value" => $get_settings["description"]
                ],
                "email" => [
                    "element" => "input",
                    "type" => "email",
                    "label" => "E-Mail",
                    "id" => "email",

                    //"value" => $get_settings["username"]

                ],
                "facebook" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Facebook",
                    "id" => "facebook",
                    //"value" => $get_settings["username"]
                ],
                "twitter" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Facebook",
                    "id" => "facebook",
                    //"value" => $get_settings["username"]
                ],
                "instagram" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Facebook",
                    "id" => "facebook",
                    //"value" => $get_settings["username"]
                ],
                "googleplus" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Facebook",
                    "id" => "facebook",
                    //"value" => $get_settings["username"]
                ],
                "linkedin" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Linkedin",
                    "id" => "linkedin",
                    //"value" => $get_settings["username"]
                ],
                "steam" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Steam",
                    "id" => "steam",
                    //"value" => $get_settings["username"]
                ],
                "twitch" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Twitch",
                    "id" => "twitch",

                    //"value" => $get_settings["username"]
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}

