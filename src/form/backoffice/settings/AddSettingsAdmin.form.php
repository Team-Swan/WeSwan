<?php

class AddSettingsAdmin {

    function __construct()
    {


    }

    public function adminSettings()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/settings",
                "parent" => "conf",
                "class" => "form",
                "submit" => "Envoyer",
                "name" => "settings"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Title",
                    "id" => "title"
                ],
                "description" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Description",
                    "id" => "description"
                ],
                "email" => [
                    "element" => "input",
                    "type" => "email",
                    "label" => "E-Mail",
                    "id" => "email"
                ],
                "facebook" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Facebook",
                    "id" => "facebook"
                ],
                "twitter" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Twitter",
                    "id" => "twitter"
                ],
                "instagram" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Instagram",
                    "id" => "instagram"
                ],
                "googleplus" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Google plus",
                    "id" => "googleplus"
                ],
                "linkedin" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Linkedin",
                    "id" => "linkedin"
                ],
                "steam" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Steam",
                    "id" => "steam"
                ],
                "twitch" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Twitch",
                    "id" => "twitch"
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}