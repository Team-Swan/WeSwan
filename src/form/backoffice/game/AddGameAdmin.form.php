<?php

class AddGameAdmin {

    function __construct()
    {


    }

    public function addGame()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/addgame",
                "enctype" => "multipart/form-data",
                "parent" => "game",
                "class" => "form",
                "submit" => "Ajouter",
                "name" => "addgame"
            ],

            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Nom du jeu",
                    "id" => "gamename"
                ],
                "avatar" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Image",
                    "id" => "avatar"
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}
