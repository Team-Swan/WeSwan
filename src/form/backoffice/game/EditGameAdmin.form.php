<?php

class EditGameAdmin {

    function __construct()
    {


    }

    public function adminGame($data)
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/editgame/" . $data['id'],
                "parent" => "listGame",
                "class" => "form",
                "submit" => "Envoyer",
                "name" => "editGame"
            ],
            "data" => [
                "user" => [
                    "element" => "input",
                    "name" => "text",
                    "label" => "Ajouter un jeu",
                    "id" => "game",
                    "value" => $data['id']
                ],
                "avatar" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Image",
                    "id" => "avatar",
                    "values" => $data['image'],
                    "id_user" => $data['id']

                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}

