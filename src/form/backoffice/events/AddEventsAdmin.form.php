<?php


class AddEventsAdmin {

    function __construct()
    {


    }

    public function addEvents()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/addevent",
                "enctype" => "multipart/form-data",
                "parent" => "page",
                "class" => "form",
                "submit" => "Ajouter",
                "name" => "addevents"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Titre de la page",
                    "id" => "title"
                ],
                "content" => [
                    "element" => "textarea",
                    "type" => "text",
                    "label" => "Contenu de la page",
                    "id" => "content"
                ],
                "category" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Catégorie",
                    "id" => "category"
                ],
                "avatar" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Image",
                    "id" => "avatar"
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}