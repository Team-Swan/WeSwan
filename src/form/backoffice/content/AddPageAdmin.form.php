<?php


class AddPageAdmin {

    function __construct()
    {


    }

    public function addPage()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/addpage",
                "enctype" => "multipart/form-data",
                "parent" => "page",
                "class" => "form",
                "submit" => "Ajouter",
                "name" => "addpage"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Titre de la page",
                    "id" => "title"
                ],
                "content" => [
                    "element" => "textarea",
                    "type" => "text",
                    "label" => "Contenu de la page",
                    "id" => "content"
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}