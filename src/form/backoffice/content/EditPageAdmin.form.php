<?php


class EditPageAdmin {

    function __construct()
    {


    }

    public function editPage($data)
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/editpage/" . $data['id'],
                "enctype" => "multipart/form-data",
                "parent" => "page",
                "class" => "form",
                "submit" => "Modifier",
                "name" => "editpage"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Titre de la page",
                    "id" => "title",
                    "value" => $data['title']
                ],
                "content" => [
                    "element" => "textarea",
                    "type" => "text",
                    "label" => "Contenu de la page",
                    "id" => "content",
                    "value_selected" => $data['content']
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}