<?php

class AddAboutAdmin {

    function __construct()
    {


    }

    public function adminAbout()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/about",
                "parent" => "about",
                "class" => "form",
                "submit" => "Envoyer",
                "name" => "about"
            ],
            "data" => [
                "about" => [
                    "element" => "textarea",
                    "type" => "text",
                    "label" => "About",
                    "id" => "title"
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}
