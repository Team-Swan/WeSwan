<?php

class addGameAdmin {

    function __construct()
    {


    }

    public function adminGame()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/listUserGame",
                "parent" => "listUserGame",
                "class" => "form",
                "submit" => "Envoyer",
                "name" => "listUserGame"
            ],
            "data" => [
                "user" => [
                    "element" => "select",
                    "name" => "user",
                    "label" => "Ajouter un utilisateur",
                    "id" => "title",
                    "value" => ""
                ],
                "jeu" => [
                    "element" => "select",
                    "name" => "game",
                    "label" => "Jeu",
                    "id" => "title",
                    "value" => ""
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}