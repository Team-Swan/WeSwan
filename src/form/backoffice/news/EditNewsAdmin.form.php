<?php


class EditNewsAdmin {

    function __construct()
    {


    }

    public function editNews($data)
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/editnews/" . $data['id'],
                "enctype" => "multipart/form-data",
                "parent" => "page",
                "class" => "form",
                "submit" => "Modifier",
                "name" => "editpage"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Titre de la page",
                    "id" => "title",
                    "value" => $data['title']
                ],
                "content" => [
                    "element" => "textarea",
                    "type" => "text",
                    "label" => "Contenu de la page",
                    "id" => "content",
                    "value_selected" => $data['content']
                ],
                "category" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Catégorie",
                    "id" => "category",
                    "value" => $data['category']
                ],
                "avatar" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Image",
                    "id" => "avatar",
                    "values" => $data['image'],
                    "id_user" => $data['id']
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}