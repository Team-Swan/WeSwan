<?php


class AddUserAdmin {

    function __construct()
    {


    }

    public function addAdmin()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/adduser",
                "enctype" => "multipart/form-data",
                "parent" => "user",
                "class" => "form",
                "submit" => "Ajouter",
                "name" => "addUser"
            ],
            "data" => [
                "firstname" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Prénom",
                    "id" => "firstname"
                ],
                "lastname" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Nom",
                    "id" => "lastname"
                ],
                "birthday" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Date anniversaire",
                    "id" => "birthday"
                ],
                "username" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Pseudo",
                    "id" => "username"
                ],
                "email" => [
                    "element" => "input",
                    "type" => "email",
                    "label" => "E-Mail",
                    "id" => "email"
                ],
                "password" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Mot de passe",
                    "name" => "password_adduser",
                    "id" => "password_adduser"
                ],
                "role" => [
                    "element" => "select",
                    "name" => "role",
                    "label" => "Role",
                    "class" => "form_input",
                    "id" => "role",
                    "array_element" => [1 => "Invité", 2 => "Membre", 3 => "Modérateur", 4 => "Administrateur"],
                 ],
                "certify" => [
                    "element" => "select",
                    "name" => "certify",
                    "label" => "Activer le compte",
                    "class" => "form_input",
                    "id" => "certify",
                    "array_element" => [0 => "Non", 1 => "Oui"]
                ],
                "avatar" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Avatar",
                    "id" => "avatar"
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}