<?php


class EditUserAdmin {

    function __construct()
    {


    }

    public function editAdmin($get_user_info, $get_array_role)
    {
        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/edituser/".$get_user_info['id'],
                "enctype" => "multipart/form-data",

                "parent" => "user",
                "class" => "form",
                "submit" => "Modifier",
                "name" => "editUser"
            ],
            "data" => [
                "username" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Pseudo",
                    "id" => "username",
                    "value" => $get_user_info["username"],
                    "required" => 1
                ],
                "firstname" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Prénom",
                    "id" => "firstname",
                    "value" => $get_user_info["firstname"]
                ],
                "lastname" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Nom",
                    "id" => "lastname",
                    "value" => $get_user_info["lastname"]
                ],
                "birthday" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Date anniversaire",
                    "id" => "birthday",
                    "placeholder" => "dd/mm/YYYY",
                    "value" => $get_user_info["birthday"]
                ],
                "email" => [
                    "element" => "input",
                    "type" => "email",
                    "label" => "E-Mail",
                    "id" => "email",
                    "value" => $get_user_info["email"],
                    "required" => 1
                ],
                "password" => [
                    "element" => "input",
                    "type" => "password",
                    "label" => "Mot de passe",
                    "name" => "password_adduser",
                    "id" => "password_adduser"
                ],
                "role" => [
                    "element" => "select",
                    "name" => "role",
                    "label" => "Role",
                    "class" => "form_input",
                    "id" => "role",
                    "array_element" => $get_array_role,
                    "value_selected" => $get_user_info['id_role']
                ],
                "avatar" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Avatar",
                    "id" => "avatar",
                    "values" => $get_user_info['avatar'],
                    "id_user" => $get_user_info['id']
                ],
                "certify" => [
                    "element" => "select",
                    "name" => "certify",
                    "label" => "Activer le compte",
                    "class" => "form_input",
                    "id" => "certify",
                    "array_element" => [0 => "Non", 1 => "Oui"],
                    "value_selected" => $get_user_info['certify']
                ],
                "csrf" => [
                    "element" => "input",
                    "type" => "text",
                    "value" => CoreHelper::generateToken(),
                    "hidden" => 1,
                    "required" => 1
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 0
            ]
        ];

    }



}