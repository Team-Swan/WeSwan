<?php

class EditMenuAdmin {

    function __construct()
    {


    }

    public function adminMenu()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/menu",
                "parent" => "menu",
                "class" => "form",
                "submit" => "Envoyer",
                "name" => "menu",
                "value" => ""
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Ajouter un menu",
                    "id" => "title",
                    "value" => ""
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}