<?php

class AddMenuAdmin {

    function __construct()
    {


    }

    public function adminMenu()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/menu",
                "parent" => "menu",
                "class" => "form",
                "submit" => "Envoyer",
                "name" => "menu"
            ],
            "data" => [
                "title" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Ajouter un menu",
                    "id" => "title"
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];
    }

}