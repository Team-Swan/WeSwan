<?php


class AddTeamAdmin {

    function __construct()
    {


    }

    public function addAdminTeam()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/form/user/add",
                "enctype" => "multipart/form-data",
                "parent" => "team",
                "class" => "form",
                "submit" => "Ajouter",
                "name" => "addTeam"
            ],
            "data" => [
                "nom" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Nom de la team",
                    "id" => "name"
                ],
                "description" => [
                    "element" => "textarea",
                    "label" => "description",
                    "id" => "description"
                ],
                "jeux" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Jeux",
                    "id" => "game"
                ],
                "image" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Image",
                    "id" => "avatar"
                ],
                "leader" => [
                    "element" => "select",
                    "label" => "Leader de l'équipe",
                    "id" => "leader",
                    "name" => "leader"
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}