<?php


class EditTeamAdmin {

    function __construct()
    {


    }

    public function editAdminTeam()
    {

        return [
            "struct" => [
                "method" => "POST",
                "action" => ROOT_FOLDER."/admin/editTeam",
                "enctype" => "multipart/form-data",
                "parent" => "team",
                "class" => "form",
                "submit" => "Modifier",
                "name" => "editTeam"
            ],
            "data" => [
                "nom" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Nom de la team",
                    "id" => "name",
                    "value" => ""
                ],
                "description" => [
                    "element" => "textarea",
                    "label" => "description",
                    "id" => "description",
                    "value" => ""
                ],
                "jeux" => [
                    "element" => "input",
                    "type" => "text",
                    "label" => "Jeux",
                    "id" => "game",
                    "value" => ""
                ],
                "image" => [
                    "element" => "input",
                    "type" => "file",
                    "label" => "Image",
                    "id" => "avatar",
                    "value" => ""
                ],
                "leader" => [
                    "element" => "select",
                    "label" => "Leader de l'équipe",
                    "id" => "leader",
                    "name" => "leader",
                    "value" => ""
                ],
            ],
            "error" => [
                "id" => "error_sign_div",
                "control" => 1
            ]
        ];

    }



}