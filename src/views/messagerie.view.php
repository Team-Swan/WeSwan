
<div id="messagerie" class="container-fluid first">
    <h1 class="titre"><i class="fa fa-calendar" aria-hidden="true"></i> MAIL</h1>
    <div class="row">
            <script>
                var data = [];
                data = {
                    boxes: [
                        <?php if(is_array($messages_r)):?>
                        {
                            label: 'Reçus',
                            icon: 'mail',
                            mail: [
                                <?php $coma = '},'; $received = 0;?>
                                <?php foreach($messages_r as $message):?>
                                {
                                    Objet: '<?php echo htmlspecialchars($message['title']); ?>',
                                    Emetteur: '<?php echo htmlspecialchars($message['sender']); ?>',
                                    Destinataire: '<?php echo htmlspecialchars($message['received']); ?>',
                                    Date: '<?php echo CoreHelper::getShortdate($message['created_at'], 3); ?>',
                                    Message: "<?php echo  str_replace('"', '\"',html_entity_decode( str_replace("\n", "<br>", $message['content']))); ?>"

                                <?php if(count($messages_r)-1 == $received):?>
                                <?php $coma = '}'; ?>
                                <?php endif; ?>
                                <?php echo $coma; ?>
                                <?php $received++; ?>
                                <?php endforeach;?>
                            ]
                        },
                        <? endif; ?>
                        <?php if(is_array($messages_s)):?>
                        {
                            label: 'Envoyés',
                            icon: 'pan_tool',
                            mail: [
                                <?php $coma = '},'; ?>
                                <?php foreach($messages_s as $message):?>
                                {
                                    Objet: '<?php echo htmlspecialchars($message['title']); ?>',
                                    Emetteur: '<?php echo htmlspecialchars($message['sender']); ?>',
                                    Destinataire: '<?php echo htmlspecialchars($message['received']); ?>',
                                    Date: '<?php echo CoreHelper::getShortdate($message['created_at'], 3); ?>',
                                    Message: "<?php echo  str_replace('"', '\"',html_entity_decode(str_replace("\n", "<br>", $message['content']))); ?>"
                                <?php if(count($messages_s) == $message['id']):?>
                                <?php $coma = '}'; ?>
                                <?php endif; ?>
                                <?php echo $coma; ?>
                                <?php endforeach;?>
                            ]
                        }
                        <? endif; ?>
                    ]
                };
            </script>
        <div class="containerr">
            <div class="boxes"></div>
            <div class="mail">
                <table class="letters">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Objet</th>
                        <th>Emetteur</th>
                        <th>Destinataire</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="display"></div>
            </div>
        </div>
        <div style="background-color: #ececec; padding: 10px; margin-top: 20px;">
            <?php $this->includeModal("write_pm", $write_pm);?>
        </div>
    </div>
</div>