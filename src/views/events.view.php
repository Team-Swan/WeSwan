<div id="messagerie" class="container-fluid first">
    <h1 class="titre"><i class="fa fa-calendar" aria-hidden="true"></i> &Eacute;VENEMENTS</h1>
    <div class="row">

        <?php if(is_array($block_events)): ?>
            <?php foreach ($block_events as $article): ?>
                <figure class="article-block">
                    <img src="<?php echo $article['image'];?>" alt="<?php echo htmlspecialchars($article['title']);?>" />
                    <figcaption>
                        <h2><?php echo htmlspecialchars($article['title']);?></h2>
                        <p><?php echo $article['resume'];?></p><a href="<?php echo ROOT_FOLDER.'/'.$article['slug'];?>" class="read-more">En savoir plus</a>
                    </figcaption>
                </figure>
            <?php endforeach; ?>
        <?php endif; ?>

    </div>
    <?php if($page > 1) :?>
    <div id="pagination-buttons" class="container">
        <a href="<?php echo $page-1; ?>" class="btn btn-4"><span style="color:black">PREV</span></a>
        <?php endif; ?>

        <?php if(count($block_events) == 3) :?>
        <div class="pagi-next"> <a href="<?php echo $page+1; ?>" class="btn btn-4">
                <span style="color:black">NEXT</span></a>
        </div>
    </div>
<?php endif; ?>
</div>
</div>

