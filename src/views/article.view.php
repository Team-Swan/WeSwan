


<div class="fifth" style="background: url(<?php echo $article->getImage(); ?>);">


</div>
<div id="article_page" class="container-fluid">
    <h1 class="titre"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <?php echo htmlspecialchars($article->getTitle()); ?></h1>

    <div class="row ">
        <h2>PAR <?php echo htmlspecialchars($user->getUsername()); ?></h2>
        <p><?php echo nl2br(htmlspecialchars($article->getContent())); ?><p/>
    </div>

</div>

<?php $comment_form['struct']['action'] .= "/".$article->getId(); ?></p>

<div id="comments" class="comments container-fluid">
    <?php if(is_array($comments)): ?>
        <?php foreach($comments as $comment): ?>
            <div class="comment-wrap">
                <div class="photo">
                    <div class="avatar" style="background-image: url('<?php echo $comment['avatar']; ?>')"></div>
                </div>
                <div class="comment-block">
                    <p class="comment-text">
                    <h3><?php echo htmlspecialchars($comment['username']); ?></h3>
                    <p><?php echo nl2br( html_entity_decode( $comment['comment'])); ?></p>
                    <div class="bottom-comment">
                        <div class="comment-date"><?php echo CoreHelper::getShortdate($comment['created_at'], 3); ?></div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    <?php endif; ?>

    <?php if(isset($_SESSION['id'])): ?>
    <div class="comment-wrap pusher-right">
        <div class="comment-block">
            <?php $this->includeModal('comment_form', $comment_form); ?>
        </div>
    </div>
    <?php endif; ?>

</div>


