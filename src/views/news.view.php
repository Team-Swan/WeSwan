<div id="news" class="container first">
    <h1 class="titre"><i class="fa fa-newspaper-o" aria-hidden="true"></i> NEWS</h1>
    <div class="col-12 ">
        <div class="news_list col-3">

            <nav class="tri col-12">

                <ul>
                    <li id="all" class="active">all</li>
                    <?php if(is_array($categories)): ?>
                        <?php foreach($categories as $category): ?>
                            <li id="<?php echo htmlspecialchars($category['category']); ?>"><?php echo htmlspecialchars($category['category']); ?></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="clear"></div>
                </ul>
            </nav>
        </div>

        <div class="display-news">
            <?php if(is_array($news)) : ?>
                <?php foreach($news as $article): ?>
                    <div class="news-section col-2 team-card <?php echo $article['category'];?>">
                        <div class="wrapper" style="background: url(<?php echo $article['image'];?>) center/cover no-repeat;}">
                            <div class="head">
                                <div class="date">
                                    <?php
                                    if(isset($article['date_created'])) {
                                        $date = explode('/', CoreHelper::getShortdate($article['date_created'], 2));
                                        $day = $date[0];
                                        $month = $date[1];
                                        $year = $date[2];
                                        ?>
                                        <span class="day"><?php echo $day ?></span>
                                        <span class="month"><?php echo $month ?></span>
                                        <span class="year"><?php echo $year ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="data">
                                <div class="content">
                                    <span class="author"><?php echo htmlspecialchars($article['username']);?></span>
                                    <h1 class="title"><a href="<?php echo ROOT_FOLDER.''.$article['slug'];?>"><?php echo htmlspecialchars($article['title']);?></a></h1>
                                    <p class="text"><?php echo nl2br(htmlspecialchars($article['resume']));?></p>
                                    <a href="<?php echo ROOT_FOLDER.''.$article['slug']; ?>" class="button">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <div class="clear"></div>
    </div>
    <?php if($page > 1) :?>
    <div id="pagination-buttons" class="container">
        <a href="<?php echo $page-1; ?>" class="btn btn-4"><span>PREV</span></a>
        <?php endif; ?>

        <?php if(count($news) == 2) :?>
        <div class="pagi-next"> <a href="<?php echo $page+1; ?>" class="btn btn-4">
                <span>NEXT</span></a>
        </div>
    </div>
    <?php endif; ?>
</div>


