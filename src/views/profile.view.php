<div id="profil" class="">
    <div class="profile__wrapper">
        <div class="profile__information">
            <div class="profile__information-images">
                <div class="profile__information-image">
                    <img id="cat" class="profile__image-circle animal" src="<?php echo $user['avatar']; ?>">
                </div>
                <div class="profile__information-name">
                    <span class="profile__information-nametag"><?php echo htmlspecialchars($user['username']) ; ?></span>
                </div>
            </div>

            <div class="main-container col-12">

                <!-- HEADER -->
                <div class="block header-user">
                    <ul class="header-menu horizontal-list">

                    </ul>
                </div>

                <!-- LEFT-CONTAINER -->

                <div class="container-fluid">
                    <div class="col-3 left-container  float-left">

                        <div class="menu-box block"> <!-- MENU BOX (LEFT-CONTAINER) -->
                            <?php if($user['id'] == $_SESSION['id']): ?>
                            <h2 class="titular">MENU BOX</h2>
                            <ul class="menu-box-menu">
                                <li>
                                    <a class="menu-box-tab" href="<?php echo ROOT_FOLDER;?>messagerie"><span class="icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i></span>Messages Reçus<div class="menu-box-number"><?php echo $nb_msg_received[0]['nb']; ?></div></a>
                                </li>
                            </ul>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>

                <div id="" class="containerr float-left">
                    <div class="profile__information-details">

                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-globe"></div>
                                <div class="profile__information-detail-label">Rôle:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo htmlspecialchars($user['role']); ?></div>
                        </div>


                        <?php if($_SESSION['id'] == $user['id']): ?>
                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-envelope"></div>
                                <div class="profile__information-detail-label">Email:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo htmlspecialchars($user['email']); ?></div>
                        </div>
                        <?php endif; ?>


                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-globe"></div>
                                <div class="profile__information-detail-label">Nom:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo htmlspecialchars($user['lastname']); ?></div>
                            <?php if($user['id'] == $_SESSION['id']): ?>
                            <div class="profile__information-actions">
                                <div class="profile__information-action button--light waves-effect">
                                    <i class="fa fa-pencil"></i>
                                    <span id="lastname" user="<?php echo $user['id'] ?>" class="profile_change">Modifier</span>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-star"></div>
                                <div class="profile__information-detail-label">Prénom:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo htmlspecialchars($user['firstname']); ?></div>
                            <?php if($user['id'] == $_SESSION['id']): ?>
                            <div class="profile__information-actions">
                                <div class="profile__information-action button--light waves-effect">
                                    <i class="fa fa-pencil"></i>
                                    <span id="firstname" user="<?php echo $user['id'] ?>" class="profile_change">Modifier</span>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-briefcase"></div>
                                <div class="profile__information-detail-label">Membre depuis:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo CoreHelper::getShortdate($user['created_at']); ?></div>
                        </div>

                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-user"></div>
                                <div class="profile__information-detail-label">Dernière connexion:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo CoreHelper::getShortdate($user['last_connexion_at']); ?></div>
                        </div>

                        <?php if($_SESSION['id'] == $user['id']): ?>
                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-briefcase"></div>
                                <div class="profile__information-detail-label">Date d'anniversaire:</div>
                            </div>
                            <div class="profile__information-detail-text"><?php echo CoreHelper::getShortdate($user['birthday']); ?></div>
                            <?php if($user['id'] == $_SESSION['id']): ?>
                            <div class="profile__information-actions">
                                <div class="profile__information-action button--light waves-effect">
                                    <i class="fa fa-pencil"></i>
                                    <span id="birthday" user="<?php echo $user['id'] ?>" class="profile_change">Modifier</span>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>

                        <?php if($_SESSION['id'] == $user['id']): ?>
                        <div class="profile__information-detail">
                            <div class="profile__information-detail-content">
                                <div class="profile__information-detail-icon fa fa-lock"></div>
                                <div class="profile__information-detail-label">Mot de passe:</div>
                            </div>
                            <div class="profile__information-detail-text">*******</div>
                            <?php if($user['id'] == $_SESSION['id']): ?>
                            <div class="profile__information-actions">
                                <div class="profile__information-action button--light waves-effect">
                                    <i class="fa fa-pencil"></i>
                                    <span id="password" user="<?php echo $user['id'] ?>" class="profile_change">Modifier</span>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end main-container -->
</div>

