<div class="infomsg" id="<?php echo $config['error']['id']; ?>"></div>
<form method="<?php echo $config['struct']['method']; ?>" <?php echo isset($config['struct']['id'])?'name="'.$config['struct']['id'].'"':"" ?> <?php echo isset($config['struct']['id'])?'id="'.$config['struct']['id'].'"':"" ?> action="<?php echo $config['struct']['action']; ?>">
        <?php foreach ($config['data'] as $name => $attribut):?>
            <?php if($attribut['element'] == "input" && !isset($attribut['hidden'])):?>
                <label for="<?php echo $attribut['label']; ?>">
                    <input placeholder="<?php echo $attribut['label']; ?>"
                           type="<?php echo $attribut['type']; ?>"
                        <?php echo isset($attribut['hidden'])?'style="display:none;"':"" ?>
                        <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?>
                        id="<?php echo $name; ?>" name="<?php echo $name; ?>"
                        <?php echo isset($attribut['placeholder'])?'placeholder="'.$attribut['placeholder'].'"':"" ?>
                        <?php echo (isset($attribut['required']))?"required='required'":"";?>
                        <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>>

                </label>
            <?php endif; ?>
        <?php endforeach; ?>
    <div class="reset_form" style="display:none;">
        <label for="email">Saisir votre e-mail
            <input placeholder="E-mail" disabled="disabled" type="email" class="" id="reset_input" name="email" placeholder="">
        </label>
    </div>
    <?php foreach ($config['links'] as $a => $link) :?>
        <a style="display: block;" href="<?php echo $link['href']; ?>" <?php echo isset($link['class'])?'class="'.$link['class'].'"':"" ?>  <?php echo isset($link['class'])?'class="'.$link['class'].'"':"" ?> ><?php echo $link['content']; ?></a>
    <?php endforeach; ?>
    <input type="submit" id="login_submit" value="<?php echo $config['struct']['submit']; ?>">
</form>
<?php include 'ajax_form.mod.php' ?>