<div class="infomsg" id="<?php echo $config['error']['id']; ?>"></div>
<form class="wrap" method="<?php echo $config['struct']['method']; ?>"
    <?php echo isset($config['struct']['id'])?'id="'.$config['struct']['id'].'"':"" ?> action="
    <?php echo $config['struct']['action']; ?>">

    <?php foreach ($config['data'] as $name => $attribut):?>
        <?php if($attribut['element'] == "input" && !isset($attribut['hidden']) && ($attribut['type'] != "checkbox" )):?>
            <?php if(isset($_SESSION['listOfErrors'])):?>
                <?php $attribut['value'] = $_SESSION['oldData'][$name]; ?>
            <?php endif; ?>
            <label for="<?php echo $attribut['label']; ?>">
                <input placeholder="<?php echo $attribut['label']; ?>" type="<?php echo $attribut['type']; ?>"
                    <?php echo isset($attribut['id'])?'id="'.$attribut['id'].'':"" ?>
                    <?php echo isset($attribut['hidden'])?'style="display:none;"':"" ?>
                    <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?>
                    id=" <?php echo $name; ?>" name="<?php echo $name; ?>"
                    <?php echo isset($attribut['placeholder'])?'placeholder="'.$attribut['placeholder'].'"':"" ?>
                    <?php echo (isset($attribut['required']))?"required='required'":"";?>
                    <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>>
            </label>
        <?php endif; ?>
        <?php if($attribut['type'] == "checkbox" && !isset($attribut['hidden'])) : ?>

           <!-- Style checkbox-->


            <div id="checkbox_signin">

                <label for="<?php echo $label; ?>">
                    <input class="option-input checkbox"  type="<?php echo $attribut['type']; ?>"
                    <?php echo isset($attribut['id'])?'id="'.$attribut['id'].'':"" ?>
                    <?php echo isset($attribut['hidden'])?'style="display:none;"':"" ?>
                    <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?>id=" <?php echo $name; ?>" name="<?php echo $name; ?>"
                    <?php echo isset($attribut['placeholder'])?'placeholder="'.$attribut['placeholder'].'"':"" ?>
                    <?php echo (isset($attribut['required']))?"required='required'":"";?>
                    <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>

                    />
                    Vous acceptez les conditions d'utilisation du site
                </label>


            </div>




        <?php endif; ?>
    <?php endforeach; ?>
    <?php if($config['data']['captcha']):?>
        <img src="./captcha.php" width="200" height="70" border="1" alt="CAPTCHA">
        <label for="captcha">Veuillez saisir le message de l'image ci-dessus
            <input placeholder="Captcha" type="text" size="6" maxlength="5" id="captcha" name="captcha" value="" required="required">
        </label>
    <?php endif; ?>
    <input type="submit" value="<?php echo $config['struct']['submit']; ?>">
</form>