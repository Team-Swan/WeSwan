<?php

// Handles Ajax Form calls dynamically


$ajaxData = '';
echo '
<script>
    $("#'.$config['struct']['id'].'").unbind().on("submit", function(e) {

        if("'.$config['links'][0]['form_name'].'" == $(this).attr("name")) {
            e.preventDefault();
            ';
            if(!$config['struct']['hasReset']) {
                foreach ($config['data'] as $name => $attribut) {
                    echo 'var ' . $name . ' = $("#' . $attribut['id'] . '_' . $config['struct']['name'] . '").val();';
                }
            }
            echo '
            $.ajax({
                type: "POST",
                url: $("#'.$config['struct']['id'].'").attr("action"),
                content: "application/json; charset=utf-8",
                dataType: "html",
                ';

                if($config['struct']['hasReset']) {
                    $ajaxData = '"&email="+ $("#'.$config['struct']['id'].'").find("#reset_input").val(),';
                } else {
                    foreach($config['data'] as $name => $attribut) {
                        $ajaxData .= '"&'.$name.'="'.$name.',';
                    }
                }
                echo '
                data: "'.substr(substr($ajaxData, 2), 0, -1).',
                success: function(msg) {
                    $("#'.$config['error']['id'].'").html(msg);
                },
                error: function (xhr, textStatus, errorThrown) {
                    // TODO: Show error
                }
            });
        }';
    echo '});
</script>';
