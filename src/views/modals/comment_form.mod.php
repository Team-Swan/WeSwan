<form method="<?php echo $config['struct']['method']; ?>" style="width:100%;" <?php echo isset($config['struct']['id'])?'name="'.$config['struct']['id'].'"':"" ?> <?php echo isset($config['struct']['id'])?'id="'.$config['struct']['id'].'"':"" ?> action="<?php echo $config['struct']['action']; ?>">
    <?php foreach ($config['data'] as $name => $attribut):?>
        <?php if($attribut['element'] == "input" && isset($attribut['hidden'])):?>
            <label for="<?php echo $attribut['label']; ?>">
                <input placeholder="<?php echo $attribut['label']; ?>"  type="<?php echo $attribut['type']; ?>" <?php echo isset($attribut['hidden'])?'style="display:none;"':"" ?> <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?> id="<?php echo $name; ?>" name="<?php echo $name; ?>" <?php echo isset($attribut['placeholder'])?'placeholder="'.$attribut['placeholder'].'"':"" ?> <?php echo (isset($attribut['required']))?"required='required'":"";?> <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>>
            </label>
            <span><?php echo $attribut['label']; ?></span>
        <?php endif; ?>
        <?php if($attribut['element'] == "textarea" && !isset($attribut['hidden'])):?>
            <textarea <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?>  rows="<?php echo $attribut['rows']; ?>"  cols="<?php echo $attribut['cols']; ?>" id="<?php echo $name; ?>" name="<?php echo $name; ?>" <?php echo (isset($attribut['required']))?"required='required'":"";?> <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>></textarea>
        <?php endif; ?>
    <?php endforeach; ?>
    <input class="float-right" style="margin-top:10px;padding:20px;text-align: center;" type="submit" id="comment_submit" value="<?php echo $config['struct']['submit']; ?>">
</form>