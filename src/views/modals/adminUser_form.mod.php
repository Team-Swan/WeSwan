<div class="infomsg" id="<?php echo $config['error']['id']; ?>"></div>
<form method="<?php echo $config['struct']['method']; ?>"<?php echo isset($config['struct']['id']) ? 'id="' . $config['struct']['id'] . '"' : "" ?>
      action="<?php echo $config['struct']['action']; ?>" <?php echo isset($config['struct']['enctype']) ? 'enctype="' . $config['struct']['enctype'] . '"' : "" ?>>
    <div class="adminform">
        <div class="oneCol">
            <?php foreach ($config['data'] as $name => $attribut): ?>
                <?php if ($attribut['element'] == "input" && !isset($attribut['hidden'])): ?>
                    <?php if (isset($_SESSION['listOfErrors'])):
                        if (isset($_SESSION['oldData'][$name])):?>
                            <?php $attribut['value'] = $_SESSION['oldData'][$name]; ?>
                        <?php endif; endif; ?>
                    <label for="<?php echo $attribut['label']; ?>"><strong><?php echo $attribut['label']; ?></strong>
                        <input type="<?php echo $attribut['type']; ?>" <?php echo isset($attribut['id']) ? 'id="' . $attribut['id'] . '"' : "" ?> <?php echo isset($attribut['hidden']) ? 'style="display:none;"' : "" ?> <?php echo isset($attribut['class']) ? 'class="' . $attribut['class'] . '"' : "" ?>
                               name="<?php echo $name; ?>" <?php echo isset($attribut['placeholder']) ? 'placeholder="' . $attribut['placeholder'] . '"' : "" ?> <?php echo (isset($attribut['required'])) ? "required='required'" : ""; ?> <?php echo isset($attribut['value']) ? 'value="' . $attribut['value'] . '"' : "" ?>>
                    </label>
                    <?php
                    if ($name == "password"):
                        ?>
                        Mot de passe aléatoire : <i id="random_password" class="fa-2x fa fa-random"></i>  <br>
                        Afficher / Masquer : <i id="lock_unlock" class="fa-2x fa fa-eye"></i>

                        <?php
                    endif;

                /*
                 * SELECT
                 *
                 */
                elseif ($attribut['element'] == "select" && !isset($attribut['hidden'])): ?>
                    <label for="<?php echo $name; ?>"><strong><?php echo $attribut['label']; ?></strong>
                        <select name="<?php echo $name; ?>" <?php echo isset($attribut['class']) ? 'class="' . $attribut['class'] . '"' : "" ?> <?php echo isset($attribut['id']) ? 'id="' . $attribut['id'] . '"' : "" ?> <?php echo (isset($attribut['required'])) ? "required='required'" : ""; ?>>
                            <?php
                            $value_selected = "";
                            if (isset($attribut['value_selected'])):
                                $value_selected = $attribut['value_selected'];
                            else:
                                $value_selected = "";
                            endif;
                            foreach ($attribut['array_element'] as $name => $attribut): ?>
                                <option <?php echo ($value_selected == $name) ? "selected='selected'" : ""; ?>
                                        value="<?php echo $name; ?>"><?php echo $attribut; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                <?php endif; ?>
            <?php endforeach; ?>
            <label for="submit" id="submit">
                <input type="submit" name="<?php echo $config['struct']['name']; ?>" value="<?php echo $config['struct']['submit']; ?>">
            </label>
        </div>
    </div>
</form>