<?php if(is_array($config)): ?>
    <?php foreach ($config as $article): ?>
        <div class="news-section team-card <?php echo htmlspecialchars($article['category']);?>">
            <div class="wrapper" style="background: url(<?php echo $article['image'];?>) center/cover no-repeat;}">
                <div class="head">
                    <div class="date">
                        <?php
                        $date = explode('/', CoreHelper::getShortdate($article['date_created'], 2));
                        $day = $date[0];
                        $month = $date[1];
                        $year = $date[2];
                        ?>
                        <span class="day"><?php echo $day ?></span>
                        <span class="month"><?php echo $month ?></span>
                        <span class="year"><?php echo $year ?></span>
                    </div>
                </div>
                <div class="data">
                    <div class="content">
                        <span class="author"><?php echo htmlspecialchars($article['username']);?></span>
                        <h1 class="title"><a href="<?php echo ROOT_FOLDER.'/'.$article['slug'];?>"><?php echo htmlspecialchars($article['title']);?></a></h1>
                        <p class="text"><?php echo nl2br(htmlspecialchars($article['resume']));?></p>
                        <a href="<?php echo ROOT_FOLDER.'/'.$article['slug'];?>" class="button">En savoir plus</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>