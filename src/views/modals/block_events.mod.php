<?php if(is_array($config)): ?>
    <?php foreach ($config as $article): ?>
        <figure class="article-block">
            <img src="<?php echo $article['image'];?>" alt="<?php echo $article['title'];?>" />
            <figcaption>
                <h2><?php echo $article['title'];?></h2>
                <p><?php echo $article['resume'];?></p><a href="<?php echo ROOT_FOLDER.'/'.$article['slug'];?>" class="read-more">En savoir plus</a>
            </figcaption>
        </figure>
    <?php endforeach; ?>
<?php endif; ?>