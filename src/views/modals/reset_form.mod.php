<div class="infomsg" id="<?php echo $config['error']['id']; ?>"></div>
<form method="<?php echo $config['struct']['method']; ?>" <?php echo isset($config['struct']['id'])?'name="'.$config['struct']['id'].'"':"" ?> <?php echo isset($config['struct']['id'])?'id="'.$config['struct']['id'].'"':"" ?> action="<?php echo $config['struct']['action']; ?>">
    <?php foreach ($config['data'] as $name => $attribut):?>
        <?php if($attribut['element'] == "input" && !isset($attribut['hidden'])):?>
            <label for="<?php echo $attribut['label']; ?>"><?php echo $attribut['label'];?>
                <input type="<?php echo $attribut['type']; ?>" <?php echo isset($attribut['id'])?'id="'.$attribut['id'].'':"" ?> <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?> id="<?php echo $name; ?>" name="<?php echo $name; ?>" <?php echo isset($attribut['placeholder'])?'placeholder="'.$attribut['placeholder'].'"':"" ?> <?php echo (isset($attribut['required']))?"required='required'":"";?> <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>>
            </label>
        <?php endif; ?>
    <?php if($attribut['element'] == "input" && $attribut['hidden']):?>
            <input type="<?php echo $attribut['type']; ?>" <?php echo isset($attribut['id'])?'id="'.$attribut['id'].'':"" ?> <?php echo isset($attribut['hidden'])?'style="display:none;"':"" ?> <?php echo isset($attribut['class'])?'class="'.$attribut['class'].'"':"" ?> id="<?php echo $name; ?>" name="<?php echo $name; ?>" <?php echo isset($attribut['placeholder'])?'placeholder="'.$attribut['placeholder'].'"':"" ?> <?php echo (isset($attribut['required']))?"required='required'":"";?> <?php echo isset($attribut['value'])?'value="'.$attribut['value'].'"':"" ?>>
        <?php endif; ?>
    <?php endforeach; ?>
    <input type="submit" id="login_submit" value="<?php echo $config['struct']['submit']; ?>">
</form>

<?php include 'ajax_form.mod.php' ?>