<div id="profil" class="">
    <div class="profile__wrapper">
        <div class="profile__information">
            <div class="profile__information-images">
                <div class="profile__information-image">
                    <img class="profile__image-circle" src="<?php  echo ($team)?$team->getImage():""; ?>" alt="avatar user">
                </div>
                <div class="profile__information-name">
                    <span class="profile__information-nametag">
                </div>
            </div>

            <div class="main-container col-12">

                <!-- HEADER -->
                <div class="block header-user">
                    <ul class="header-menu horizontal-list">
                    </ul>t
                </div>


                <div id="" class="container-fluid ">
                    <h2>DESCRIPTION</h2>
                    <p><?php echo nl2br(htmlspecialchars($team->getContent())); ?></p>
                </div>


                <?php $isPlayer = 0; ?>
                <?php if(is_array($players)): ?>
                    <!-- LEFT-CONTAINER -->
                    <div class="container-fluid">
                        <div class="col-3 left-containerr ">
                            <h2>NOS JOUEURS</h2>
                            <div class="material clearfix">
                                <div class="tabs-content">
                                    <div class="friend-list">
                                        <div class="list-ul">
                                            <?php foreach($players as $player): ?>
                                                <?php if($player['username'] == $_SESSION['username'])
                                                    $isPlayer = 1;
                                                ?>
                                                <div class="list-li clearfix">
                                                    <div class="photo pull-left"><img src="<?php echo htmlspecialchars($player['avatar']); ?>"></div>
                                                    <div class="info pull-left">
                                                        <div class="name"><a class="name" href="<?php echo ROOT_FOLDER."user/profile/".$player['username']; ?>"><?php echo htmlspecialchars($player['username']); ?></a> - <?php echo htmlspecialchars($player['role']); ?></div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if(isset($_SESSION['id']) && $_SESSION['id'] == $team->getLeader()): ?>
                    <div style="margin-top: 50px;" class="container-fluid ">
                        <h2>Demandes pour rejoindre ton équipe</h2>
                        <?php if(is_array($demands)): ?>
                            <?php foreach($demands as $demand): ?>
                                <a href="<?php echo ROOT_FOLDER."user/profile/".$demand['username']; ?>"<h4 style="color:black;"><?php echo htmlspecialchars($demand['username']); ?></h4></a> -> <a style="color:#A9D822" href="<?php echo ROOT_FOLDER."team/accept/".$demand['username']."/".$team->getId(); ?>">Accepter</a><br>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if(isset($_SESSION['id']) && !$isPlayer): ?>
                    <div style="margin-top: 50px;" class="container-fluid ">
                        <h2>Intéressé par cette équipe?</h2>
                        <?php $join_team['struct']['action'] .= "/".$team->getId(); ?></p>
                        <?php $this->includeModal("join_team", $join_team);?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div> <!-- end main-container -->
</div>

