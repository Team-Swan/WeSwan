<div class="first">
    <script>var news_slide = [];</script>
    <?php if(is_array($slide_news)): ?>
        <?php
        $cpt = 0;
        foreach($slide_news as $news):
            $tab_news_slide[$cpt]['link'] = $news['slug'];
            $tab_news_slide[$cpt]['url'] = $news['image'];
            $tab_news_slide[$cpt]['title'] = mb_substr($news['title'], 0, 30).'...';
            $tab_news_slide[$cpt]['desc'] = mb_substr($news['content'], 0, 50).'...';
            $cpt++;
        endforeach;?>
        <script> news_slide = <?php echo json_encode($tab_news_slide); ?>;</script>
    <?php endif; ?>
    <div id="slideshow"></div>

    <noscript><div style="text-align:center;"><p>Please enable JavaScript in your browser for better use of the website.</p></div></noscript>

</div>


<div class="third"></div>


<div id="news" class="container-fluid first">
    <div class="all-features">
        <h1 class="titre"><i class="fa fa-newspaper-o" aria-hidden="true"></i> NEWS</h1>
        <div class="row ">
            <?php $this->includeModal("block_articles", $block_articles);?>
        </div>
        <div class="more-info col-12">
            <a href="<?php echo ROOT_FOLDER;?>news/1"" class="btn btn-1">
                <svg>
                    <rect x="0" y="0" fill="none" width="100%" height="100%"/>
                </svg>
                VOIR PLUS
            </a>
        </div>

    </div>
</div>

<div class="third"></div>

<div class="container-fluid first">
    <div class="all-features">
        <h1 class="titre"><i class="fa fa-calendar" aria-hidden="true"></i> EVENEMENTS</h1>
        <div id="articles" class="row">
            <div id="article-block">
                <?php $this->includeModal("block_events", $block_events);?>
            </div>
            <div class="more-info col-12">
                <a href="<?php echo ROOT_FOLDER;?>event/1" class="btn btn-1">
                    <svg>
                        <rect x="0" y="0" fill="none" width="100%" height="100%"/>
                    </svg>
                    VOIR PLUS
                </a>
            </div>

        </div>
    </div>
</div>

<div class="third"></div>

<div class="container-fluid first">
    <div class="all-features">
        <h1 class="titre"><i class="fa fa-calendar" aria-hidden="true"></i> FLUX RSS</h1>
        <div>
        </div>
    </div>
</div

<div class="third"></div>

<div class="site-cache" id="site-cache"></div>

