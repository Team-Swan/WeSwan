<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Back office WeSwan CMS</title>
    <meta name="description" content="Créez votre site e-sport avec WeSwan, un cms adapté à tout type de jeu! Venez essayer!">
    <meta name="author" content="WeSwan">
    <link href="<?php echo ROOT_FOLDER; ?>/public/css/main.css" rel="stylesheet">
    <script src="https://use.fontawesome.com/a59c5afcc7.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<main class="page-content">
    <div class="nav-top">
        <div class="logo">
            <a href="<?php echo ROOT_FOLDER ?>/admin/">
                <img src="<?php echo ROOT_FOLDER; ?>/public/img/logo.png" alt="logo weswan">
            </a>
            <h1><a href="<?php echo ROOT_FOLDER ?>/admin/">We Swan</a></h1>
        </div>
        <span class="menuBurger"><i class="fa fa-bars"></i></span>
    </div>

    <section class="left-column">
        <div class="accordion">
            <i class="fa fa-dashboard"></i>
            <a href="<?php echo ROOT_FOLDER ?>/admin/">Dashboard</a>
        </div>
        <button class="accordion">
            <i class="fa fa-cogs"></i> Parameters
        </button>
        <ul class="panel">
            <li>
                <a <?php echo trim($_SERVER['REQUEST_URI'], '/'); ?> href="<?php echo BASE_PATH_PATTERN ?>admin/settings"> <i class="fa fa-wrench"></i> Settings</a>
            </li>
        </ul>
        <button class="accordion">
            <i class="fa fa-users"></i> Users
        </button>
        <ul class="panel">
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listusers"><i class="fa fa-user"></i> List</a>
            </li>
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listroles"><i class="fa fa-graduation-cap "></i> Role</a>
            </li>
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listbanned"><i class="fa fa-ban"></i> Bans</a>
            </li>
        </ul>
        <button class="accordion">
            <i class="fa fa-dashboard"></i> Content
        </button>
        <ul class="panel">
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listpages"><i class="fa fa-book "></i> Pages</a>
            </li>
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listnews"><i class="fa fa-newspaper-o"></i> News</a>
            </li>
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listevents"><i class="fa fa-calendar"></i> Events</a>
            </li>
        </ul>
        <button class="accordion"><i class="fa fa-gamepad"></i> Gaming</button>
        <ul class="panel">
            <li>
                <a href="<?php echo ROOT_FOLDER ?>/admin/listGames"><i class="fa fa-gamepad"></i> Games</a>
            </li>
        </ul>
        <div class="accordion">
            <a href="<?php echo ROOT_FOLDER ?>/admin/about"><i class="fa fa-info"></i> About</a>
        </div>
    </section>

    <section class="right-column">
        <?php include "src/views/".$this->view; ?>
    </section>
</main>
on>
</main>

<footer>
    <?php if(isset($_SESSION['listOfSuccess']) && is_array($_SESSION['listOfSuccess'])): ?>
        <div class="bottom-right notify do-show" data-notification-status="success">
            <ul>
                <?php foreach($_SESSION['listOfSuccess'] as $error): ?>
                    <li class="col-sm-12"><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php unset($_SESSION['listOfSuccess']); ?>
    <?php endif; ?>

    <?php if(isset($_SESSION['listOfErrors']) && is_array($_SESSION['listOfErrors'])): ?>
        <div class="bottom-right notify do-show" data-notification-status="error">
            <ul>
                <?php foreach($_SESSION['listOfErrors'] as $error): ?>
                    <li class="col-sm-12"><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php unset($_SESSION['listOfErrors']); ?>
    <?php endif; ?></footer>
<script>
    var root_folder = '<?php echo ROOT_FOLDER; ?>';
</script>
<script src='<?php echo ROOT_FOLDER; ?>/public/js/main.js?<?php echo JS_VERSION; ?>'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=mgt9ko5cwhugw5zxgr8zxkw7dduuasc0uvmwce0a68m544cl"></script>
<script>tinymce.init({ selector:'textarea', plugins: "image", });</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }
    $(".menuBurger").click(function(){
        $(".left-column").toggleClass("active");
        console.log("test");
    });


</script>
</body>
</html>