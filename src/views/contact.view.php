
        <div id="showcase">
            <section id="section3" class="scroll-based bounce-up bg-showcase2 in-view">
                <div class="container-fluid">
                    <h1 class="titre"><b>WeSwan</b></h1>
                    <div class="row">
                        <div class="col-7">
                            <p class="lead">WeSwan est un CMS (système de gestion de contenu) à la fois puissant, compact et performant, pour créer votre site web orienté eSport !</p>
                            <h3 class="text-primary">C'est entièrement <b>gratuit</b> et <b>personnalisable</b> !</h3>
                            <p>Peu importe votre niveau dans le domaine du web, ce projet a pour but de vous proposer une solution clés en main pour créer votre site à l'aide d'interfaces modernes, personnalisables et évolutives pour correspondre à un maximum d'univers.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>


