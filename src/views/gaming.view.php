<div id="lineup" class="container-fluid first">
        <h1 class="titre"><i class="fa fa-newspaper-o" aria-hidden="true"></i> LINE UP</h1>

        <div class="row ">
            <?php if(is_array($teams)): ?>
                <?php foreach($teams as $team): ?>
                <article class="team-card">
                    <header class="team-cardThumb">
                        <a href="<?php echo ROOT_FOLDER;?>team/index/<?php echo htmlspecialchars($team['name']);?>"">
                            <img src="<?php echo $team['image']?>">
                        </a>
                    </header>

                    <div class="team-cardBody">
                        <div class="team-cardCategory"><a href="<?php echo ROOT_FOLDER;?>team/index/<?php echo $team['name']?>"><?php echo htmlspecialchars($team['game']); ?></a></div>
                        <h2 class="team-cardTitle"><a href="<?php echo ROOT_FOLDER;?>team/index/<?php echo $team['name']?>">Team <?php echo htmlspecialchars($team['name']); ?></a></h2>
                        <p class="team-cardDescription">
                            <?php echo substr(htmlspecialchars($team['content']), 0, 250).'...'; ?>
                        </p>
                    </div>
                    <footer class="team-cardFooter">
                        <span class="icon icon--time"></span><?php echo CoreHelper::getShortdate($team['created_at']); ?>
                    </footer>
                </article>
                <?php endforeach; ?>

                <?php if($page > 1) :?>
                    <div id="pagination-buttons" class="container">
                        <a href="<?php echo $page-1; ?>" class="btn btn-4"><span>PREV</span></a>

                <?php endif; ?>

                <?php if(count($teams) == 3) :?>
                        <div class="pagi-next"> <a href="<?php echo $page+1; ?>" class="btn btn-4">
                                <span>NEXT</span></a>
                        </div>
                    </div>
                <?php endif; ?>



            <?php endif; ?>

        </div>
</div>