
</div>
</div>
</div>
<footer class="footer-distributed">

    <?php
    $user = new User();
    $login_form = $user->getLoginForm();
    $sign_form = $user->getSignForm();
    ?>
    <div id="somedialog" class="dialog">
        <div class="dialog-overlay"></div>
        <div class="dialog-content">
            <button role="button" class="close btnn-close" title="Close"><span></span></button>
            <?php
            $this->includeModal("login_form", $login_form);
            ?>
        </div>

    </div>

    <div class="modal_info">
        <?php
        $this->includeModal("sign_form", $sign_form);
        ?>
    </div>
    <div class="modal_overlay"></div>


    <div class="footer-right">
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-youtube-play"></i></a>
        <a href="#"><i class="fa fa-twitch"></i></a>
    </div>

    <div class="footer-left">
        <p class="footer-company-name"><a href=""><img src="<?php echo ROOT_FOLDER; ?>public/img/logo.png" width="100" alt="logo WeSwan"></p></a>
        <p class="footer-links">
            <a href="<?php echo ROOT_FOLDER;?>">ACCUEIL</a>
            ·
            <a href="<?php echo ROOT_FOLDER;?>news/1">NEWS</a>
            ·
            <a href="<?php echo ROOT_FOLDER;?>gaming/1">LINE UP</a>
            ·
            <a href="<?php echo ROOT_FOLDER;?>event/1">&Eacute;VENEMENTS</a>
            ·
            <a href="<?php echo ROOT_FOLDER;?>stream">STREAM</a>
            ·
            <a href="<?php echo ROOT_FOLDER;?>contact">A PROPOS</a>
        </p>
    </div>

    <?php if(isset($_SESSION['listOfSuccess']) && is_array($_SESSION['listOfSuccess'])): ?>
        <div class="bottom-right notify do-show" data-notification-status="success">
            <ul>
                <?php foreach($_SESSION['listOfSuccess'] as $error): ?>
                    <li class="" style="list-style: none"><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php unset($_SESSION['listOfSuccess']); ?>
    <?php endif; ?>

    <?php if(isset($_SESSION['listOfErrors']) && is_array($_SESSION['listOfErrors'])): ?>
        <div class="bottom-right notify do-show" data-notification-status="error">
            <ul>
        <?php foreach($_SESSION['listOfErrors'] as $error): ?>
            <li class="" style="list-style: none"><?php echo $error; ?></li>
        <?php endforeach; ?>
            </ul>
        </div>
        <?php unset($_SESSION['listOfErrors']); ?>
    <?php endif; ?>

</footer>
<div class="copyrights">
    Designed, Developed &amp; Hosted By WeSwan &copy; 2017
        <br> All Rights Reserved.
</div>
<script>
    var root_folder = '<?php echo (ROOT_FOLDER)?ROOT_FOLDER:''; ?>';
</script>
<script src='<?php echo ROOT_FOLDER; ?>public/js/configSlideShow.js?<?php echo JS_VERSION; ?>'></script>
<script src='<?php echo ROOT_FOLDER; ?>public/js/main.js?<?php echo JS_VERSION; ?>'></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=mgt9ko5cwhugw5zxgr8zxkw7dduuasc0uvmwce0a68m544cl"></script>
<script>tinymce.init({ selector:'textarea', plugins: "image", });</script>
</body>
</html>