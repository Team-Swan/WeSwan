<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WeSwan CMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Créez votre site e-sport avec WeSwan, un cms adapté à tout type de jeu! Venez essayer!">
    <meta name="author" content="WeSwan">
    <link href="<?php echo ROOT_FOLDER; ?>public/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo ROOT_FOLDER; ?>public/css/font-awesome-animation.min.css" rel="stylesheet">
    <link href="<?php echo ROOT_FOLDER; ?>public/css/main.css?<?php echo CSS_VERSION; ?>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>