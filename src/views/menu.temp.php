<div class="site-container">
    <div class="site-pusher">
        <header class="header nav-down">
            <div class="col-12 top-header">
                <div class="col-4 social-links">
                    <ul>
                        <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-twitch" aria-hidden="true"></i></a></li>
                    </ul>
                </div>


                <div class="col-4 logo_links">
                    <a href="<?php echo ROOT_FOLDER; ?>" class="header__icon" id="header__icon"></a>
                    <a href="<?php echo ROOT_FOLDER; ?>"><img src="<?php echo ROOT_FOLDER; ?>public/img/logo.png" alt="logo WeSwan" class="header__logo"><h1>WESWAN</h1></a>
                </div>

                <div class="col-2 pusher-right text-right ">
                    <div class="">
                        <?php if(isset($_SESSION['id'])) {?>
                        <div class="profile">
                            <figure>
                                <a href="<?php echo ROOT_FOLDER;?>user/profile">
                                    <img src="<?php echo $_SESSION['avatar']; ?>" alt="" />
                                </a>
                            </figure>
                            <div class="toggle">
                                <input type="checkbox" class="view_details" id="view_details">
                                <label for="view_details" title="Ouvrir le profil">=</label>
                            </div>
                            <main>
                                <dl>
                                    <a href="<?php echo ROOT_FOLDER;?>user/logout"><h3>Deconnexion</h3></a>
                                    <a href="<?php echo ROOT_FOLDER;?>messagerie"><h3>Messagerie</h3></a>

                                </dl>
                            </main>
                            <?php } else { ?>
                                <div id="auth" class="btn_container">
                                    <a href="#" class="open btnn"><h3>Connexion</h3></a>
                                    <a class="open_button" href="#"><h3>Inscription</h3></a>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="col-12 menu text-center">
                        <nav class="navmenu">
                            <a class="scroll" data-speed="2000" href="<?php echo ROOT_FOLDER;?>">ACCUEIL</a>
                            <a class="scroll" data-speed="2000" href="<?php echo ROOT_FOLDER;?>news/1">NEWS</a>
                            <a class="scroll" data-speed="2000" href="<?php echo ROOT_FOLDER;?>gaming/1">LINE UP</a>
                            <a class="scroll" data-speed="2000" href="<?php echo ROOT_FOLDER;?>event/1">&Eacute;VENEMENTS</a>
                            <a class="scroll" data-speed="2000" href="<?php echo ROOT_FOLDER;?>stream">STREAM</a>
                            <a class="scroll" data-speed="2000" href="<?php echo ROOT_FOLDER;?>contact">A PROPOS</a>
                        </nav>
                    </div>
                    <nav class="burger">
                        <a href="#" class="burger__button" id="burger-button">
                            <span class="burger__button__icon"></span>
                        </a>
                        <ul class="burger__menu">
                            <li><a href="<?php echo ROOT_FOLDER;?>user/conpage">CONNEXION</a></li>
                            <li><a href="<?php echo ROOT_FOLDER;?>user/inspage">INSCRIPTION</a></li>
                    </nav>
                </div>

        </header>
        <div class="site-content">