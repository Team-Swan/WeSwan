<div class="form-style-6">
    <form action="POST" enctype="multipart/form-data">
        <h1>Menu</h1>

        <div class="adminform">
            <?php
            if ($ActionPage == "edit") {

                if (isset($adminMenu_form) != NULL) {
                    $this->includeModal("adminMenu_form", $adminMenu_form);
                } else {
                    echo "l'id n'existe pas";
                }
            } else {
                $this->includeModal("adminMenu_form", $adminMenu_form);
            }

            ?>

        </div>

        <div class="adminform">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Delete</th>
                </tr>
                <tr>
                    <td>Accueil</td>
                    <td><i class="fa fa-trash"></i> <a href="<?php echo BASE_PATH_PATTERN."admin/delete/menu/1"; ?>">Delete</a></td>
                </tr>
            </table>
        </div>
    </form>
</div>