<div class="form-style-6">
    <h1><?php echo $ActionPage; ?> page</h1>

    <?php
    if ($ActionPage == "edit") {
        if (isset($editAdminPage_form) != NULL) {
            $this->includeModal("editAdminPage_form", $editAdminPage_form);
        } else {
            echo "l'id n'existe pas";
        }
    } else if($ActionPage == "add") {
        $this->includeModal("addAdminPage_form", $addAdminPage_form);
    }
    ?>
</div>
