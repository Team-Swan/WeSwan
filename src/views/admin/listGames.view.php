<div class="form-style-6">
    <h1>Liste des jeux</h1>

    <div class="adminform">
        <div class="oneCol">
            <?php $this->includeModal("addAdminGames_form", $addAdminGames_form); ?>
        </div>
    </div>

    <div class="adminform">

        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Afficher</th>
            </tr>
            </thead>
            <tbody>
            <?php
            for ($i = 0; $i < count($get_game_liste); $i++) {
                ?>
                <tr>
                    <td><?php echo htmlspecialchars($get_game_liste[$i]['name']); ?></td>
                    <td><a href="<?php echo ROOT_FOLDER . "admin/".($get_game_liste[$i]['status'] == 1 ? "unDisplayGame/" . intval($get_game_liste[$i]['id']) : "displayGame/" . intval($get_role[$i]['id'])); ?>"
                           alt="display"><i <?php echo ($get_game_liste[$i]['status'] == 1) ? "style='color:red;'" : ""; ?>
                                    class="fa fa-eye"></i></a></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
