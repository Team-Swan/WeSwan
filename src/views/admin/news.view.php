<div class="form-style-6">
    <h1><?php echo $ActionPage; ?> news</h1>

    <?php
    if ($ActionPage == "edit") {
        if (isset($editAdminNews_form) != NULL) {
            $this->includeModal("editAdminNews_form", $editAdminNews_form);
        } else {
            echo "l'id n'existe pas";
        }
    } else if($ActionPage == "add") {
        $this->includeModal("addAdminNews_form", $addAdminNews_form);
    }
    ?>
</div>
