<h1>Liste des rôles</h1>

<div class="adminform">
    <div class="oneCol">
        <?php $this->includeModal("addAdminRole_form", $addAdminRole_form); ?>
    </div>
</div>

<div class="adminform">

    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Edition</th>
            <th>Afficher</th>
        </tr>
        </thead>
        <tbody>
        <?php
        for ($i = 0; $i < count($get_role); $i++) {
            ?>
            <tr>
                <td><?php echo htmlspecialchars($get_role[$i]['name']); ?></td>
                <td><a href="<?php echo ROOT_FOLDER . "admin/editroles/" . intval($get_role[$i]['id']); ?>"
                       alt="Edit"><i class="fa fa-edit"></i></a></td>

                <td><a href="<?php echo ROOT_FOLDER . "admin/".($get_role[$i]['status'] == 1 ? "unDisplayrole/" . intval($get_role[$i]['id']) : "displayRole/" . intval($get_role[$i]['id'])); ?>"
                       alt="display"><i <?php echo ($get_role[$i]['status'] == 1) ? "style='color:red;'" : ""; ?>
                                class="fa fa-eye"></i></a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
