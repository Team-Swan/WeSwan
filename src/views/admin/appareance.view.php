<form action="POST" enctype="multipart/form-data">
    <h1><?php echo $ActionPage; ?> user profil</h1>
    <div class="adminform">
        <div class="oneCol">
            <label for="username"><strong>Username</strong> <input type="text" name="username" value=""></label>
            <label for="firstname"><strong>Firstname</strong> <input type="text" name="firstname" value=""></label>
            <label for="lastname"><strong>Lastname</strong> <input type="text" name="lastname" value=""></label>
            <label for="email"><strong>E-mail</strong> <input type="email" name="email" value=""></label>
            <label for="birthday"><strong>Birthday</strong> <input type="date" name="birthday"></label>
            <label for="password"><strong>Password</strong> <input type="password" name="password"></label>
            <label for=""><strong>Rôle</strong>
                <select name="role">
                    <option value="1">Administrateur</option>
                    <option value="1">Modérateur</option>
                    <option value="1">Rédacteur</option>
                </select>
            </label>
            <label for="avatar">
                <strong>Avatar</strong>
                <input type="file" name="avatar">
            </label>
            <label for="submit" id="submit">
                <input type="submit" placeholder="Submit">
            </label>
        </div>
    </div>
</form>