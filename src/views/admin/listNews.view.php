<h1>Liste des news</h1>

<div class="buttonAdd">
    <a href="<?php echo ROOT_FOLDER."admin/addnews"; ?>"><i class="fa fa-plus"></i> ajouter une news</a>
</div>

<div class="adminform">
    <table>
        <thead>
        <tr>
            <th>Titre</th>
            <th>Date</th>
            <th>Auteur</th>
            <th>Editer</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <?php

        for ($i = 0; $i < count($get_news); $i++) {
            ?>
            <tr>
                <td><?php echo $get_news[$i]['title']; ?></td>
                <td><?php echo htmlspecialchars($get_news[$i]['date_created']); ?></td>
                <td><?php echo htmlspecialchars($get_user_name[0]['username']); ?></td>
                <td><i class="fa fa-edit"></i> <a href="<?php echo ROOT_FOLDER."admin/editnews/" . intval($get_news[$i]['id']); ?>">Edit</a></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/deletenews/" . intval($get_news[$i]['id']); ?>" alt="Delete"><i <?php echo ($get_news[$i]['status'] != 1) ? "style='color:red;'" : ""; ?> class="fa fa-trash"></i></a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>