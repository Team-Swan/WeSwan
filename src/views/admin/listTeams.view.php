<h1>List teams</h1>

<div class="buttonAdd">
    <a href="<?php echo ROOT_FOLDER."admin/addteam"; ?>"><i class="fa fa-plus"></i> Add team</a>
</div>

<div class="adminform">
    <table>
        <thead>
            <tr>
                <th>Team</th>
                <th>Game</th>
                <th>Number</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>TeamFUT</td>
                <td>FIFA</td>
                <td>4</td>
                <td><i class="fa fa-edit"></i><a href="<?php echo ROOT_FOLDER."admin/editteam"; ?>"> edit</a></td>
                <td><i class="fa fa-trash"></i><a href="<?php echo ROOT_FOLDER."admin/delete/team/1"; ?>"> delete</a></td>
            </tr>
        </tbody>
    </table>
</div>
