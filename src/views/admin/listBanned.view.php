<h1>Liste des utilisateurs bannis</h1>


<?php

?>
<div class="adminform">
    <table>
        <thead>
        <tr>
            <th>Pseudo</th>
            <th>E-mail</th>
            <th>Rôle</th>
            <th>Voir sa fiche</th>
            <th>Supprimé</th>
            <th>Debannir</th>
        </tr>
        </thead>
        <tbody>
        <?php
        for ($i = 0; $i < count($get_user); $i++) {
            ?>
            <tr>
                <td><?php echo htmlspecialchars($get_user[$i]['username']); ?></td>
                <td><?php echo htmlspecialchars($get_user[$i]['email']); ?></td>
                <td><?php echo htmlspecialchars($get_role_user[$i]['name']); ?></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/edituser/" . intval($get_user[$i]['id']); ?>" alt="voir sa fiche"><i class="fa fa-eye"></i></a></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/delete/user/" . intval($get_user[$i]['id']); ?>" alt="Delete"><i <?php echo ($get_user[$i]['is_deleted'] == 1) ? "style='color:red;'" : ""; ?> class="fa fa-trash"></i></a></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/unBanUser/" . intval($get_user[$i]['id']); ?>" alt="Ban"><i <?php echo ($get_user[$i]['is_banned'] == 1) ? "style='color:red;'" : ""; ?> class="fa fa-ban"></i></a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
