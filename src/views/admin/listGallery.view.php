<h1>List Gallery</h1>

<div class="buttonAdd">
    <a href="<?php echo ROOT_FOLDER."admin/form/gallery/add"; ?>"><i class="fa fa-plus"></i> Add gallery</a>
</div>

<div class="adminform">
    <table>
        <thead>
            <tr>
                <th>Album</th>
                <th>Categorie</th>
                <th>Date</th>
                <th>Number</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Guild Wars 2</td>
                <td>Guild Wars 2</td>
                <td>13/06/2017 12:45</td>
                <td>3</td>
                <td><i class="fa fa-edit"></i> <a href="<?php echo ROOT_FOLDER."admin/form/gallery/edit"; ?>">edit</a></td>
                <td><i class="fa fa-trash"></i> <a href="<?php echo ROOT_FOLDER."admin/delete/gallery/1"; ?>">delete</a></td>
            </tr>
        </tbody>
    </table>
</div>
