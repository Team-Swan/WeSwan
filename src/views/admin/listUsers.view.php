<h1>Liste des utilisateurs</h1>

<div class="buttonAdd">
    <a href="<?php echo ROOT_FOLDER."admin/adduser"; ?>"><i class="fa fa-plus"></i> Add user</a>
</div>
<?php

?>
<div class="adminform">
    <table>
        <thead>
        <tr>
            <th>Pseudo</th>
            <th>E-mail</th>
            <th>Rôle</th>
            <th>Edition</th>
            <th>Suppression</th>
            <th>Bannir</th>
        </tr>
        </thead>
        <tbody>
        <?php
        for ($i = 0; $i < count($get_user); $i++) {
            ?>
            <tr>
                <td><?php echo htmlspecialchars($get_user[$i]['username']); ?></td>
                <td><?php echo htmlspecialchars($get_user[$i]['email']); ?></td>
                <td><?php echo htmlspecialchars($get_role_user[$i]['name']); ?></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/edituser/" . intval($get_user[$i]['id']); ?>" alt="Edit"><i class="fa fa-edit"></i></a></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/delete/user/" . intval($get_user[$i]['id']); ?>" alt="Delete"><i <?php echo ($get_user[$i]['is_deleted'] == 1) ? "style='color:red;'" : ""; ?> class="fa fa-trash"></i></a></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/banUser/" . intval($get_user[$i]['id']); ?>" alt="Ban"><i <?php echo ($get_user[$i]['is_banned'] == 1) ? "style='color:red;'" : ""; ?> class="fa fa-ban"></i></a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>