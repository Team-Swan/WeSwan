<h1>Liste des evenements</h1>

<div class="buttonAdd">
    <a href="<?php echo ROOT_FOLDER."admin/addevent"; ?>"><i class="fa fa-plus"></i> ajouter un evenement</a>
</div>

<div class="adminform">
    <table>
        <thead>
        <tr>
            <th>Titre</th>
            <th>Date</th>
            <th>Auteur</th>
            <th>Editer</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <?php

        for ($i = 0; $i < count($get_events); $i++) {
            ?>
            <tr>
                <td><?php echo $get_events[$i]['title']; ?></td>
                <td><?php echo htmlspecialchars($get_events[$i]['date_created']); ?></td>
                <td><?php echo htmlspecialchars($get_user_name[0]['username']); ?></td>
                <td><i class="fa fa-edit"></i> <a href="<?php echo ROOT_FOLDER."admin/editevent/" . intval($get_events[$i]['id']); ?>">Edit</a></td>
                <td><a href="<?php echo ROOT_FOLDER."admin/deleteevent/" . intval($get_events[$i]['id']); ?>" alt="Delete"><i <?php echo ($get_events[$i]['status'] != 1) ? "style='color:red;'" : ""; ?> class="fa fa-trash"></i></a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>