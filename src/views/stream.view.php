<script>
    var users = [];
</script>
<?php if(is_array($streamers_list)): ?>
    <?php foreach($streamers_list as $streamer):?>
        <script>
            users.push('<?php echo $streamer['username'];?>');
        </script>
    <?php endforeach; ?>
<?php endif; ?>
<div id="stream" class="container first">
    <div class="all-features">
        <h1 class="titre"><i class="fa fa-television" aria-hidden="true"></i> STREAM</h1>
        <div class="">
            <div col-12>
                <div class=" col-3 body col-mobile">
                    <!-- Loading bar while Twitch API is getting ready -->
                    <div class="load-bar">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                    </div> <!-- End Loading bar -->

                    <div class="container border-radius"> <!-- Main container for all elements-->

                        <div class="section"> <!-- Header with icon, search bar and buttons-->
                            <div class="header__main">
                                <a href="" title="Twitch.tv Streamers"><i class="fa fa-twitch header__logo" aria-hidden="true"></i></a>
                                <!--<input class="header__input" type="search" name="" value="" data-search placeholder="Find a streamer">-->
                            </div>

                            <div class="availability"> <!-- All / Online / Offlne Buttons -->
                                <button class="availability__btn availability__btn--all" id="all" type="button" name="button">All</button>
                                <button class="availability__btn availability__btn--on" id="on" type="button" name="button">Online</button>
                                <button class="availability__btn availability__btn--off" id="off" type="button" name="button">Offline</button>
                            </div> <!-- End Buttons-->
                        </div> <!-- End Header -->

                        <div class="section main"></div>
                    </div> <!-- End Main container-->
                </div>
                <div class="col-8 display-stream col-mobile">
                    <div class="exemple-iframe">
                        <div class="float-left videoWrapper col-5 col-mobile">
                            <iframe  src="https://player.twitch.tv/?channel=PGL" frameborder="0" allowfullscreen="true" scrolling="no" height="400" width="720"></iframe>
                            <a href="https://www.twitch.tv/PGL?tt_medium=live_embed&tt_content=text_link" style="padding:2px 0px 4px; display:block; width:345px; font-weight:normal; font-size:10px; text-decoration:underline;"></a>
                        </div>
                        <div class="float-right videoWrapper  col-5 col-mobile">
                            <iframe class="float-right" src="https://www.twitch.tv/pgl/chat?popout=" frameborder="0" scrolling="no" height="500" width="350"></iframe>
                        </div>

                </div>

            </div>
        </div>
    </div>
</div>