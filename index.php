<?php
session_start();

require "conf.inc.php";

require "src/helpers/UserHelper.class.php";
require "src/helpers/CoreHelper.class.php";
require "vendor/phpmailer/phpmailer/PHPMailerAutoload.php";
//require "vendor/RSSFeed/RSSFeed.class.php";

if(file_exists('configDB.inc.php')) {
    require 'configDB.inc.php';
}


if(!file_exists('configDB.inc.php')) {
    $step = (!isset($_SESSION['step'])) ? 1 : $_SESSION['step'];
    header('Location: install/');
    die();
}



spl_autoload_register(function ($class) {
    if (file_exists("src/core/" . $class . ".class.php")) {
        include "src/core/" . $class . ".class.php";
    } else if (file_exists("src/models/" . $class . ".class.php")) {
        include "src/models/" . $class . ".class.php";
    } else {
        // For optimisation use "USE \form\...\toto.form.php but not functionnal on this CMS.
        $directory = 'src/form/';
        foreach (scandir($directory) as $readDir) {
            $sub_directory = 'src/form/' . $readDir . '/';
            foreach (scandir($sub_directory) as $readDir) {
                if (file_exists($sub_directory . $readDir . "/" . $class . ".form.php")) {
                    include $sub_directory . $readDir . "/" . $class . ".form.php";
                }
            }
        }
    }
});



$route = new Routing();


