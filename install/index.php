<?php
session_start();
require 'includes/config.php';
$curdir = dirname($_SERVER['REQUEST_URI']);
$step = 0;
if ($_SESSION['step'] > 4) {
    $_SESSION['step'] = 0;
    $step = 0;
} else if (time() > $_SESSION['time'] + MAXTIME) {
    $_SESSION['step'] = 0;
    $step = 0;
    $_SESSION['data_step'] = [];

}
require 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WeSwan - Install</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<!-- multistep form -->
<div id="msform">
    <img src="../public/img/logo.png" width="200px" alt="">
    <fieldset>
        <?php
        if (file_exists('../configDB.inc.php')) {
            ?>
            <p class="error">La base de données existe déjà.</p>
            <?php
        } else {
            if (isset($_GET['error']) && $_GET['error'] == "1") {
                if (isset($_SESSION['error']['step_' . $_SESSION['step']])) {
                    ?>
                    <p class="error"><?php echo $_SESSION['error']['step_' . $_SESSION['step']]; ?></p>
                    <?php
                }
            }

            switch ($_SESSION['step']) {
            case 0:
            $_SESSION['time'] = time();
            $_SESSION['step'] = 0;
            $error = FALSE;
            ?>
            Bienvenue dans l'installer de votre CMS de jeux vidéos.
            <table>
                <thead>
                <tr>
                    <th>Config</th>
                    <th>Statut</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>CHMOD 777 en écriture (Minimum 755)</td>
                    <?php
                    if (is_writable("../")) {
                        echo "<td class='approuval'>Approuvé</td>";
                    } else {
                        echo "<td class='desapprouval'>Désapprouvé</td>";
                        $error = TRUE;
                    }
                    ?>
                </tr>
                <tr>
                    <td>Version PHP >= 5.6.0</td>
                    <?php
                    if (version_compare(PHP_VERSION, "5.6.0", ">")) {
                        echo "<td class='approuval'>Approuvé</td>";
                    } else {
                        echo "<td class='desapprouval'>Désapprouvé</td>";
                        $error = TRUE;
                    }
                    ?>

                </tr>
                <tr>
                    <td>MySQL PDO</td>
                    <?php
                    if (extension_loaded("PDO")) {
                        echo "<td class='approuval'>Approuvé</td>";
                    } else {
                        echo "<td class='desapprouval'>Désapprouvé</td>";
                        $error = TRUE;
                    }
                    ?>
                </tr>
                </tbody>
            </table>
            <?php
            if ($error != TRUE) {
            ?>
            <form action="step.php" method="POST" id="formStep0">
                <input type="submit" class="action-button" value="Aller à l'étape 1">
                <?php
                }
                break;
                case 1:
                    $_SESSION['time'] = time();
                    $_SESSION['step'] = 1;
                    echo "Setp 1";
                    ?>

                    <br><br>

                    <p id="error-ajax" class="error"></p>
                    <form action="step.php" method="POST" id="formStep1">
                        <label for="db_host">Hôte</label>
                        <input type="text" name="db_host" id="db_host" required="required">
                        <label for="db_user">Nom d'utilisateur</label>
                        <input type="text" name="db_user" id="db_user" required="required">
                        <label for="db_password">Mot de passe</label>
                        <input type="text" name="db_password" id="db_password">
                        <label for="db_name">Nom de la base de donnée</label>
                        <input type="text" id="db_dbname" name="db_dbname" required="required">
                        <label for="db_port">Port</label>
                        <input type="number" name="db_port" id="db_port" value="3306" placeholder="Default">
                        <button class="test action-button">Tester la connexion</button>
                        <div class="centerElement loader"></div>
                        <input type="submit" class="action-button" id="displayStep1" value="Aller à l'étape 2">
                    </form>
                    <?php
                    break;
                case 2:
                    $_SESSION['time'] = time();
                    $_SESSION['step'] = 2;
                    echo "Setp 2";
                    ?>
                    <br><br>
                    <form action="step.php" method="POST" id="formStep2">
                        <label for="site_name">Nom du site</label>
                        <input type="text" name="site_name" id="site_name" value="" required="required">
                        <label for="site_desc">Description du site</label>
                        <textarea name="site_desc" id="site_desc"></textarea>
                        <label for="site_link">Lien du site (URL)</label>
                        <input type="text" name="site_link" id="site_link" required="required">
                        <input type="submit" class="action-button" value="Aller à l'étape 3">
                    </form>
                    <?php
                    break;
                case 3:
                    $_SESSION['time'] = time();
                    $_SESSION['step'] = 3;
                    echo "Setp 3";
                    ?>
                    <br><br>
                    <form action="step.php" method="POST" id="formStep3">
                        <label for="account_pseudo">Pseudo</label>
                        <input type="text" name="account_pseudo" id="account_pseudo" required="required">
                        <label for="account_mail">E-mail</label>
                        <input type="email" name="account_mail" id="account_mail" required="required">
                        <label for="account_password">Mot de passe</label>
                        <div class="accout_password">
                            <input type="text" name="account_password" id="account_password" disabled="disabled"
                                   required="required" value="<?php echo getRandom(); ?>">
                            <button class="hideShow"></button>
                        </div>
                        Choisir mon propre mot de passe : <input type="checkbox" value="yes" name="account_choose_password"
                                                                 id="account_choose_password">
                        <input type="submit" class="action-button" value="Valider">
                    </form>
                    <?php
                    break;
                case 4:
                    ?>
                    <button class="installDatabase action-button">Installer la base de donnée</button>
                    <div class="centerElement loader"></div>
                    <p class="text-loader">Installation de la base de données en cours...</p>
                    <div class="contentMessage">
                    </div>
                    <?php

                    break;
                default:
                    break;
                }
            }
            ?>
    </fieldset>
</div>
</body>
<script src="js/jquery.min.js"></script>
<script src="js/steps/step1.js"></script>
<script src="js/steps/step3.js"></script>
<script src="js/steps/step4.js"></script>
</html>