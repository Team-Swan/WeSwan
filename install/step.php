<?php
session_start();
require 'includes/config.php';
$curdir = dirname($_SERVER['REQUEST_URI']);
if ($_SERVER['REQUEST_URI'] == $curdir . "/install/") {
    if (time() > $_SESSION['time'] + MAXTIME) {
        $_SESSION['step'] = 0;
        $step = 1;
        $_SESSION['data_step'] = [];
    }
    header('Location: index.php');
    die();
}
switch ($_SESSION['step']) {
    case 1:
        $_SESSION['error']['step_' . $_SESSION['step']] = "";
        if (empty($_POST['db_host']) && empty($_POST['db_user']) && empty($_POST['db_password']) && empty($_POST['db_dbname'])) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "Les champs ne doivent pas être vides.";
            header('Location: index.php?error=1');
            die();
        } else {
            try {
                @$db = new PDO('mysql:host=' . $_POST['db_host'] . ';dbname=' . $_POST['db_dbname'] . ';port=' . intval($_POST['db_port']) . ';charset=utf8',
                    $_POST['db_user'], $_POST['db_password'], [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
            } catch (Exception $e) {
                $_SESSION['error']['step_' . $_SESSION['step']] .= "La connexion à la base de donnée a échouée";
                header('Location: index.php?error=1');
                die();
            }
        }
        break;
    case 2:
        if (empty($_POST['site_name']) && empty($_POST['site_link'])) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "Les champs ne doivent pas être vides.";
            header('Location: index.php?error=1');
            die();
        } else if (strlen($_POST['site_name']) > 30 || strlen($_POST['site_name']) < 1) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "Le nom du site doit être compris entre 1 et 30 caractères.";
            header('Location: index.php?error=1');
            die();
        } else if (!filter_var($_POST['site_link'], FILTER_VALIDATE_URL)) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "L'URL du site est incorrect.";
            header('Location: index.php?error=1');
            die();
        }
        break;
    case 3:
        if (empty($_POST['account_pseudo']) && empty($_POST['account_mail']) && empty($_POST['account_password'])) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "Les champs ne doivent pas être vides.";
            header('Location: index.php?error=1');
            die();
        } else if (strlen($_POST['account_pseudo']) > 15 || strlen($_POST['account_pseudo']) < 3) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "Le pseudo doit être comprit entre 3 et 15 caractères.";
            header('Location: index.php?error=1');
            die();
        } else if (!filter_var($_POST['account_mail'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error']['step_' . $_SESSION['step']] .= "L'email est incorrect.";
            header('Location: index.php?error=1');
            die();
        }
        break;
    case 4:
        break;
    default:
}

$_SESSION["data_step"]['step' . $_SESSION['step']] = $_POST;
$_SESSION['step'] += 1;
$urlLocation = 'index.php';

header('Location: ' . $urlLocation);
die();




