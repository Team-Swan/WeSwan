$(document).ready(function() {



    var randomCaracter = function () {
        var listString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var string = '';
        for (var i = 0; i < 15; i++) {
            string = string + listString[Math.floor(Math.random() * listString.length)];
        }
        return string;
    }
    $('input[name="account_password"]').val(randomCaracter());


    $('#account_choose_password').on('change', function () {
        if ($('input[name="account_choose_password"]').is(':checked')) {
            if (confirm("Vous êtes sûr de vouloir choisir votre propre mot de passe ?")) {
                $('#account_password').val('');
                $('#account_password').focus();
                $('#account_password').removeAttr('disabled');

            }
            else {
                $('input[name="account_choose_password"]').prop('checked', false);
            }

        } else {
            $('#account_password').val(randomCaracter());
        }
    });

    $('button').on('click', function (e) {
        e.preventDefault();

        if ($("#account_password").attr("type") === "text") {
            $("#account_password").attr("type", "password");
        }
        else {
            $("#account_password").attr("type", "text");
        }

    });

});
