$(document).ready(function() {



    $('.loader').css('display', 'none');
    $('.text-loader').css('display', 'none');
    $('.installDatabase').click(function (e) {
        $('.loader').css('display', 'block');
        $('.text-loader').css('display', 'block');
        e.preventDefault();
        $('.installDatabase').hide();
        var request2 = $.ajax({
            url: 'updateall.php',
            dataType: "json",
        });

        request2.done(function (data) {
            $('.contentMessage').html('<p class="' + data.notification + '">' + data.message + '</p>');
            $('.loader').css('display', 'none');
            $('.text-loader').css('display', 'none');
            if(data.getError == 1) {
               var getCurrentSplited = $(location).attr('href').split('/install/');
                window.location.href = getCurrentSplited[0] + '/admin/';
            }
        });

        request2.fail(function (jqXHR, textStatus) {
            $('.contentMessage').html('<p class="error">Erreur lors de l\'insertion en base de données.<br>Actualisez pour recommencer l\'insertion.</p>');
            $('.loader').css('display', 'none');
            $('.text-loader').css('display', 'none');
        });
    });

});
