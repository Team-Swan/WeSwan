$(document).ready(function() {

    $('.modalco').css('display', 'block');
    $('.close-modalco').on('click', function (e) {
        e.preventDefault();
        $('.modalco').css('display', 'none');
    });


    var htmlEntities = function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    $('#displayStep1').hide();
    $('#error-ajax').hide();
    $('.test').click(function (e) {
        e.preventDefault();
        var db_host = $('#db_host').val();
        var db_user = $('#db_user').val();
        var db_password = $('#db_password').val();
        var db_dbname = $('#db_dbname').val();
        var db_port = $('#db_port').val();
        var getInputs = {};
        $('#formStep1 > input').each(function () {
            getInputs[htmlEntities($(this).attr('id'))] = htmlEntities($(this).val());
        });

        $('.loader').css('display', 'block');
        var request = $.ajax({
            url: 'ajax.php',
            method: "POST",
            data: {getElements: getInputs},
            dataType: "json",
        });

        request.done(function (data) {
            if (data.connection == false) {
                $('#error-ajax').fadeIn(500).html(data.message);
                $('#displayStep1').hide();
            } else {
                $('#displayStep1').fadeIn(500);
                $('#error-ajax').hide();
            }
            $('.loader').css('display', 'none');

        });

        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });

    });

});