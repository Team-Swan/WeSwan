-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mer 26 Juillet 2017 à 16:05
-- Version du serveur :  5.5.42
-- Version de PHP :  7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `weswan_cms`
--

-- --------------------------------------------------------

--
-- Structure de la table `we_comment`
--

CREATE TABLE `we_comment` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `id_content` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_bin NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_comment`
--

INSERT INTO `we_comment` (`id`, `id_user`, `id_content`, `comment`, `date_created`) VALUES
(1, 1, 1, '&lt;p&gt;dzedzedze&lt;/p&gt;', '2017-07-26 11:29:32');

-- --------------------------------------------------------

--
-- Structure de la table `we_config`
--

CREATE TABLE `we_config` (
  `id` int(10) unsigned NOT NULL,
  `wording` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` text COLLATE utf8_bin
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_config`
--

INSERT INTO `we_config` (`id`, `wording`, `value`) VALUES
(1, 'site_name', 'AltrosGaming'),
(2, 'site_desc', 'Altroos'),
(3, 'site_link', 'https://altrosgaming.com');

-- --------------------------------------------------------

--
-- Structure de la table `we_content`
--

CREATE TABLE `we_content` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `title` varchar(150) COLLATE utf8_bin NOT NULL,
  `resume` mediumtext COLLATE utf8_bin,
  `content` mediumtext COLLATE utf8_bin NOT NULL,
  `image` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_bin NOT NULL,
  `category` varchar(30) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_content`
--

INSERT INTO `we_content` (`id`, `id_user`, `title`, `resume`, `content`, `image`, `created_at`, `date_updated`, `type`, `status`, `slug`, `category`) VALUES
(1, 1, 'Le succès de LoL', 'Comment accrocher le public', 'Ca y est, LoL a été installé sur votre ordinateur, et rien qu''avec ça, il est déjà allé plus loin que l''immense majorité des jeux disponibles sur le marché. Il s''agit maintenant de faire du nouveau joueur un membre à part entière de la communauté, il s''agit de faire naître la passion.\n \nUn moba assez facile d''accès\n \nHeroes of the Storm étant arrivé un peu tard dans la course, le principal rival de LoL dans la course au MOBA fut Dota 2 mais alors que le MOBA est dors et déjà un style de jeu abrupte au toucher pour les nouvelles recrues, DOTA n''a vraiment fait aucun effort pour se rendre plus facile d''accès. Le pull ( faire s''affronter les sbires et les monstres neutres pour faire reculer les lanes ), le deny ( tuer ses propres sbires et alliés pour empêcher l''équipe adverse d''en toucher les golds ), la perte de gold lors d''une mort ou encore l''utilisation des coursiers sont autant de mécaniques fondamentales de Dota 2 qui n''existent pas sur LoL et qui repoussent d''autant le moment ou le jeune joueur peut dire qu''il "connait les bases".\n \n\n Dans Dota presque tous les items de tier 3 ont un actif.\n \nEntendons nous bien, LoL n''est pas casual, tout joueur s''essayant à un MOBA pour la première fois est instantanément perdu, mais il est parmi les MOBA, l''un des plus facile d''accès. Cela permet de faire arriver rapidement le moment où le joueur sent qu''il a un impact sur la partie, et évite la plupart des désinstallations dues à la frustration de ne pas progresser assez vite.\n \nFacile à aborder mais dur à maitriser\n \nUne prise en main simplifiée ne s''oppose pas à une grande profondeur de jeu : League of Legend comporte une courbe de progression  presque infinie : entre les mécaniques, la prise de décision et la connaissance générale du jeu ce sont des années d''effort qui sont nécessaires pour se hisser en haut du classement. Sur ce point LoL a été plus malin que Dota, Riot a compris qu''une entrée en matière facilitée ne signifiait pas le sacrifice de la profondeur de jeu.\n \n\nPages de runes, masteries, build order, pour les amateurs de théorycraft LoL offre tout de même de quoi faire.\n \nC''est là toute la magie : détruire des tours pour avancer jusqu''au nexus est un principe simplissime et le joueur qui fait ses premières parties comprend immédiatement ce qu''il doit faire. Mais à chaque fois qu''il relance une partie il comprend un peu mieux comment il doit le faire, comment être plus rapide, plus efficace, plus utile à ses coéquipiers.\nC''est la même chose avec l''équipement : de l''AD sur les personnages AD, de l''AP sur les mages et des résistances sur les tanks, tout cela est très intuitif, n''importe qui ayant compris ne peut pas se tromper entièrement dans son build. Par contre, trouver le build le plus optimisé possible demande du temps, de la patience, et une sacré connaissance de jeu.\n \nUne expérience renforcée par la compétition entre joueurs\n \nComme pour tous les jeux multijoueurs dotés d''un classement, il fait bon occuper les derniers échelons de l''échelle du skill. Plus facile en effet de se vanter à la récréation quand on est master 300 lp que quand on est silver 4. Mais quelque chose vient donner aux parties de MOBA une saveur toute particulière : la vue du dessus. Cette vue qui rend la navigation sur la carte si facile et qui permet de regarder   ( et de juger ) les actions de ses coéquipiers.\nLes joueurs de CS:GO connaissent bien cet instant où ils sont le dernier joueur en vie, et où il tient à eux de remporter le round. Ce qui rend ces instants si tendus, c''est de savoir que l''équipe entière est en train de scruter le moindre de vos geste. Sur LoL, cette sensation est permanente.\n \n\nL''âme du jeu, c''est avant-tout la compétition entre les joueurs.\n \nSur LoL, tout est exacerbé : la réussite comme la défaite, la gloire comme la honte. C''est pourquoi un mauvais move est aussi frustrant et pourquoi un play incroyable rend aussi heureux. Ce sentiment d''être toujours dans la lumière génère des sensations très fortes, si fortes qu''elles en sont addictives. Ce sont probablement ces sensations plus que n''importe quoi d''autre, qui font revenir les joueurs vers le jeu encore et encore.', 'public/img/third2.jpg', '2017-07-16 15:28:21', NULL, 'news', 1, 'news/show/league-of-legends/comment-accrocher-le-public', 'league-of-legends'),
(2, 1, 'Gagner contre un deck cochon', NULL, 'Dans la suite de la série d''articles stratégiques pour revoir et optimiser son jeu, Millenium vous propose aujourd''hui de faire le point sur les principales erreurs à éviter lorsque l''on tombe contre un adversaire jouant un deck cochon, une des cartes les plus jouées du jeu.\r\n \r\nDépenser trop d''elixir pour se défendre\r\nLes decks cochons cyclent très rapidement et permettent au joueur de faire de nombreux pushs au cours de la partie. A dépenser trop d''élixir pour se défendre au début, on creuse petit à petit l''écart et on prend le risque de finir par ne plus pouvoir poser quoique ce soit plus tard dans la partie.\r\n \r\nMal choisir sa défense\r\nLa majorité des decks cochons contiennent des sorts du type bûche, zap, flèches ... pouvant anéantir d''un seul coup ou presque les cartes comme l''armée des squelettes, gargouilles ou gobelins. Si vous optez pour une défense de ce type pour arrêter le push du chevaucheur, assurez-vous d''abord que votre adversaire ait déjà joué son sort de contre et qu''il ne l''ait pas en main à ce moment-là. Sinon, votre tentative de défense est vouée à l''échec.\r\n \r\nNe pas se préparer à une attaque multi lane\r\nCe type de deck permet aisément à votre adversaire à mener ses attaques sur deux lanes à la fois, et les bons joueurs le savent bien. Aussi, ne vous focalisez pas entièrement sur une seule lane et gardez toujours en tête d''avoir besoin de défendre sur les 2 lanes à tout moment de la partie.\r\n \r\nRater le placement de ses bâtiments\r\nUn bâtiment placé trop tôt ou trop loin sur la map ne jouera pas son rôle efficacement et peut coûter cher lorsque l''on joue contre ce type de carte à visée exclusive des bâtiments. A défaut d''avoir une distraction, le chevaucheur ira se rabattre sur votre tour, lui infligeant de gros dégâts.', 'public/img/first-small.jpg', '2017-07-16 15:30:07', NULL, 'news', 0, 'news/show/clash-royale/gagner-contre-un-deck-cochon', 'clash-royale'),
(3, 1, 'Overwatch World Cup 2017', 'Disputées aux quatre coins du monde, les phases de qualification estivales de l''Overwatch World Cup 2017 se jouent actuellement en Chine.', 'Disputées aux quatre coins du monde, les phases de qualification estivales de l''Overwatch World Cup 2017 se jouent actuellement en Chine où Jack et Skypper sont allé couvrir l''évènement pour Millenium. Comme, a priori, vous n''avez malheureusement pas la chance d''être sur place pour voir les joueurs, nous vous proposons de retrouver toutes les photos de l''événement réalisées sur place.', 'public/img/second.jpg', '2017-07-17 14:07:27', NULL, 'events', 1, 'news/show/overwatch/overwatch-world-cup-2017', 'overwatch');

-- --------------------------------------------------------

--
-- Structure de la table `we_game`
--

CREATE TABLE `we_game` (
  `id` int(10) unsigned NOT NULL,
  `id_game_list` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` varchar(100) COLLATE utf8_bin NOT NULL,
  `player_max_per_team` int(10) unsigned NOT NULL,
  `nb_team` int(10) unsigned DEFAULT NULL,
  `stream_link` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_game_fixture`
--

CREATE TABLE `we_game_fixture` (
  `id` int(10) unsigned NOT NULL,
  `id_game` int(10) unsigned NOT NULL,
  `id_team1` int(10) unsigned NOT NULL,
  `id_team2` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_game_list`
--

CREATE TABLE `we_game_list` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_game_list`
--

INSERT INTO `we_game_list` (`id`, `id_user`, `name`, `image`, `status`) VALUES
(1, 1, 'LOL', 'DEDE', 1),
(2, 1, 'DOTA', 'DDFF', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `we_game_result`
--

CREATE TABLE `we_game_result` (
  `id` int(10) unsigned NOT NULL,
  `id_game` int(10) unsigned NOT NULL,
  `id_team1` int(10) unsigned NOT NULL,
  `id_team2` int(10) unsigned NOT NULL,
  `result_team1` int(11) DEFAULT NULL,
  `result_team2` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_game_team`
--

CREATE TABLE `we_game_team` (
  `id` int(10) unsigned NOT NULL,
  `id_game` int(10) unsigned NOT NULL,
  `id_team` int(10) unsigned NOT NULL,
  `score` int(10) unsigned DEFAULT NULL,
  `has_played` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_game_team_player`
--

CREATE TABLE `we_game_team_player` (
  `id` int(10) unsigned NOT NULL,
  `id_game` int(10) unsigned NOT NULL,
  `id_team` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_notification`
--

CREATE TABLE `we_notification` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `id_notification_type` int(10) unsigned NOT NULL,
  `wording` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `is_view` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_notification_type`
--

CREATE TABLE `we_notification_type` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_private_message`
--

CREATE TABLE `we_private_message` (
  `id` int(10) unsigned NOT NULL,
  `id_sender` int(10) unsigned NOT NULL,
  `id_receiver` int(10) unsigned NOT NULL,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_private_message`
--

INSERT INTO `we_private_message` (`id`, `id_sender`, `id_receiver`, `title`, `content`, `created_at`, `is_deleted`) VALUES
(1, 1, 2, 'defrfrffr', '&lt;p&gt;frfrfrfrf&lt;/p&gt;', '2017-07-26 11:28:47', 0),
(2, 1, 2, 'test', '&lt;p&gt;test&lt;/p&gt;', '2017-07-26 12:20:36', 0);

-- --------------------------------------------------------

--
-- Structure de la table `we_rights`
--

CREATE TABLE `we_rights` (
  `id` int(10) unsigned NOT NULL,
  `right_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `right_code` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_role`
--

CREATE TABLE `we_role` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(155) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_role`
--

INSERT INTO `we_role` (`id`, `name`, `status`, `created_at`) VALUES
(1, 'Invité', 1, '2017-07-09 12:34:33'),
(2, 'Membre', 1, '2017-07-09 12:34:33'),
(3, 'Modérateur', 1, '2017-07-09 12:34:33'),
(4, 'Administrateur', 1, '2017-07-09 12:34:33');

-- --------------------------------------------------------

--
-- Structure de la table `we_role_rights`
--

CREATE TABLE `we_role_rights` (
  `id_right` int(10) unsigned NOT NULL,
  `id_role` int(10) unsigned NOT NULL,
  `id_action` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_role_user`
--

CREATE TABLE `we_role_user` (
  `id` int(10) unsigned NOT NULL,
  `id_role` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_role_user`
--

INSERT INTO `we_role_user` (`id`, `id_role`, `id_user`, `status`) VALUES
(1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `we_streamer`
--

CREATE TABLE `we_streamer` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_streamer`
--

INSERT INTO `we_streamer` (`id`, `username`) VALUES
(1, 'esl_csgo');

-- --------------------------------------------------------

--
-- Structure de la table `we_team`
--

CREATE TABLE `we_team` (
  `id` int(10) unsigned NOT NULL,
  `id_game_list` int(10) unsigned NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `status` tinyint(1) DEFAULT NULL,
  `on_demand` tinyint(1) DEFAULT NULL,
  `score` int(10) unsigned DEFAULT NULL,
  `has_played` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_team`
--

INSERT INTO `we_team` (`id`, `id_game_list`, `leader`, `name`, `image`, `content`, `status`, `on_demand`, `score`, `has_played`, `created_at`) VALUES
(1, 2, 6, 'Augue Scelerisque Mollis LLC', '246238109', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,', 0, 0, 39, 10, '2018-05-29 12:06:01'),
(2, 10, 7, 'Adipiscing Ltd', '122001092', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer', 0, 0, 37, 7, '2016-12-26 19:00:00'),
(3, 6, 4, 'Proin Velit Sed Limited', '972620348', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,', 0, 0, 2, 6, '2017-02-15 10:34:44'),
(4, 10, 5, 'Tincidunt LLP', '087967352', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna.', 0, 0, 12, 9, '2017-12-12 11:07:37'),
(5, 8, 4, 'Dictum Phasellus Industries', '414821975', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet', 0, 0, 18, 9, '2016-10-16 18:50:49'),
(6, 3, 7, 'Per Inceptos Ltd', '008224164', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum', 0, 0, 24, 5, '2016-08-19 04:59:20'),
(7, 3, 6, 'Sodales Purus Ltd', '422511816', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.', 0, 0, 4, 10, '2018-05-29 20:16:25'),
(8, 6, 9, 'Sit Amet Ante Consulting', '410353346', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque', 0, 0, 24, 10, '2016-10-20 23:55:06'),
(9, 2, 3, 'Ullamcorper Magna Sed Ltd', '567510995', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras', 0, 0, 12, 1, '2017-06-25 13:31:44'),
(10, 7, 1, 'Ipsum Institute', '063489512', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu', 0, 0, 7, 3, '2018-05-28 11:00:45'),
(11, 5, 8, 'Euismod LLP', '362575656', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at', 0, 0, 5, 1, '2017-10-28 16:43:58'),
(12, 5, 8, 'Cum Sociis Foundation', '609425061', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin', 0, 0, 11, 5, '2016-08-10 21:05:31'),
(13, 9, 4, 'Neque Corp.', '477279293', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.', 0, 0, 36, 5, '2017-02-20 05:11:07'),
(14, 2, 4, 'Nonummy Fusce Corp.', '516867694', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.', 0, 0, 24, 7, '2017-08-06 17:14:37'),
(15, 4, 2, 'Cursus Luctus Ipsum Corp.', '074814658', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis', 0, 0, 15, 4, '2017-11-19 10:54:25'),
(16, 10, 9, 'Auctor Incorporated', '442477907', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec', 0, 0, 32, 5, '2017-08-31 15:28:08'),
(17, 5, 4, 'Orci Limited', '794010975', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 4, 3, '2018-05-19 11:13:26'),
(18, 9, 9, 'Sociis Industries', '462335977', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida', 0, 0, 17, 10, '2017-02-28 15:01:04'),
(19, 5, 9, 'Sed Est Nunc Corporation', '843349804', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras', 0, 0, 33, 2, '2018-05-28 19:48:53'),
(20, 9, 5, 'A Feugiat LLC', '887620755', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,', 0, 0, 38, 10, '2017-07-01 16:55:35'),
(21, 8, 7, 'Fringilla LLP', '836615849', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam', 0, 0, 1, 7, '2017-12-07 00:21:54'),
(22, 2, 8, 'Augue Associates', '479725509', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt', 0, 0, 2, 8, '2018-01-03 07:57:42'),
(23, 6, 5, 'Lectus Ltd', '845321470', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.', 0, 0, 4, 4, '2017-01-28 05:45:41'),
(24, 9, 6, 'Sit Inc.', '627267008', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac', 0, 0, 32, 7, '2016-09-11 02:20:57'),
(25, 8, 4, 'Elementum Sem Vitae Consulting', '888245669', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut', 0, 0, 11, 1, '2017-05-26 01:02:55'),
(26, 2, 7, 'Nec Foundation', '003900735', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt', 0, 0, 19, 6, '2017-01-28 08:46:16'),
(27, 4, 8, 'Libero Morbi Accumsan PC', '293510152', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 0, 0, 31, 7, '2017-09-16 00:29:04'),
(28, 7, 8, 'Fusce Aliquam Inc.', '325367365', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.', 0, 0, 16, 10, '2016-12-22 12:34:23'),
(29, 4, 6, 'Cras Corporation', '106321292', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 8, 8, '2017-05-01 16:34:09'),
(30, 6, 6, 'Non Lobortis PC', '379114549', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque', 0, 0, 31, 7, '2016-10-09 13:11:53'),
(31, 6, 2, 'Vehicula Pellentesque Tincidunt Institute', '520169079', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,', 0, 0, 22, 7, '2016-10-11 04:15:30'),
(32, 8, 9, 'Lacus Corporation', '328137617', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum', 0, 0, 14, 10, '2017-09-13 02:49:06'),
(33, 4, 8, 'Feugiat Lorem Ipsum Industries', '465989788', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus', 0, 0, 25, 6, '2016-12-14 00:00:59'),
(34, 8, 8, 'Vulputate Velit Eu Corp.', '953355484', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras', 0, 0, 24, 1, '2017-04-11 18:54:10'),
(35, 9, 2, 'Aliquet Libero Integer PC', '232963843', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna.', 0, 0, 27, 6, '2016-08-23 03:46:03'),
(36, 10, 2, 'Malesuada Augue Ltd', '668070238', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 0, 0, 18, 7, '2018-04-22 19:58:07'),
(37, 7, 1, 'Imperdiet Nec Associates', '445421779', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede', 0, 0, 29, 8, '2016-11-06 11:38:25'),
(38, 4, 5, 'Mollis Dui LLC', '491057790', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,', 0, 0, 37, 7, '2018-01-05 06:33:54'),
(39, 9, 5, 'Faucibus Id Ltd', '617709399', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac', 0, 0, 32, 9, '2018-02-05 13:01:46'),
(40, 10, 8, 'Non Justo Proin Ltd', '860872936', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor.', 0, 0, 12, 1, '2018-06-11 19:59:14'),
(41, 10, 7, 'Purus Duis Industries', '694044272', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.', 0, 0, 26, 8, '2016-10-24 11:15:01'),
(42, 6, 4, 'Morbi Consulting', '288213226', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna.', 0, 0, 7, 5, '2016-08-12 21:46:44'),
(43, 4, 8, 'Gravida Sit Company', '526869599', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.', 0, 0, 26, 10, '2017-01-12 08:21:27'),
(44, 4, 1, 'Nulla Tincidunt Institute', '410209068', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna', 0, 0, 19, 3, '2016-10-14 00:34:42'),
(45, 4, 8, 'Dictum Foundation', '833208309', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 0, 0, 39, 7, '2016-12-01 06:27:03'),
(46, 9, 1, 'Adipiscing Industries', '078537081', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede', 0, 0, 38, 3, '2017-09-24 10:51:48'),
(47, 10, 2, 'Eleifend Company', '092422625', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque', 0, 0, 3, 6, '2017-08-16 03:04:06'),
(48, 9, 5, 'A Purus Incorporated', '351618426', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam', 0, 0, 26, 4, '2016-07-31 03:12:58'),
(49, 1, 7, 'Ligula Institute', '377531223', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer', 0, 0, 30, 4, '2017-12-10 00:48:59'),
(50, 7, 3, 'Blandit Inc.', '332674795', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu', 0, 0, 20, 3, '2016-09-11 03:36:47'),
(51, 3, 4, 'Velit Dui Company', '129202396', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna.', 0, 0, 12, 6, '2017-10-22 03:57:43'),
(52, 8, 4, 'Magna A Neque Institute', '511526576', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras', 0, 0, 10, 5, '2018-01-02 21:36:18'),
(53, 1, 7, 'Sed Nunc Est Associates', '585706666', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 0, 0, 13, 5, '2017-09-02 15:59:24'),
(54, 7, 10, 'Mauris Nulla Integer Company', '108926643', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus', 0, 0, 10, 4, '2016-09-10 21:22:23'),
(55, 3, 3, 'Aliquet Libero PC', '562734327', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu', 0, 0, 19, 7, '2018-03-05 23:28:58'),
(56, 1, 8, 'Orci Luctus LLP', '524870714', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 11, 9, '2017-07-29 08:02:35'),
(57, 3, 5, 'Quisque Ltd', '206555005', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna', 0, 0, 34, 7, '2016-07-28 01:56:14'),
(58, 1, 10, 'Natoque Penatibus Et Corporation', '841901820', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 0, 0, 29, 4, '2017-06-18 13:40:23'),
(59, 7, 10, 'Odio Auctor Ltd', '674911649', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,', 0, 0, 35, 1, '2017-10-16 10:25:57'),
(60, 1, 5, 'Fringilla LLC', '757702451', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 39, 1, '2017-11-07 04:01:32'),
(61, 6, 10, 'Scelerisque Scelerisque Dui Inc.', '471010520', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus', 0, 0, 16, 2, '2018-03-29 06:06:16'),
(62, 1, 8, 'Et Associates', '195118161', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum', 0, 0, 7, 2, '2016-08-21 00:27:30'),
(63, 2, 9, 'Libero Morbi Accumsan Institute', '499086601', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis', 0, 0, 26, 3, '2017-03-10 06:21:55'),
(64, 5, 8, 'Erat Vel Pede Consulting', '298985292', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 0, 0, 3, 6, '2016-08-17 03:50:01'),
(65, 1, 10, 'Purus Institute', '544033376', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.', 0, 0, 34, 10, '2017-02-14 03:45:36'),
(66, 1, 9, 'Leo In Lobortis Corp.', '103901195', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et', 0, 0, 13, 10, '2017-04-01 05:23:51'),
(67, 8, 3, 'Commodo At Libero Industries', '381112176', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam', 0, 0, 40, 3, '2016-12-11 01:17:59'),
(68, 9, 1, 'Sollicitudin Company', '703131789', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 0, 0, 22, 10, '2017-12-14 08:49:08'),
(69, 3, 4, 'Ipsum Phasellus Corporation', '926560012', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.', 0, 0, 24, 1, '2017-02-18 13:21:39'),
(70, 1, 10, 'Et Arcu Imperdiet LLC', '624935227', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing', 0, 0, 9, 2, '2018-06-19 04:33:15'),
(71, 1, 2, 'Volutpat Nulla Dignissim Incorporated', '427384748', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa', 0, 0, 10, 8, '2017-04-22 08:17:25'),
(72, 5, 4, 'Maecenas Iaculis Aliquet LLC', '539564864', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,', 0, 0, 37, 5, '2017-10-08 11:41:57'),
(73, 3, 4, 'Vulputate Company', '733327142', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin', 0, 0, 10, 3, '2018-01-09 14:54:43'),
(74, 4, 2, 'Mollis LLP', '239988892', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.', 0, 0, 25, 3, '2016-11-01 18:28:23'),
(75, 7, 3, 'Montes Ltd', '500834908', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna.', 0, 0, 23, 1, '2017-11-01 22:47:43'),
(76, 10, 6, 'Elit Fermentum Inc.', '399604693', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor.', 0, 0, 19, 5, '2018-04-05 18:59:10'),
(77, 8, 6, 'Est Limited', '765244082', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida', 0, 0, 26, 5, '2017-07-07 00:50:21'),
(78, 3, 5, 'Lorem Ipsum LLP', '983614090', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.', 0, 0, 1, 4, '2016-08-19 13:23:36'),
(79, 10, 4, 'Magna PC', '351287529', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa', 0, 0, 20, 3, '2016-11-23 19:52:33'),
(80, 10, 1, 'Metus Aliquam PC', '328572532', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor.', 0, 0, 15, 10, '2018-07-07 20:02:10'),
(81, 4, 8, 'Dictum Mi LLC', '530550557', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 15, 9, '2018-07-22 15:16:49'),
(82, 8, 3, 'Sem Molestie LLP', '653505214', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis', 0, 0, 16, 3, '2017-10-26 09:33:17'),
(83, 6, 9, 'Quis Pede Praesent LLC', '391642949', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin', 0, 0, 24, 4, '2017-03-31 04:05:29'),
(84, 5, 4, 'Pharetra Sed Hendrerit LLC', '325602217', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 12, 3, '2017-11-04 06:23:53'),
(85, 8, 10, 'Nisi Cum Sociis Limited', '985125657', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna.', 0, 0, 4, 5, '2017-06-10 23:04:29'),
(86, 1, 4, 'Eu Ultrices Sit Company', '099482598', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.', 0, 0, 7, 10, '2016-09-09 20:36:44'),
(87, 10, 8, 'Enim LLC', '322564816', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna', 0, 0, 16, 7, '2017-10-18 12:23:12'),
(88, 2, 10, 'Lectus A Inc.', '247531932', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis', 0, 0, 26, 1, '2017-12-04 09:41:51'),
(89, 2, 2, 'Magna Et LLP', '780914719', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,', 0, 0, 38, 1, '2017-11-03 21:45:52'),
(90, 8, 1, 'Justo Praesent Industries', '313158024', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer', 0, 0, 22, 7, '2017-06-05 16:32:40'),
(91, 10, 2, 'Dictum Consulting', '189959596', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing', 0, 0, 35, 5, '2017-04-20 17:45:57'),
(92, 5, 3, 'Sagittis Semper Ltd', '686093949', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.', 0, 0, 16, 5, '2016-09-06 11:50:59'),
(93, 8, 9, 'Quisque Libero Lacus Industries', '419628821', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam', 0, 0, 16, 4, '2017-05-22 18:02:49'),
(94, 6, 3, 'Morbi Tristique Senectus Industries', '513311381', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.', 0, 0, 10, 7, '2018-07-15 23:09:57'),
(95, 10, 3, 'Mauris Eu Turpis LLC', '863015921', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam', 0, 0, 32, 5, '2016-11-04 17:26:34'),
(96, 8, 2, 'Non Feugiat Institute', '318211778', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut', 0, 0, 33, 8, '2018-07-14 00:35:17'),
(97, 10, 6, 'Aenean Massa Corporation', '934323239', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt', 0, 0, 9, 7, '2016-07-31 09:32:06'),
(98, 4, 3, 'Duis Sit Associates', '226543080', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam', 0, 0, 32, 10, '2017-09-27 18:57:55'),
(99, 1, 7, 'Pellentesque Corporation', '956794879', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida', 0, 0, 25, 5, '2017-09-05 06:38:20'),
(100, 2, 7, 'Donec Nibh Incorporated', '859410912', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede', 0, 0, 23, 7, '2017-03-14 09:17:26'),
(101, 4, 1, 'EXEMPLE', 'EXEMPLE', 'EXEMPLE', 0, 0, 0, 0, '2017-07-23 20:49:25');

-- --------------------------------------------------------

--
-- Structure de la table `we_team_user`
--

CREATE TABLE `we_team_user` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `id_team` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `we_team_user_demand`
--

CREATE TABLE `we_team_user_demand` (
  `id` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `id_team` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `demand_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_team_user_demand`
--

INSERT INTO `we_team_user_demand` (`id`, `id_user`, `id_team`, `status`, `demand_at`) VALUES
(1, 1, 22, 1, '2017-07-26 11:32:09'),
(2, 1, 14, 1, '2017-07-26 12:16:34');

-- --------------------------------------------------------

--
-- Structure de la table `we_user`
--

CREATE TABLE `we_user` (
  `id` int(10) unsigned NOT NULL,
  `id_role` int(10) unsigned NOT NULL,
  `username` varchar(15) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(60) COLLATE utf8_bin NOT NULL,
  `avatar` text COLLATE utf8_bin,
  `certify` tinyint(1) DEFAULT NULL,
  `token` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_connexion_at` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `is_banned` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `we_user`
--

INSERT INTO `we_user` (`id`, `id_role`, `username`, `firstname`, `lastname`, `birthday`, `email`, `password`, `avatar`, `certify`, `token`, `created_at`, `last_connexion_at`, `date_updated`, `is_deleted`, `is_banned`) VALUES
(1, 4, 'admin', 'Waziri', 'SAMUEL', NULL, 'samuelantunes@hotmail.fr', '$2y$10$ULRVoOyJmeDAIUCiYsmub.5qlpsKlZx5S0p1DXpkyCqCZMHHjfv3O', NULL, 1, 'e7bd788900002f8fb9b4154695844d6f67e5006920170716151846', '2017-07-09 12:34:33', '2017-07-25 23:28:19', '2017-07-26 12:20:00', NULL, NULL),
(2, 3, 'Yann77', 'Nouve', 'WAZ', '1970-01-01', 'weswan.team@gmail.com', '$2y$10$e2Y1C8Gshw.Z8lqetoC9u.laohIV.hRsXpsCsSYzwd0OsD1XQgOMm', 'uploads/597889a18b566.jpg', 1, 'e3377ac06e72374d334e05d5ce74cf650c323c8120170709140211', '2017-07-09 12:55:56', NULL, '2017-07-26 12:22:57', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `we_comment`
--
ALTER TABLE `we_comment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_config`
--
ALTER TABLE `we_config`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_content`
--
ALTER TABLE `we_content`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_game`
--
ALTER TABLE `we_game`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_game_fixture`
--
ALTER TABLE `we_game_fixture`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_game_list`
--
ALTER TABLE `we_game_list`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_game_result`
--
ALTER TABLE `we_game_result`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_game_team`
--
ALTER TABLE `we_game_team`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_game_team_player`
--
ALTER TABLE `we_game_team_player`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_notification`
--
ALTER TABLE `we_notification`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_notification_type`
--
ALTER TABLE `we_notification_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_private_message`
--
ALTER TABLE `we_private_message`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_rights`
--
ALTER TABLE `we_rights`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_role`
--
ALTER TABLE `we_role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_role_user`
--
ALTER TABLE `we_role_user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_streamer`
--
ALTER TABLE `we_streamer`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_team`
--
ALTER TABLE `we_team`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_team_user`
--
ALTER TABLE `we_team_user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_team_user_demand`
--
ALTER TABLE `we_team_user_demand`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `we_user`
--
ALTER TABLE `we_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `we_comment`
--
ALTER TABLE `we_comment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `we_config`
--
ALTER TABLE `we_config`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `we_content`
--
ALTER TABLE `we_content`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `we_game`
--
ALTER TABLE `we_game`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_game_fixture`
--
ALTER TABLE `we_game_fixture`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_game_list`
--
ALTER TABLE `we_game_list`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `we_game_result`
--
ALTER TABLE `we_game_result`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_game_team`
--
ALTER TABLE `we_game_team`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_game_team_player`
--
ALTER TABLE `we_game_team_player`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_notification`
--
ALTER TABLE `we_notification`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_notification_type`
--
ALTER TABLE `we_notification_type`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_private_message`
--
ALTER TABLE `we_private_message`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `we_rights`
--
ALTER TABLE `we_rights`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_role`
--
ALTER TABLE `we_role`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `we_role_user`
--
ALTER TABLE `we_role_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `we_streamer`
--
ALTER TABLE `we_streamer`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `we_team`
--
ALTER TABLE `we_team`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT pour la table `we_team_user`
--
ALTER TABLE `we_team_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `we_team_user_demand`
--
ALTER TABLE `we_team_user_demand`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `we_user`
--
ALTER TABLE `we_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;