CREATE TABLE we_user(
    id                    INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_role               INT UNSIGNED NOT NULL,
    username              VARCHAR(15)  NOT NULL,
    firstname             VARCHAR(50)  NULL,
    lastname              VARCHAR(50)  NULL,
    birthday              DATE         NULL,
    email                 VARCHAR(100) NOT NULL,
    password              VARCHAR(60)  NOT NULL,
    avatar                TEXT         NULL,
    certify               TINYINT(1)   NULL,
    token                 VARCHAR(60)  NULL,
    created_at            TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_connexion_at     TIMESTAMP    NULL,
    date_updated          TIMESTAMP    NULL,
    is_deleted            TINYINT(1)   NULL,
    is_banned             TINYINT(1)   NULL,
    PRIMARY KEY (id)
)
ENGINE=INNODB;


CREATE TABLE we_role (
    id          INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name        VARCHAR(155) NOT NULL,
    status      TINYINT(1)   NOT NULL,
    created_at  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;


CREATE TABLE we_role_user (
    id          INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_role     INT UNSIGNED NOT NULL,
    id_user     INT UNSIGNED NOT NULL,
    status      TINYINT(1)   NOT NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_rights (
    id                          INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    right_name                  VARCHAR(100)  NULL,
    right_code                  VARCHAR(50)  NULL,
    created_at                  TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_role_rights (
    id_right                    INT UNSIGNED  NOT NULL,
    id_role                     INT UNSIGNED  NOT NULL,
    id_action                   INT UNSIGNED  NOT NULL, #1 = read ; 2 = create; 3 = update; 4 = delete
    PRIMARY KEY(id_right, id_role, id_action)
)
ENGINE=INNODB;


CREATE TABLE we_content (
    id            INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_user       INT UNSIGNED NOT NULL,
    title         VARCHAR(150) NOT NULL,
    resume        TEXT         NULL,
    content       TEXT         NOT NULL,
    date_created  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    date_updated  TIMESTAMP    NULL,
    type          VARCHAR(100) NULL,
    status        TINYINT(1)   NOT NULL,
    slug          VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
)
ENGINE=INNODB;


CREATE TABLE we_comment (
    id            INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_user       INT UNSIGNED NOT NULL,
    id_content    INT UNSIGNED NOT NULL,
    comment       TEXT         NOT NULL,
    date_created  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
)
ENGINE=INNODB;

CREATE TABLE we_team (
    id           INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_game_list INT UNSIGNED  NOT NULL,
    leader       INT UNSIGNED  NOT NULL,
    name         VARCHAR(255)  NOT NULL,
    image        VARCHAR(255)  NULL,
    content      TEXT          NULL,
    status       TINYINT(1)    NULL,
    on_demand    TINYINT(1)    NULL,
    score        INT UNSIGNED  NULL,   /* 3 point : gagné ;  1 match nul ; 0 perdu */
    has_played   INT UNSIGNED  NULL,
    created_at   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_team_user (
    id           INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_user      INT UNSIGNED NOT NULL,
    id_team      INT UNSIGNED NOT NULL,
    status       TINYINT(1)   NOT NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;


CREATE TABLE we_team_user_demand (
    id           INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_user      INT UNSIGNED NOT NULL,
    id_team      INT UNSIGNED NOT NULL,
    status       TINYINT(1)   NOT NULL,
    demand_at    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_game_list (
    id       INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_user  INT UNSIGNED NOT NULL,
    name     VARCHAR(255) NOT NULL,
    image    VARCHAR(255) NULL,
    status   TINYINT(1)   NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_game (
    id                        INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_game_list              INT UNSIGNED  NOT NULL,
    id_user                   INT UNSIGNED  NOT NULL,
    title                     VARCHAR(255)  NOT NULL,
    type                      VARCHAR(100)  NOT NULL,
    player_max_per_team       INT UNSIGNED  NOT NULL,
    nb_team                   INT UNSIGNED  NULL,
    stream_link               VARCHAR(255)  NULL,
    date_start                TIMESTAMP     NOT NULL,
    date_end                  TIMESTAMP     NOT NULL,
    status                    TINYINT(1)    NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_game_team (
    id           INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_game      INT UNSIGNED NOT NULL,
    id_team      INT UNSIGNED NOT NULL,
    score        INT UNSIGNED NULL,
    has_played   INT UNSIGNED NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_game_team_player (
    id           INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_game      INT UNSIGNED NOT NULL,
    id_team      INT UNSIGNED NOT NULL,
    id_user      INT UNSIGNED NOT NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_game_fixture (
    id            INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_game       INT UNSIGNED  NOT NULL,
    id_team1      INT UNSIGNED  NOT NULL,
    id_team2      INT UNSIGNED  NOT NULL,
    created_at    TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_game_result (
    id            INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_game       INT UNSIGNED  NOT NULL,
    id_team1      INT UNSIGNED  NOT NULL,
    id_team2      INT UNSIGNED  NOT NULL,
    result_team1  INT SIGNED    NULL,
    result_team2  INT SIGNED    NULL,
    created_at    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;



CREATE TABLE we_private_message (
    id              INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_sender       INT UNSIGNED  NOT NULL,
    id_receiver     INT UNSIGNED  NOT NULL,
    title           VARCHAR(100)  NULL,
    content         TEXT          NOT NULL,
    created_at      TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_deleted      INT UNSIGNED  NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_notification_type (
    id          INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_user     INT UNSIGNED  NOT NULL,
    name        VARCHAR(100)  NOT NULL,
    status      INT UNSIGNED  NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;


CREATE TABLE we_notification (
    id                          INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    id_user                     INT UNSIGNED  NOT NULL,
    id_notification_type        INT UNSIGNED  NOT NULL,
    wording                     VARCHAR(100)  NULL,
    is_view                     INT UNSIGNED  NULL,
    created_at                  TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)
ENGINE=INNODB;

CREATE TABLE we_config (
    id                          INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    wording                     VARCHAR(255)  NOT NULL,
    value                       TEXT          NULL,
    PRIMARY KEY(id)
)
ENGINE=INNODB;