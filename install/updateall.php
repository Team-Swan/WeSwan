<?php
session_start();

if(file_exists('../configDB.inc.php')) {
    die(json_encode(['notification' => 'error', 'getError' => 1, 'message' => "La base de données a déjà été créée."]));
}
try {
    $db = new PDO('mysql:host=' . $_SESSION['data_step']['step1']['db_host'] . ';dbname=' . $_SESSION['data_step']['step1']['db_dbname'] . ';port=' . intval($_SESSION['data_step']['step1']['db_port']) . ';charset=utf8',
        $_SESSION['data_step']['step1']['db_user'], $_SESSION['data_step']['step1']['db_password']);
} catch (PDOException $e) {
    die(json_encode(['notification' => 'error', 'message' => "La connexion a la base de donnée a échouée"]));
}

$sql = file_get_contents("wecms.sql");
$db->query($sql);

/* CREATE PHP FILE */
$configBDD = '<?php 

define("DB_USER", "' . trim($_SESSION['data_step']['step1']['db_user']) . '");
define("DB_PASSWORD", "' . trim($_SESSION['data_step']['step1']['db_password']) . '");
define("DB_HOST", "' . trim($_SESSION['data_step']['step1']['db_host']) . '");
define("DB_NAME", "' . trim($_SESSION['data_step']['step1']['db_dbname']) . '");
define("DB_PORT", ' . trim($_SESSION['data_step']['step1']['db_port']) . ');';

$myfile = fopen("../configDB.inc.php", "w") or die("Unable to open file!");
fwrite($myfile, $configBDD);
fclose($myfile);


/* INSERT WEBSITE INFORMATIONS */
$query = $db->prepare('INSERT INTO we_config(wording,value) VALUES (:wording,:value)');
$query->execute([
    "wording" => "site_name",
    "value" => trim($_SESSION['data_step']['step2']['site_name'])
]);
$query = $db->prepare('INSERT INTO we_config(wording,value) VALUES (:wording,:value)');
$query->execute([
    "wording" => "site_desc",
    "value" => trim(htmlentities($_SESSION['data_step']['step2']['site_desc']))
]);
$query = $db->prepare('INSERT INTO we_config(wording,value) VALUES (:wording,:value)');
$query->execute([
    "wording" => "site_link",
    "value" => trim($_SESSION['data_step']['step2']['site_link'])
]);

/* INSERT ROLE */
$query = $db->prepare('INSERT INTO we_role(name,status) VALUES (:name,:status)');
$query->execute([
    "name" => "Invité",
    "status" => 1
]);
$query = $db->prepare('INSERT INTO we_role(name,status) VALUES (:name,:status)');
$query->execute([
    "name" => "Membre",
    "status" => 1
]);
$query = $db->prepare('INSERT INTO we_role(name,status) VALUES (:name,:status)');
$query->execute([
    "name" => "Modérateur",
    "status" => 1
]);
$query = $db->prepare('INSERT INTO we_role(name,status) VALUES (:name,:status)');
$query->execute([
    "name" => "Administrateur",
    "status" => 1
]);

/* ATTRIBUTE ROLE TO USER */

$query = $db->prepare('INSERT INTO we_role_user(id_role,id_user,status) VALUES (id_role,id_user,status)');
$query->execute([
    "id_role" => 4,
    "id_user" => 1,
    "status" => 1
]);


/* INSERT USER INFORMATIONS*/
$query = $db->prepare('INSERT INTO we_user(id_role,username,email,password, certify) VALUES (:id_role,:username,:email,:password, :certify)');
$query->execute([
    "id_role" => 4,
    "username" => htmlentities(trim($_SESSION['data_step']['step3']['account_pseudo'])),
    "email" =>  trim($_SESSION['data_step']['step3']['account_mail']),
    "password" => password_hash(trim($_SESSION['data_step']['step3']['account_password']), PASSWORD_BCRYPT),
    "certify" => 1
]);


if ($db->errorInfo()[2] == NULL) {
    die(json_encode(['notification' => 'success', 'getError' => 1, 'message' => "Votre base de données a correctement été créée."]));
    unset($_SESSION['data_step']);
    unset($_SESSION['step']);
} else {
    die(json_encode(['notification' => 'error', 'message' => "La base de données existe déjà."]));
}



