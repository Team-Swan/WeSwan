<?php
// Get random char.
function getRandom()
{
    $caracters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $result = '';
    for ($i = 0; $i < 15; $i++)
        $result .= $caracters[mt_rand(0, strlen($caracters))];

    return $result;
}
