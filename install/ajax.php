<?php
if(empty($_POST['getElements']['db_dbname'])) {
    die(json_encode(['connection' => FALSE, 'message' => "La connexion à la base de données a échouée."]));
}
try {
    @$db = new PDO('mysql:host=' . $_POST['getElements']['db_host'] . ';dbname=' . $_POST['getElements']['db_dbname'] . ';port=' . intval($_POST['getElements']['db_port']) . ';charset=utf8',
        $_POST['getElements']['db_user'], $_POST['getElements']['db_password'], [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    die(json_encode(['connection' => TRUE]));
} catch (Exception $e) {
    die(json_encode(['connection' => FALSE, 'message' => "La connexion à la base de données a échouée."]));
}




